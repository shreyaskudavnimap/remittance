import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class HttpClientService implements HttpInterceptor {
    constructor() {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


        const request = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });


        if (request.headers.has('flask')) {
            return next.handle(request);
        }

        if (request.headers.has('IP')) {
            const request = req.clone({ headers: req.headers.set('Accept', "text/plain") });
            return next.handle(request);
        }
        if (request.headers.has('LOG')) {
            return next.handle(request);
        }
        if (request.headers.has('Transaction_sent')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'Transaction_sentmXEKTzgF8WTXYhw') });
            // console.log(request); -- not USED
            return next.handle(request);
        }
        if (request.headers.has('TRANSACTION_NUM')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'TRANSACTION_NUMmXEKTzgF8WTXYhw') });
            // console.log(request); -- Not USED
            return next.handle(request);
        }
        if (request.headers.has('filterone')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '1eb21a4e784c474e8705f33282b4ade4') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('filtertwo')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '175638af961b48629d89f29a954c5512') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('finalfilter')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '9cd5963e0ddc414d851bd9ea5c26ff48') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('Login')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '5_aVlc9TVEGgXBi-bPJqMQ') });
            // console.log(request); -- Not USED 
            return next.handle(request);
        }

        if (request.headers.has('One')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'bd570befe48844708b0b1d0a8cefb553') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('Two')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '66d73f7b58b0405cb1fb1eef7f6948b2') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('Countries')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '76f78676000449d79673b036bc0abe21') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('Complaince')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '175638af961b48629d89f29a954c5512') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('Cagent')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '1861b94e29fc4b87a90c01c45062e984') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_TWO')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '1eb21a4e784c474e8705f33282b4ade4') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_MENUTWO')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'uO4fJ0WuC0mcbw1wndkXMMM') });
            // console.log(request); --- Its not used yet 
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_MENUTHREE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '46ff60d513c34a2aae507bfc246bde2c') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_MENUTHREETWO')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'b222ababee7a4a518321b5f2a33f97b9') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_MENUTHREETHREE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'b222ababee7a4a518321b5f2a33f97b9') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_MENUFour')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '59279aee715949429514eb2ad0141cf5') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_SUBMIT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '46ff60d513c34a2aae507bfc246bde2c') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_MENU_TWO')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'd35487cbbbf845e8820eb21d2daa7c59') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('COMPLAINCE_MENU_SUBMIT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '9cd5963e0ddc414d851bd9ea5c26ff48') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('TRANSFER_POT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'dae3472736aa4f35b7a2aff21529c1a6') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('TRANSFER_RC')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'ae498fbc7ff3449ab78eea55b656b7cb') });
            // console.log(request);
            return next.handle(request);
        }
        if (request.headers.has('TRANSFER_CITY')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '037a0dc632004a1fac8cf9c1113f5afe') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('TRANSFER_MOP')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '98821f6e9e4f46d5be71acc716eab917') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('TRANSFER_CURR')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '102981e471db4b7cb7e8e14f24d9d85f') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('TRANSFER_RECC')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '53fcb5ef12724c31aca985f0a843c55d') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('CUSTOMER_NATIONALITY')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'bd570befe48844708b0b1d0a8cefb553') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('CUSTOMER_COUNTRY')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'b3227a8794b3461082045d2ef30e7c78') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('CUSTOMER_IDTYPE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'f8b0bb6a69e541239b768dbcce812984') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('transfer_currencyreceive')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '94d45dcf854b4951842dcdbc3d99ccb1') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('CUSSTOMER_SOURCE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '238d3438d17d4098b4706b49f7914cd9') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('CUSSTOMER_REGISTER')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '314af22ba41e4436b5218878dca10990') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENE_REGISTER')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '3173c2cbe51c4b4f9a876079c347c1ff') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENE_BRANCH')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '99be9f0ea8e44609bcfbcc16c903f27c') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENE_MODE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'db67219b40674be3bc4b49d94c777e17') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENE_PURPOSE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'dae3472736aa4f35b7a2aff21529c1a6') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENE_IDTYPE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'e481f5486912463ca2c9294347a9c5e1') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENE_RELATION')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '7c2996ed87534af9b0e4afe56e8faced') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BANK_SENDING_CURRENCY_CER')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '8c73b3a3f2b043da87d22b834b79a7d3') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENEFICIARY_DATA')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'dea05380b6274e7cadf7ecf34b85768a') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('GET_PAYIN_USD_AMT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'd1c037ff17644dd9a705c213624b709c') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('GET_RANDOM_TRANSACTION_NUM')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'fe42d54d0d4b41eeba6aad8d78e02e22') });
            // console.log(request);
            return next.handle(request);
        }   

        if (request.headers.has('SUBMIT_TR_MASTER_TEMP')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '1d50e423fab349d5b6bfb013f248e5f4') });
            // console.log(request);
            return next.handle(request);
        }  

        if (request.headers.has('CURRENCY_DETAILS_1')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'b6b414c13d7d4b7ea73d305bedfe2592') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('CUSTOMER_DATA')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'a9dce71255f94440a96e0a486658061b') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('VAR_INFORMATION')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '45daa93675f44f74a783200fa7341352') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('RECEIVING_AGENT_PERCENTAGE_WISE_COMMISSION')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'aa92c902c3674a0c9973029258e0ae83') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('AGENT_WISE_COMMISSION')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '80232b7e40ac481cad5576db06745c36') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('VW_COMMISSION')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'f2bda95bc5914a20bed57439b38d043c') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('AGENT_PERCENTAGE_WISE_COMMISSION')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '30f3875dd3e7423fb31947a9b26d6ae1') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENE_SUBMIT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '418b640684bf48698b03fbbec327df2c') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('CUSTOMER_RANDOMNUMBER')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '287f80f84c754fecae2d07d4f159b60b') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('BENE_RANDOMNUMBER')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '287f80f84c754fecae2d07d4f159b60b') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('REGISTERED_CUSTOMER')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'b90e3f32f6a84640ac9c8c81eb890d64') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('REGISTERED_BENE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '0cb0caf2c02241638b19a4f4f7e9a1d3') });
            // console.log(request);
            return next.handle(request);
        }

        if (request.headers.has('log')) {
            const request = req.clone({ headers: req.headers.set('No-Auth', 'True') });
            return next.handle(request);
        }

        if (request.headers.has('DD_DESTINATION_COUNTRY')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '76f78676000449d79673b036bc0abe21') });
            return next.handle(request);
        }

        if (request.headers.has('DD_PAYMENT_MODE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'db67219b40674be3bc4b49d94c777e17') });
            return next.handle(request);
        }

        if (request.headers.has('DD_DESTINATION_CITY')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '037a0dc632004a1fac8cf9c1113f5afe') });
            return next.handle(request);
        }

        if (request.headers.has('DD_RECOMMENDED_AGENT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '549a04d758ca4f03bce5d01a30560b32') });
            return next.handle(request);
        }

        if (request.headers.has('AGENT_DETAILS')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'f4b1b93d736740bf843ec0939f9aeb55') });
            return next.handle(request);
        }

        if (request.headers.has('LOAD_SENDER_AVAIL_CREDIT_LIMIT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'b6147af959844d4385b0cf649443272a') });
            return next.handle(request);
        }

        if (request.headers.has('RATE_PER_CREDIT_LIMIT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '98326ec925e24963b61a8929f4569696') });
            return next.handle(request);
        }

        if (request.headers.has('DD_PURPOSE_OF_TRANSFER')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'e3f60bf3f1c44dcbacf64222a7a15e8a') });
            return next.handle(request);
        }

        if (request.headers.has('DD_DESTINATION_CURRENCY')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '94d45dcf854b4951842dcdbc3d99ccb1') });
            return next.handle(request);
        }

        if (request.headers.has('DAILY_RATES')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'a9fac632c12a4515a4d9465fcd710304') });
            return next.handle(request);
        }

        if (request.headers.has('DAILY_RATES_2')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '8c16099d83ed4f9b87ebbfe7a0e49427') });
            return next.handle(request);
        }

        if (request.headers.has('RECEIVE_AGENT_SETTLEMENT_CURR')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '1c9a22d7c2aa4bc28f2f100bed17895d') });
            return next.handle(request);
        }

        if (request.headers.has('SENDING_AGENT_CURRENCY_EXCHANGE_RATE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '62456fd03b5e4c8eb89edfcd88bd42c6') });
            return next.handle(request);
        }

        if (request.headers.has('CURRENCY_EXCHANGE_RATE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'acda0150c6ed4e9c8096dfe49d683b65') });
            return next.handle(request);
        }
        
        if (request.headers.has('TR_MASTER_TEMP')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '1d50e423fab349d5b6bfb013f248e5f4') });
            return next.handle(request);
        }

        if (request.headers.has('SP_TRMASTER')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'b37c015e751540dd9f189af929babaa2') });
            return next.handle(request);
        }


        if (request.headers.has('CURR_US_WISE_EXCHANGE_RATE')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '3aaa081fb04441d18d9c45d7f6de35ca') });
            return next.handle(request);
        }

        if (request.headers.has('COUNTRY')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '77c3f92498c341f8b129445951ec1868') });
            return next.handle(request);
        }

        if (request.headers.has('EXCHANGE_BRANCH_PAYOUT')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '9aef1af395694664b3cfea5bac2dd43f') });
            return next.handle(request);
        }

        if (request.headers.has('SENDING_AGENT_CURRENCY_EXCHANGE_RATE_2')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', '797115e47b5e44158ed02cf825afd19b') });
            return next.handle(request);
        }

        if (request.headers.has('AGENT_SETTLEMENT_CURRENCY')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'e703a56acf8346c084cb8af0c578cb3a') });
            return next.handle(request);
        }

        if (request.headers.has('AGENT_BRANCH_DETAILS')) {
            const request = req.clone({ headers: req.headers.set('93e8e529aa0ab2321e76b1a114ebc999', 'f4b1b93d736740bf843ec0939f9aeb55') });
            return next.handle(request);
        }
    }

}


