import { Injectable } from "@angular/core";
import { Router } from '@angular/router';
import { ToastrService } from "ngx-toastr";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { HandleErrorService } from "./HandleErrorService";

@Injectable()

export class InterceptorService implements HttpInterceptor{
  constructor(private error: HandleErrorService,
    private router: Router,private toastrs: ToastrService){}

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    // returning an observable to complete the request cycle
    return new Observable((observer) => {
      next.handle(req).subscribe(
        (res: HttpResponse<any>) => {
          if (res instanceof HttpResponse) {
            observer.next(res);
          }
        },
        (err: HttpErrorResponse) => {
          console.log(err);
          this.toastrs.error(err.error.status_msg.message);
      // this.router.navigateByUrl('/miscellaneous/not-authorized-with-image');
          this.error.handleError(err);
        }
      );
    });
  }
}