import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalyticsComponent } from './components/pages/dashboard/analytics/analytics.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard/dashboard.component';
import { EcommerceComponent } from './components/pages/dashboard/ecommerce/ecommerce.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { InboxComponent } from './components/pages/app-email/inbox/inbox.component';
import { ReadComponent } from './components/pages/app-email/read/read.component';
import { ComposeComponent } from './components/pages/app-email/compose/compose.component';
import { AppEmailComponent } from './components/pages/app-email/app-email/app-email.component';
import { TableComponent } from './components/pages/table/table/table.component';
import { AuthenticationComponent } from './components/pages/authentication/authentication/authentication.component';
import { LoginWithImageComponent } from './components/pages/authentication/login-with-image/login-with-image.component';
import { RegisterComponent } from './components/pages/authentication/register/register.component';
import { ForgotPasswordComponent } from './components/pages/authentication/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/pages/authentication/reset-password/reset-password.component';
import { SessionLockScreenComponent } from './components/pages/authentication/session-lock-screen/session-lock-screen.component';
import { MiscellaneousComponent } from './components/pages/miscellaneous/miscellaneous/miscellaneous.component';
import { NotAuthorizedComponent } from './components/pages/miscellaneous/not-authorized/not-authorized.component';
import { MaintenanceComponent } from './components/pages/miscellaneous/maintenance/maintenance.component';
import { ComingSoonComponent } from './components/pages/miscellaneous/coming-soon/coming-soon.component';
import { Errorv1Component } from './components/pages/error/errorv1/errorv1.component';
import { ErrorComponent } from './components/pages/error/error/error.component';
import { Errorv3Component } from './components/pages/error/errorv3/errorv3.component';
import { DCR } from './components/pages/customer_registration/customer.component';
import {DBR} from './components/pages/beneficiary_registration/beneficiary.component';
import { TXN } from './components/pages/Txn/Txn.component';
import { REPORTS } from './components/pages/report/report.component';
import { MENU } from './components/pages/submenu/submenu.component';
import { ReportComponent } from './components/pages/report/report/report.component';
import { ReportOneComponent } from './components/pages/report/report1/reportone.component';
import {TRANSFER} from './components/pages/transfer/transfer.component';
import { LANDING } from './components/pages/landing/landing.component';
import { Newuser } from './components/pages/newuser/newuser.component';
import { Demosuper } from './components/pages/demosuper/demo.component';
import { Menuone } from './components/pages/demo/menuone/menuone.component';
import { Menutwo } from './components/pages/demo/menutwo/menutwo.component';
import { Menuthree } from './components/pages/demo/menuthree/menuthree.component';
import { Menufour } from './components/pages/demo/menufour/menufour.component';
import { Menufive } from './components/pages/demo/menufive/menufive.component';
import { Menusix } from './components/pages/demo/menusix/menusix.component';
import { Menuseven } from './components/pages/demo/menuseven/menuseven.component';
import { Menueight } from './components/pages/demo/menueight/menueight.component';
import { Menunine } from './components/pages/demo/menunine/menunine.component';
import { Menuten } from './components/pages/demo/menuten/menuten.component';
import { Menueleven } from './components/pages/demo/menueleven/menueleven.component';
import { Menutwel } from './components/pages/demo/menutwelev/menutwel.component';
import { Menuparent } from './components/pages/demo/demomenuparent/menuparent.component';

import { Treasuremenuparent } from './components/pages/Treasuremenu/treasureparent/treasureparent.component';
import { treasureone } from './components/pages/Treasuremenu/treasure1/treasureone.component';
import { treasuretwo } from './components/pages/Treasuremenu/treasure2/treasuretwo.component';
import { treasurethree } from './components/pages/Treasuremenu/treasure3/treasurethree.component';
import { treasurefour } from './components/pages/Treasuremenu/treasure4/treasurefour.component';
import { treasurefive } from './components/pages/Treasuremenu/treasure5/treasurefive.component';
import { treasuresix } from './components/pages/Treasuremenu/treasure6/treasuresix.component';
import { treasureseven } from './components/pages/Treasuremenu/treasure7/treasureseven.component';
import { treasureeight } from './components/pages/Treasuremenu/treasure8/treasureeight.component';
import { treasurenine } from './components/pages/Treasuremenu/treasure9/treasurenine.component';
import { treasureten } from './components/pages/Treasuremenu/treasure10/treasureten.component';
import { treasureeleven } from './components/pages/Treasuremenu/treasure11/treasureeleven.component';
import { treasuretwel } from './components/pages/Treasuremenu/treasure12/treasuretwel.component';
import { treasurethirt } from './components/pages/Treasuremenu/treasure13/treasurethirt.component';
import { treasurefourt } from './components/pages/Treasuremenu/treasure14/treasurefourt.component';
import { treasurefift } from './components/pages/Treasuremenu/treasure15/treasurefift.component';
import { treasuresixte } from './components/pages/Treasuremenu/treasure16/treasuresixte.component';
import { treasurest } from './components/pages/Treasuremenu/treasure17/treasurest.component';
import { treasureeighteen } from './components/pages/Treasuremenu/treasure18/treasureeighteen.component';
import { treasurenineteen } from './components/pages/Treasuremenu/treasure19/treasurenineteen.component';
import { Treasuredashboard } from './components/pages/Treasuremenu/treasuredash/treasuredashboard.component';

import { Customermenuparent } from './components/pages/Customerservicemenu/customer_sparent/customerparent.component';
import { Customerone } from './components/pages/Customerservicemenu/customer1/customerone.component';
import { Customertwo } from './components/pages/Customerservicemenu/customer2/customertwo.component';
import { Customerthree } from './components/pages/Customerservicemenu/customer3/customerthree.component';
import { Customerfour } from './components/pages/Customerservicemenu/customer4/customerfour.component';
import { Customerfive } from './components/pages/Customerservicemenu/customer5/customerfive.component';
import { Customersix } from './components/pages/Customerservicemenu/customer6/customersix.component';
import { Customerseven } from './components/pages/Customerservicemenu/customer7/customerseven.component';
import { Customereight } from './components/pages/Customerservicemenu/customer8/customereight.component';
import { Customernine } from './components/pages/Customerservicemenu/customer9/customernine.component';
import { Customerten } from './components/pages/Customerservicemenu/customer10/customerten.component';
import { Customereleven } from './components/pages/Customerservicemenu/customer11/customereleven.component';
import { Customertwel } from './components/pages/Customerservicemenu/customer12/customertwel.component';
import { Customerthirt } from './components/pages/Customerservicemenu/customer13/customerthirt.component';
import { Customerfourt } from './components/pages/Customerservicemenu/customer14/customerfourt.component';

import { MSmenuparent } from './components/pages/MSmenu/MSparentmenu/msparent.component';
import { MSone } from './components/pages/MSmenu/msmenu1/msmenuone.component';
import { MStwo } from './components/pages/MSmenu/msmenu2/msmenutwo.component';
import { MSthree } from './components/pages/MSmenu/msmenu3/msmenuthree.component';
import { MSfour } from './components/pages/MSmenu/msmenu4/msmenufour.component';
import { MSfive } from './components/pages/MSmenu/msmenu5/msmenufive.component';
import { MasterOneComponent } from './components/pages/masters/masterui_one/master_one.component';
import { SummaryComponent } from './components/pages/report_summary/summary.component';
import { MasterTwoComponent } from './components/pages/masters/masterui_two/mastertwo.component';
import { AuthGuard } from './auth.guard';



const routes: Routes = [
  {path: '', component: LoginWithImageComponent},
  {
    path: 'dashboard', component: DashboardComponent,
    children: [
      {path: 'analytics', data: { breadcrumb: 'Analytics' }, component: AnalyticsComponent},
      {path: 'e-commerce', data: { breadcrumb: 'eCommerce' }, component: EcommerceComponent},
    ]
  },
  {
    path: 'msmenu', component: MSmenuparent,
    children: [
      {path: 'msone', component: MSone},
      {path: 'mstwo', component: MStwo},
      {path: 'msthree', component: MSthree},
      {path: 'msfour', component: MSfour},
      {path: 'msfive', component: MSfive},
    ]
  },
  {
  path: 'treasuremenu', component: Treasuremenuparent,
  children: [
    {path: 'treasuredashboard',component: Treasuredashboard},
    {path: 'treasureone',component: treasureone},
    {path: 'treasuretwo', component: treasuretwo},
    {path: 'treasurethree',component: treasurethree},
    {path: 'treasurefour',component: treasurefour},
    {path: 'treasurefive',component: treasurefive},
    {path: 'treasuresix',component: treasuresix},
    {path: 'treasureseven',component: treasureseven},
    {path: 'treasureeight',component: treasureeight},
    {path: 'treasurenine',component: treasurenine},
    {path: 'treasureten',component: treasureten},
    {path: 'treasureeleven',component: treasureeleven},
    {path: 'treasuretwel',component: treasuretwel},
    {path: 'treasurethirt',component: treasurethirt},
    {path: 'treasurefourt',component: treasurefourt},
    {path: 'treasurefift',component: treasurefift},
    {path: 'treasuresixte',component: treasuresixte},
    {path: 'treasurest',component: treasurest},
    {path: 'treasureeighteen',component: treasureeighteen},
    {path: 'treasurenineteen',component: treasurenineteen},
  ]
},
  {
    path: 'customermenu', component: Customermenuparent,
    children: [
      {path: 'customerone',component: Customerone},
      {path: 'customertwo', component: Customertwo},
      {path: 'customerthree',component: Customerthree},
      {path: 'customerfour',component: Customerfour},
      {path: 'customerfive',component: Customerfive},
      {path: 'customersix',component: Customersix},
      {path: 'customerseven',component: Customerseven},
      {path: 'customereight',component: Customereight},
      {path: 'customernine',component: Customernine},
      {path: 'customerten',component: Customerten},
      {path: 'customereleven',component: Customereleven},
      {path: 'customertwel',component: Customertwel},
      {path: 'customerthirt',component: Customerthirt},
      {path: 'customerfourt',component: Customerfourt},
  ]
},
  {path: 'menu', component: Menuparent,
  children: [
    {path: 'newuser',component: Newuser},
    {path: 'menuone',component: Menuone},
    {path: 'menutwo', component: Menutwo},
    {path: 'menuthree',component: Menuthree},
    {path: 'menufour',component: Menufour},
    {path: 'menufive',component: Menufive},
    {path: 'menusix',component: Menusix},
    {path: 'menuseven',component: Menuseven},
    {path: 'menueight',component: Menueight},
    {path: 'menunine',component: Menunine},
    {path: 'menuten',component: Menuten},
    {path: 'menueleven',component: Menueleven},
    {path: 'menutwel',component: Menutwel},
  ]},
//   {path: 'landing', data: { breadcrumb: 'Transfer' }, component: LANDING},
  {
    path: 'master', component: AppEmailComponent,
    children: [
      {path: 'masterone',component: MasterOneComponent},
      {path: 'mastertwo',component: MasterTwoComponent},
    ]
  },
  {path: 'demo', data: { breadcrumb: 'Transfer' }, component: Demosuper},
  {
    path: 'app-email', component: AppEmailComponent,
    children: [
      {path: 'summary',component: SummaryComponent},
      {path: 'landing', data: { breadcrumb: 'Transfer' }, component: LANDING},
      {path: 'txn', data: { breadcrumb: 'Transfer' },canActivate:[AuthGuard], component: TXN},
      {path: 'transfer', data: { breadcrumb: 'Transfer' }, component: TRANSFER},
      {path: 'customer-registration', data: { breadcrumb: 'Customer' }, component: DCR},
      {path: 'beneficiary-registration', data: { breadcrumb: 'Beneficiary' },  component: DBR},
      {path: 'inbox', data: { breadcrumb: 'Inbox' }, component: InboxComponent},
      {path: 'read', data: { breadcrumb: 'Email Read' }, component: ReadComponent},
      {path: 'compose', data: { breadcrumb: 'Email Compose' }, component: ComposeComponent},
      {path: 'reports', data: { breadcrumb: 'Report' }, component: REPORTS},
    ]
  },

  {path: 'reports', data: { text: 'reports' }, component: MENU,
  children: [
    { path: 'payOutSummary', component: ReportComponent},
    { path: 'Agent', component: ReportOneComponent}
  ]},
  {
    path: 'table', component: TableComponent,
  },
  {path: 'profile', component: ProfileComponent},
  {
    path: 'authentication', component: AuthenticationComponent,
    children: [
      {path: 'login-with-image', data: { breadcrumb: 'Login with Image' }, component: LoginWithImageComponent},
      {path: 'register', data: { breadcrumb: 'Register' }, component: RegisterComponent},
      {path: 'forgot-password', data: { breadcrumb: 'Forgot Password' }, component: ForgotPasswordComponent},
      {path: 'reset-password', data: { breadcrumb: 'Reset Password' }, component: ResetPasswordComponent},
      {path: 'lock-screen', data: { breadcrumb: 'Lock Screen' }, component: SessionLockScreenComponent},
    ]
  },
  {
    path: 'miscellaneous', component: MiscellaneousComponent,
    children: [
      {path: 'not-authorized', data: { breadcrumb: 'Not Authorized' }, component: NotAuthorizedComponent},
      {path: 'maintenance', data: { breadcrumb: 'Maintenance' }, component: MaintenanceComponent},
      {path: 'coming-soon', data: { breadcrumb: 'Coming Soon' }, component: ComingSoonComponent},
    ]
  },
  {
    path: 'error', component: ErrorComponent,
    children: [
      {path: 'errorv1', data: { breadcrumb: '404 Error' }, component: Errorv1Component},
      {path: 'errorv3', data: { breadcrumb: '500 Error with Image' }, component: Errorv3Component},
    ]
  },
  {path: '**', redirectTo: ''},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
