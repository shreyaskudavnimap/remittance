import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class ShareddataService {

  private apiData = new BehaviorSubject<any>(null);
  public apiData$ = this.apiData.asObservable();

  constructor() { }

  // setData(data: string) { 
  //   var a = data
  //   // console.log(a);
  //   this.apiData.next(a)
  //   // console.log(this.apiData)
  //   // Ag58cdtN3vPsRpvfJRmEeg==
  // }

  setData(data: any){ 
    this.apiData.next(data);
  }

}
