import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Router, NavigationCancel, NavigationEnd } from '@angular/router';
import { Location, LocationStrategy, NgIf, PathLocationStrategy } from '@angular/common';
import { filter, startWith, switchMap } from 'rxjs/operators';
import { fromEvent, merge, Subject, timer } from 'rxjs';
declare let $: any;


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [
        Location, {
            provide: LocationStrategy,
            useClass: PathLocationStrategy
        }
    ]
})
export class AppComponent implements OnInit, OnDestroy {
    location: any;
    routerSubscription: any;


    constructor(private router: Router) {
        const mouseMove$ = fromEvent<MouseEvent>(document, 'mousemove');
        const keyPress$ = fromEvent<KeyboardEvent>(document, 'keyup');
        const mergeBothEvents = merge(keyPress$, mouseMove$);
        setInterval(()=>{
        const interval$ = timer(240000);


        mergeBothEvents
            .pipe(
            startWith('initial'),
            switchMap(() => {
                return interval$;
            })
            )
            .subscribe(() => {
                localStorage.clear();
                this.router.navigate(['']);
                console.log('user Inactive!!');
            });

        },1000);

    }

    ngOnInit(){
        this.recallJsFuntions();
        // this.router.navigate(['']);  //THIS TO SEND USER TO LOGIN PAGE ON REFRESH
    }

    recallJsFuntions() {
        this.routerSubscription = this.router.events
            .pipe(filter(event => event instanceof NavigationEnd || event instanceof NavigationCancel))
            .subscribe(event => {
                $.getScript('../assets/js/custom.js');
                this.location = this.router.url;
                if (!(event instanceof NavigationEnd)) {
                    return;
                }
                window.scrollTo(0, 0)
            });
    }

    ngOnDestroy() {
        this.routerSubscription.unsubscribe();
    }
}
