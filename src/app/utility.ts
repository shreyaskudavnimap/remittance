import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
    providedIn: 'root',
  })

export class Utility {

    hashPassword = '9EB0EF699C3A41B9BA2E66CC75453670';

    public Decrypt(data: string) {
        return CryptoJS.AES.decrypt(data.trim(), this.hashPassword.trim()).toString(CryptoJS.enc.Utf8);
    }

    public Crypt(data: string) {
        return CryptoJS.AES.encrypt(data.trim(), this.hashPassword.trim()).toString();
    }

    public DecryptJson(data: string) {
        return JSON.parse(CryptoJS.AES.decrypt(data.trim(), this.hashPassword.trim()).toString(CryptoJS.enc.Utf8));
    }

    public CryptJson(data: string) {
        return JSON.parse(CryptoJS.AES.encrypt(data.trim(), this.hashPassword.trim()).toString());
    }
}