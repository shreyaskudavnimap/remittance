import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as data from '../assets/config/config.json';
import axios from 'axios';
import * as forge from 'node-forge'
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
import { HttpClientService } from './header.service';



@Injectable({
  providedIn: 'root'
})
export class RootService implements OnInit {

  COMPLAINCE_DATA: any;
  COMPLAINCE_MENUTHREETWO
  COMPLAINCE_AB
  AGENT_ID
  COMPLAINCE_MENUFOURR
  COMPLAINCE_Submit
  Submit_complaince
  _COMPLA_MENUTWO
  _TRANSFER_CID
  _TRANSFER_RECCOM
  _TRANSFERMOP
  _CUSTOMER_IDTYPE
  _TRANSFER_RECEIVE_CURRENCY
  _CUSTOMER_REGISTER
  _BENE_BRANCH
  _BENE_MODE
  _BENE_Submit
  _GenrateRandomCustomer
  _GenrateRandomtranscation
  _GenrateRandombene
  BENE_LIST
  _Transaction_send

  x: string;

  private eam
  private UserId: number;

  filter_send
  filter_encryptmessage
  filter_encrypt
  _TRANSFER_CURR


  private test: any;
  intermediateJson = data;
  data: any;
  encryptedMessage!: string;
  encrypt!: string;
  password = '9EB0EF699C3A41B9BA2E66CC75453670';

  data_Send = this.intermediateJson._BAL;
  s = JSON.stringify(this.data_Send)
  data_send1 = this.intermediateJson._CAL;
  z = JSON.stringify(this.data_send1)
  data_sendmenu = this.intermediateJson._MENU;
  menus = JSON.stringify(this.data_sendmenu);
  data_filter = this.intermediateJson._FILTER;
  filter = JSON.stringify(this.data_filter)
  data_filtertwo = this.intermediateJson._FILTERTWO;
  filtertwo = JSON.stringify(this.data_filtertwo);
  COMPLAINCE_COUNTRY = this.intermediateJson.COMPLAINCE_COUNTRY;
  COMPLAINCEC_FILTER = JSON.stringify(this.COMPLAINCE_COUNTRY);


  public_key: string = '-----BEGIN PUBLIC KEY-----\n' +
    'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBxKuHN0bkshWZB10QIr0gqBkj\n' +
    '+BiFJHknE2C6WgJyL8yJ/Wd5NTEwv9IQI16N/Dyv0Z7l5LKCePDSF/hRjeiMnkNW\n' +
    'X1ntqrPXbx3SHQUzN7HDTCcSR7vLtL/PV7w0nKmIJ2n+qRqGWGtrM0uBnapyVxyI\n' +
    'CzfQZFyq/KjT7C4glwIDAQAB\n' +
    '-----END PUBLIC KEY-----';


  public setUserId(value: number) {
    this.UserId = value;
    this.x = JSON.stringify(this.UserId)
    // console.log(this.x)
  }

  headers = new HttpHeaders({
    'Login': 'true',
  });
  options = { headers: this.headers };

  headers1 = new HttpHeaders({
    'One': 'true'
  });
  options1 = { headers: this.headers1 };

  headers2 = new HttpHeaders({
    'Two': 'true'
  });
  options2 = { headers: this.headers2 };

  headers3 = new HttpHeaders({
    'Countries': 'true'
  });
  options3 = { headers: this.headers3 };


  headers4 = new HttpHeaders({
    'filterone': 'yes'
  });
  options4 = { headers: this.headers4 };

  headers5 = new HttpHeaders({
    'filtertwo': 'yes'
  });
  options5 = { headers: this.headers5 };

  headers6 = new HttpHeaders({
    'finalfilter': 'yes'
  });
  options6 = { headers: this.headers6 };

  headers7 = new HttpHeaders({
    'Complaince': 'yes'
  });
  options7 = { headers: this.headers7 };

  headers8 = new HttpHeaders({
    'Cagent': 'yes'
  });
  options8 = { headers: this.headers8 };

  headers9 = new HttpHeaders({
    'COMPLAINCE_TWO': 'yes'
  });
  options9 = { headers: this.headers9 };

  headers10 = new HttpHeaders({
    'COMPLAINCE_MENUTWO': 'yes'
  });
  options10 = { headers: this.headers10 };

  headers11 = new HttpHeaders({
    'COMPLAINCE_MENUTHREE': 'yes'
  });
  options11 = { headers: this.headers11 };

  headers12 = new HttpHeaders({
    'COMPLAINCE_MENUTHREETWO': 'yes'
  });
  options12 = { headers: this.headers12 };

  headers13 = new HttpHeaders({
    'COMPLAINCE_MENUTHREETHREE': 'yes'
  });
  options13 = { headers: this.headers13 };

  headers14 = new HttpHeaders({
    'COMPLAINCE_MENUFour': 'yes'
  });
  options14 = { headers: this.headers14 };

  headers15 = new HttpHeaders({
    'COMPLAINCE_SUBMIT': 'yes'
  });
  options15 = { headers: this.headers15 };

  headers16 = new HttpHeaders({
    'COMPLAINCE_MENU_TWO': 'yes'
  });
  options16 = { headers: this.headers16 };

  headers17 = new HttpHeaders({
    'COMPLAINCE_MENU_SUBMIT': 'yes'
  });
  options17 = { headers: this.headers17 };

  headers18 = new HttpHeaders({
    'TRANSFER_POT': 'yes'
  });
  options18 = { headers: this.headers18 };

  headers19 = new HttpHeaders({
    'TRANSFER_RC': 'yes'
  });
  options19 = { headers: this.headers19 };

  headers20 = new HttpHeaders({
    'TRANSFER_CITY': 'yes'
  });
  options20 = { headers: this.headers20 };

  headers21 = new HttpHeaders({
    'TRANSFER_MOP': 'yes'
  });
  options21 = { headers: this.headers21 };

  headers22 = new HttpHeaders({
    'TRANSFER_CURR': 'yes'
  });
  options22 = { headers: this.headers22 };

  headers23 = new HttpHeaders({
    'TRANSFER_RECC': 'yes'
  });
  options23 = { headers: this.headers23 };

  headers24 = new HttpHeaders({
    'CUSTOMER_NATIONALITY': 'yes'
  });
  options24 = { headers: this.headers24 };

  headers25 = new HttpHeaders({
    'CUSTOMER_COUNTRY': 'yes'
  });
  options25 = { headers: this.headers25 };

  headers26 = new HttpHeaders({
    'CUSTOMER_IDTYPE': 'yes'
  });
  options26 = { headers: this.headers26 };

  headers27 = new HttpHeaders({
    'transfer_currencyreceive': 'yes'
  });
  options27 = { headers: this.headers27 };

  headers28 = new HttpHeaders({
    'CUSSTOMER_SOURCE': 'yes'
  });
  options28 = { headers: this.headers28 };

  headers29 = new HttpHeaders({
    'CUSSTOMER_REGISTER': 'yes'
  });
  options29 = { headers: this.headers29 };

  headers30 = new HttpHeaders({
    'BENE_REGISTER': 'yes'
  });
  options30 = { headers: this.headers30 };

  headers31 = new HttpHeaders({
    'BENE_BRANCH': 'yes'
  });
  options31 = { headers: this.headers31 };

  headers32 = new HttpHeaders({
    'BENE_MODE': 'yes'
  });
  options32 = { headers: this.headers32 };

  headers33 = new HttpHeaders({
    'BENE_PURPOSE': 'yes'
  });
  options33 = { headers: this.headers33 };

  headers34 = new HttpHeaders({
    'BENE_IDTYPE': 'yes'
  });
  options34 = { headers: this.headers34 };

  headers35 = new HttpHeaders({
    'BENE_RELATION': 'yes'
  });
  options35 = { headers: this.headers35 };

  headers36 = new HttpHeaders({
    'BENE_SUBMIT': 'yes'
  });
  options36 = { headers: this.headers36 };

  headers37 = new HttpHeaders({
    'CUSTOMER_RANDOMNUMBER': 'yes'
  });
  options37 = { headers: this.headers37 };

  headers38 = new HttpHeaders({
    'BENE_RANDOMNUMBER': 'yes'
  });
  options38 = { headers: this.headers38 };

  headers39 = new HttpHeaders({
    'REGISTERED_CUSTOMER': 'yes'
  });
  options39 = { headers: this.headers39 };

  headers40 = new HttpHeaders({
    'REGISTERED_BENE': 'yes'
  });
  options40 = { headers: this.headers40 };

  headers45 = new HttpHeaders({
    'DD_DESTINATION_COUNTRY': 'yes'
  });
  options45 = { headers: this.headers45 };

  headers46 = new HttpHeaders({
    'DD_PAYMENT_MODE': 'yes'
  });
  options46 = { headers: this.headers46 };

  headers47 = new HttpHeaders({
    'DD_DESTINATION_CITY': 'yes'
  });
  options47 = { headers: this.headers47 };

  headers41 = new HttpHeaders({
    'LOG': 'yes'
  });
  options41 = { headers: this.headers41 };

  headers42 = new HttpHeaders({
    'TRANSACTION_NUM': 'yes'
  });
  options42 = { headers: this.headers42 };

  headers43 = new HttpHeaders({
    'Transaction_sent': 'yes'
  });
  options43 = { headers: this.headers43 };

  constructor(private http: HttpClient, private router: Router, private ht: HttpClientService) { }

  ngOnInit(): void {
  }

  //DROPDOWN FOR BENEFICIARY REGITARATION
  BENE_RELA = { "query": "Dummy" };
  BENE_R = JSON.stringify(this.BENE_RELA);
  BENE_RELATION = CryptoJS.AES.encrypt(this.BENE_R.trim(), this.password.trim()).toString();

  //LIST OF THE REGISTERED CUSTOMER FROM THE SERVER
  CUSTOMER_LIST = CryptoJS.AES.encrypt(this.BENE_R.trim(), this.password.trim()).toString();

  //DROPDOWN FOR BENEFICIARY REGITARATION
  BENE_id = { "query": "Dummy" };
  BENE_IDS = JSON.stringify(this.BENE_id);
  BENE_IDTYPE = CryptoJS.AES.encrypt(this.BENE_IDS.trim(), this.password.trim()).toString();

  //DROPDOWN FOR BENEFICIARY REGITARATION
  BENE_ = { "query": "Dummy" };
  BENE_P = JSON.stringify(this.BENE_);
  BENE_PURP = CryptoJS.AES.encrypt(this.BENE_P.trim(), this.password.trim()).toString();

  //DROPDOWN FOR BENEFICIARY REGITARATION
  BENE_Q = { "query": "Dummy" };
  BENE_S = JSON.stringify(this.BENE_Q);
  BENE_PAYB = CryptoJS.AES.encrypt(this.BENE_S.trim(), this.password.trim()).toString();

  //DROPDOWN FOR CUSTOMER COUNTRY
  CUSTOMER_Q = { "query": "Dummy" };
  CUSTOMER_S = JSON.stringify(this.CUSTOMER_Q);
  CUSTOMER_SOI = CryptoJS.AES.encrypt(this.CUSTOMER_S.trim(), this.password.trim()).toString();


  //DROPDOWN FOR CUSTOMER COUNTRY
  CUSTOMER_COUNTRY = { "query": "Dummy" };
  CUSTOMER_C = JSON.stringify(this.CUSTOMER_COUNTRY);
  _CUSTOMER_COUNTRIES = CryptoJS.AES.encrypt(this.CUSTOMER_C.trim(), this.password.trim()).toString();

  //DROPDOWN FOR CUSTOMER REGISTRATION NATIONALITY
  CUSTOMER = { "query": "Dummy" };
  CUSTOMER_NATION = JSON.stringify(this.CUSTOMER);
  _CUSTOMER_NATIONALITY = CryptoJS.AES.encrypt(this.CUSTOMER_NATION.trim(), this.password.trim()).toString();

  //DROPDOWN FOR TRANSFER
  TRANSFER_Q = { "query": "Dummy" };
  _TRANSFER_C = JSON.stringify(this.TRANSFER_Q);
  _TRANSFERRC = CryptoJS.AES.encrypt(this._TRANSFER_C.trim(), this.password.trim()).toString();


  //DROPDOWN FOR TRANSFER
  TRANSFER_QUERY = { "query": "Dummy" };
  _TRANSFER = JSON.stringify(this.TRANSFER_QUERY);
  _TRANSFERPOT = CryptoJS.AES.encrypt(this._TRANSFER.trim(), this.password.trim()).toString();

  //COMPLAINCE USER SECOND REPORT FILTER
  _COMPLAINCE = { "query": "AgentPayOutSummary_AgentList", "params": { "variables": ["Agent_ID"], "values": ["0"] }, "type": "P" };
  _COMPLAINCE_T = JSON.stringify(this._COMPLAINCE);
  _COMPLAINCE_MENU = CryptoJS.AES.encrypt(this._COMPLAINCE_T.trim(), this.password.trim()).toString();

  //COMPLAINCE USER THIRD REPORT FILTER
  COMPLAINCE_THREE = { "UserID": "1001049" };
  COMPLAINCE_TH = JSON.stringify(this.COMPLAINCE_THREE);
  _COMPLAINCE_THREE = CryptoJS.AES.encrypt(this.COMPLAINCE_TH.trim(), this.password.trim()).toString();

  //COMPLAINCE USER THIRD REPORT FILTER
  COMPLAINCE_THREEFC = { "UserID": "1001049" };
  COMPLAINCE_T = JSON.stringify(this.COMPLAINCE_THREEFC);
  _COMPLAINCE_THREEFC = CryptoJS.AES.encrypt(this.COMPLAINCE_T.trim(), this.password.trim()).toString();

  // report component data encryption
  e = CryptoJS.AES.encrypt(this.s.trim(), this.password.trim()).toString();
  // console.log(this.encryptedMessage);
  rsa = forge.pki.publicKeyFromPem(this.public_key);
  en = window.btoa(this.rsa.encrypt(this.password));
  // console.log(this.encrypt);

  // report component data encryption
  ea = CryptoJS.AES.encrypt(this.z.trim(), this.password.trim()).toString();

  // menu component data encryption
  eas = CryptoJS.AES.encrypt(this.menus.trim(), this.password.trim()).toString();

  // menu component data encryption
  filter_data = CryptoJS.AES.encrypt(this.filter.trim(), this.password.trim()).toString();

  // menu component data encryption
  filter_datatwo = CryptoJS.AES.encrypt(this.filtertwo.trim(), this.password.trim()).toString();

  //  TO GET THE COMPLAINCE COUNTRIES
  COUNTRY_C = CryptoJS.AES.encrypt(this.COMPLAINCEC_FILTER.trim(), this.password.trim()).toString();


  public setBest(value) {
    const best = value;
    console.log(best)
    const query = { "BaseCountryID": `${best}` };
    const countries = JSON.stringify(query)
    // getting countries  data encryption
    this.eam = CryptoJS.AES.encrypt(countries.trim(), this.password.trim()).toString();
  }
  public setTest(val) {
    this.test = val;
  }

  public value(val) {
    const a = val.name;
    const b = val.password;
    this.data = {
      "P1MD6nizSA": {
        "Username": a, "Password": b
      }
    };
    const c = JSON.stringify(this.data)
    console.log(c);

    this.encryptedMessage = CryptoJS.AES.encrypt(c.trim(), this.password.trim()).toString();
    // console.log(this.encryptedMessage);
    var rsa = forge.pki.publicKeyFromPem(this.public_key);
    this.encrypt = window.btoa(rsa.encrypt(this.password));
    // console.log(this.encrypt);
  }

  public filt(val) {
    const selecteNationality = val.selecteNationality;
    const selectedCurrency = val.selectedCurrency;
    const time = val.time;
    const timeto = val.timeto;
    console.log(selecteNationality, selectedCurrency, time, timeto);
    this.filter_send = { "values": { "currency": selectedCurrency, "nationality": selecteNationality, "time": time, "timeto": timeto } };
    const y = JSON.stringify(this.filter_send)
    this.filter_encryptmessage = CryptoJS.AES.encrypt(y.trim(), this.password.trim()).toString();
    var rsa = forge.pki.publicKeyFromPem(this.public_key);
    this.filter_encrypt = window.btoa(rsa.encrypt(this.password));
  }

  // FILTER FOR COMPLAINCER USER MENU ONE
  public Complaince_user(val) {
    // console.log(val);
    const PAYOUT_COUNTRY = val.PAYOUT_COUNTRY
    const PAYOUT_AGENT = val.PAYOUT_AGENT
    const PAYOUT_AGENT_BRANCH = val.PAYOUT_AGENT_BRANCH
    const FROM_DATE = val.FROM_DATE
    const TO_DATE = val.TO_DATE
    const data = {
      'FROM_DATE': FROM_DATE,
      'TO_DATE': TO_DATE,
      'PAYOUT_COUNTRY': PAYOUT_COUNTRY,
      'PAYOUT_AGENT': PAYOUT_AGENT,
      'PAYOUT_AGENT_BRANCH': PAYOUT_AGENT_BRANCH
    }
    const sen = JSON.stringify(data);
    this.COMPLAINCE_DATA = CryptoJS.AES.encrypt(sen.trim(), this.password.trim()).toString();
  }

  // FILTER FOR COMPLAINCER USER MENU ONE
  public COMPLAINCE_AGENT(val) {
    console.log(val);
    const ID = { "countryid": `${val}` };
    console.log(ID);
    const AGENT_ID = JSON.stringify(ID);
    this.COMPLAINCE_AB = CryptoJS.AES.encrypt(AGENT_ID.trim(), this.password.trim()).toString();
    // console.log(this.COMPLAINCE_AB);
  }

  public COMPLAINCE_MENUTWO(val) {
    console.log(val);
    const valu = val;
    const COMPL = { "AgentID": `${valu}`, "BranchID": "13258" };
    const COMPLA = JSON.stringify(COMPL);
    this._COMPLA_MENUTWO = CryptoJS.AES.encrypt(COMPLA.trim(), this.password.trim()).toString();
  }

  public COMPLAINCE_MENUTHREE(val) {
    // console.log(val);
    const ID = {
      "Currency_Id": `${val}`,
      "UserID": "1001049"
    };
    // console.log(ID);
    const AGENT_ID = JSON.stringify(ID);
    this.COMPLAINCE_MENUTHREETWO = CryptoJS.AES.encrypt(AGENT_ID.trim(), this.password.trim()).toString();
  }


  public COMPLAINCE_MENUFOUR(val) {
    const ID = { "Currency_Id": `${val}` };
    console.log(ID);
    const AGENT_ID = JSON.stringify(ID);
    this.COMPLAINCE_MENUFOURR = CryptoJS.AES.encrypt(AGENT_ID.trim(), this.password.trim()).toString();
  }

  public COMPLAINCE_SUBMIT(val) {
    const ID = val;
    const AGENT_ID = JSON.stringify(ID);
    console.log(ID);
    this.COMPLAINCE_Submit = CryptoJS.AES.encrypt(AGENT_ID.trim(), this.password.trim()).toString();
  }

  public SEND_COMPLAINCE(data) {
    const Data = data;
    const AGENT_DATA = JSON.stringify(Data);
    // console.log(Data);
    this.Submit_complaince = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public TRANSFER_CID(val) {
    const ID = { "Country_Id": `${val}` };
    // console.log(ID);
    const AGENT_DATA = JSON.stringify(ID);
    this._TRANSFER_CID = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public TRANSFER_CURR(val) {
    const Agent_ID = { "AGENT_ID": `${val}` };
    // console.log(Agent_ID);
    const AGENT_DATA = JSON.stringify(Agent_ID);
    this._TRANSFER_CURR = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public RECCOMND(val) {
    const CityID = val.CityID;
    const CountryID = val.CountryID;
    const Data = {
      "CityID": `${CityID}`,
      "CountryID": `${CountryID}`
    };
    const AGENT_DATA = JSON.stringify(Data);
    this._TRANSFER_RECCOM = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public TRANSFER_MOP(val) {
    const ID = { "Country_Id": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this._TRANSFERMOP = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public CUSTOMER_ID(val) {
    const ID = { "CountryShortName": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this._CUSTOMER_IDTYPE = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public TRANSFER_RECEIVE_CURRENCY(val) {
    const ID = { "Agent_ID": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this._TRANSFER_RECEIVE_CURRENCY = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public CUSTOMER_REGISTER(val) {
    const ID = val;
    const AGENT_DATA = JSON.stringify(ID);
    this._CUSTOMER_REGISTER = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public BENE_BRANCH(val) {
    const ID = { "CountryShortName": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this._BENE_BRANCH = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public BENE_MODE(val) {
    const ID = { "CountryID": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this._BENE_MODE = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public BENE_SUBMIT(val) {
    const ID = val;
    const AGENT_DATA = JSON.stringify(ID);
    this._BENE_Submit = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public RANDOM_CUST(val) {
    const ID = { "partnercode": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this._GenrateRandomCustomer = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public RANDOM_BENE(val) {
    const ID = { "partnercode": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this._GenrateRandombene = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public CUSTOMER_BENE_DETAILS(val) {
    const ID = { "CustomerNo": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this.BENE_LIST = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public TRANS_NUM(val) {
    const ID = { "partnercode": `${val}` };
    const AGENT_DATA = JSON.stringify(ID);
    this._GenrateRandomtranscation = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  public Transaction_send(val) {
    const ID = val;
    const AGENT_DATA = JSON.stringify(ID);
    this._Transaction_send = CryptoJS.AES.encrypt(AGENT_DATA.trim(), this.password.trim()).toString();
  }

  Transaction_sent() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._Transaction_send, this: this.en }, this.options43)
  }

  TRANSACTION_NUM() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._GenrateRandomtranscation, this: this.en }, this.options42)
  }

  // LIST OF logfetcher
  _LOG() {
    return this.http.post<any>(`${this.intermediateJson.LOGFETCH}`, null, this.options41)
  }

  // LIST OF REGISTERED BENEFICIARY FROM THE SERVER
  _BENE_LIST() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.BENE_LIST, this: this.en }, this.options40)
  }

  _DD_DESTINATION_COUNTRY(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, this.options45)
  }

  _DD_PAYMENT_MODE(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, this.options46)
  }

  _DD_DESTINATION_CITY(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, this.options47)
  }

  _DD_RECOMMENDED_AGENT(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'DD_RECOMMENDED_AGENT': 'yes' }) })
  }

  _AGENT_DETAILS(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'AGENT_DETAILS': 'yes' }) })
  }

  _LOAD_SENDER_AVAIL_CREDIT_LIMIT(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'LOAD_SENDER_AVAIL_CREDIT_LIMIT': 'yes' }) })
  }

  _RATE_PER_CREDIT_LIMIT(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'RATE_PER_CREDIT_LIMIT': 'yes' }) })
  }

  _DAILY_RATES(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'DAILY_RATES': 'yes' }) })
  }

  _DD_PURPOSE_OF_TRANSFER(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'DD_PURPOSE_OF_TRANSFER': 'yes' }) })
  }

  _DD_DESTINATION_CURRENCY(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'DD_DESTINATION_CURRENCY': 'yes' }) })
  }

  _AGENT_BRANCH_DETAILS(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'AGENT_BRANCH_DETAILS': 'yes' }) })
  }

  _SENDING_AGENT_CURRENCY_EXCHANGE_RATE(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'SENDING_AGENT_CURRENCY_EXCHANGE_RATE': 'yes' }) })
  }

  CURR_US_WISE_EXCHANGE_RATE(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'CURR_US_WISE_EXCHANGE_RATE': 'yes' }) })
  }

  TR_MASTER_TEMP(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'TR_MASTER_TEMP': 'yes' }) })
  }

  SP_TRMASTER(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'SP_TRMASTER': 'yes' }) })
  }

  CURRENCY_EXCHANGE_RATE(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'CURRENCY_EXCHANGE_RATE': 'yes' }) })
  }

  COUNTRY(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'COUNTRY': 'yes' }) })
  }

  EXCHANGE_BRANCH_PAYOUT(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'EXCHANGE_BRANCH_PAYOUT': 'yes' }) })
  }

  _AGENT_SETTLEMENT_CURRENCY(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'AGENT_SETTLEMENT_CURRENCY': 'yes' }) })
  }

  _BANK_SENDING_CURRENCY_CER(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'BANK_SENDING_CURRENCY_CER': 'yes' }) })
  }

  _CURRENCY_DETAILS_1(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'CURRENCY_DETAILS_1': 'yes' }) })
  }

  BENEFICIARY_DATA(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'BENEFICIARY_DATA': 'yes' }) })
  }

  GET_PAYIN_USD_AMT(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'GET_PAYIN_USD_AMT': 'yes' }) })
  }

  GET_RANDOM_TRANSACTION_NUM(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'GET_RANDOM_TRANSACTION_NUM': 'yes' }) })
  }

  SUBMIT_TR_MASTER_TEMP(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'SUBMIT_TR_MASTER_TEMP': 'yes' }) })
  }

  DAILY_RATES_2(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'DAILY_RATES_2': 'yes' }) })
  }

  RECEIVE_AGENT_SETTLEMENT_CURR(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'RECEIVE_AGENT_SETTLEMENT_CURR': 'yes' }) })
  }

  SENDING_AGENT_CURRENCY_EXCHANGE_RATE_2(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'SENDING_AGENT_CURRENCY_EXCHANGE_RATE_2': 'yes' }) })
  }

  VAR_INFORMATION(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'VAR_INFORMATION': 'yes' }) })
  }

  CUSTOMER_DATA(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'CUSTOMER_DATA': 'yes' }) })
  }

  AGENT_PERCENTAGE_WISE_COMMISSION(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'AGENT_PERCENTAGE_WISE_COMMISSION': 'yes' }) })
  }

  RECEIVING_AGENT_PERCENTAGE_WISE_COMMISSION(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'RECEIVING_AGENT_PERCENTAGE_WISE_COMMISSION': 'yes' }) })
  }

  AGENT_WISE_COMMISSION(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'AGENT_WISE_COMMISSION': 'yes' }) })
  }

  VW_COMMISSION(payload) {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: payload, this: this.en }, { headers: new HttpHeaders({ 'VW_COMMISSION': 'yes' }) })
  }


  // LIST OF REGISTERED CUSTOMER FROM THE SERVER
  _CUSTOMER_LIST() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.CUSTOMER_LIST, this: this.en }, this.options39)
  }


  // GENERATE RANDOM NUMBER FOR BENEFICIARY
  _RANDOM_BENE() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._GenrateRandombene, this: this.en }, this.options38)
  }

  // GENERATE RANDOM NUMBER FOR CUSTOMER
  _RANDOM_CUSTOMER() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._GenrateRandomCustomer, this: this.en }, this.options37)
  }

  // DROPDOWN FOR BENE REGITRATION
  _BENE_SUBMIT() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._BENE_Submit, this: this.en }, this.options36)
  }

  // DROPDOWN FOR BENE REGITRATION
  _BENE_RELATION() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.BENE_RELATION, this: this.en }, this.options35)
  }


  // DROPDOWN FOR BENE REGITRATION
  _BENE_ID() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.BENE_IDTYPE, this: this.en }, this.options34)
  }

  // DROPDOWN FOR BENE REGITRATION
  _BENE_PURPOSE() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.BENE_PURP, this: this.en }, this.options33)
  }

  // DROPDOWN FOR BENE REGITRATION
  _BENE_MOD() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._BENE_MODE, this: this.en }, this.options32)
  }

  // DROPDOWN FOR BENE REGITRATION
  _BENE_BRANC() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._BENE_BRANCH, this: this.en }, this.options31)
  }

  // DROPDOWN FOR BENE REGITRATION
  _BENE_COUNTRY() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.BENE_PAYB, this: this.en }, this.options30)
  }

  // DROPDOWN FOR CUSTOMER REGITRATION
  _CUSTOMER_REGISTRATION() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._CUSTOMER_REGISTER, this: this.en }, this.options29)
  }

  // DROPDOWN FOR CUSTOMER REGITRATION
  _CUSTOMER_SOI() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.CUSTOMER_SOI, this: this.en }, this.options28)
  }

  // DROPDOWN FOR TRANSFER
  TRANSFERCURRENCY() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._TRANSFER_RECEIVE_CURRENCY, this: this.en }, this.options27)
  }

  // DROPDOWN FOR CUSTOMER REGISTRATION
  CUSTOMER_IDTYPE() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._CUSTOMER_IDTYPE, this: this.en }, this.options26)
  }

  // DROPDOWN FOR CUSTOMER REGISTRATION
  CUSTOMER_COUNTRIES() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._CUSTOMER_COUNTRIES, this: this.en }, this.options25)
  }

  // DROPDOWN FOR CUSTOMER REGISTRATION
  CUSTOMER_NATIONALITY() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._CUSTOMER_NATIONALITY, this: this.en }, this.options24)
  }

  // DROPDOWN FOR TRANSFER
  TRANSFER_RECCO() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._TRANSFER_RECCOM, this: this.en }, this.options23)
  }

  // DROPDOWN FOR TRANSFER
  TRANSFER_CUR() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._TRANSFER_CURR, this: this.en }, this.options22)
  }

  // DROPDOWN FOR TRANSFER
  _TRANSFER_MOP() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._TRANSFERMOP, this: this.en }, this.options21)
  }

  // DROPDOWN FOR TRANSFER
  _TRANSFER_CITY() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._TRANSFER_CID, this: this.en }, this.options20)
  }

  // DROPDOWN FOR TRANSFER
  _TRANSFER_RC() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._TRANSFERRC, this: this.en }, this.options19)
  }

  // DROPDOWN FOR TRANSFER
  _TRANSFER_POT() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._TRANSFERPOT, this: this.en }, this.options18)
  }


  // FILTER FOR COMPLAINCER USER MENU THREE
  _COMPLAINCE_MENUTWOSUBMIT() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.Submit_complaince, this: this.en }, this.options17)
  }

  // FILTER FOR COMPLAINCER USER MENU THREE
  _COMPLAINCE_SUBMIT() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.COMPLAINCE_Submit, this: this.en }, this.options15)
  }

  // FILTER FOR COMPLAINCER USER MENU THREE
  _COMPLAINCE_FILTERFOUR() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.COMPLAINCE_MENUFOURR, this: this.en }, this.options14)
  }


  // FILTER FOR COMPLAINCER USER MENU THREE
  _COMPLAINCE_FILTERTHREE() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._COMPLAINCE_THREEFC, this: this.en }, this.options13)
  }


  // FILTER FOR COMPLAINCER USER MENU THREE
  _COMPLAINCE_MENUTHREETWO() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.COMPLAINCE_MENUTHREETWO, this: this.en }, this.options12)
  }


  // FILTER FOR COMPLAINCER USER MENU THREE
  _COMPLAINCE_MENUTHREE() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._COMPLAINCE_THREE, this: this.en }, this.options11)
  }


  // FILTER FOR COMPLAINCER USER MENU TWO
  _COMPLAINCE_TWO() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._COMPLAINCE_MENU, this: this.en }, this.options9)
  }
  // FILTER FOR COMPLAINCER USER MENU TWO
  _COMPLAINCE_MENUTWO() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this._COMPLA_MENUTWO, this: this.en }, this.options16)
  }

  // FILTER FOR COMPLAINCER USER MENU ONE
  COMPLAINCE_AG() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.COMPLAINCE_AB, this: this.en }, this.options8)
  }

  // FILTER FOR COMPLAINCER USER MENU ONE
  COMPLAINCE_C() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.COUNTRY_C, this: this.en }, this.options7)
  }

  // FILTER FOR COMPLAINCER USER MENU ONE
  COMPLAINCE() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.COMPLAINCE_DATA, this: this.en }, this.options)
  }

  getData() {
    return this.http.post(`${this.intermediateJson.LOGFETCH}`, null);
  }
  send() {
    // console.log(this.encryptedMessage +  'data',this.encrypt )
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.encryptedMessage, this: this.encrypt }, this.options)
  }

  reportfilter() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.e, this: this.en }, this.options1)
  }

  reportfilterone() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.ea, this: this.en }, this.options2)
  }
  get_countriesforclock() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.eam, this: this.en }, this.options3)
  }

  filter_one() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.filter_data, this: this.en }, this.options4)
  }
  filter_two() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.filter_datatwo, this: this.en }, this.options5)
  }
  filter_final() {
    return this.http.post<any>(`${this.intermediateJson.GATEWAY}`, { data: this.filter_encryptmessage, this: this.filter_encrypt }, this.options6)
  }

}
