import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layouts/header/header.component';
import { SidebarComponent } from './components/layouts/sidebar/sidebar.component';
import { FooterComponent } from './components/layouts/footer/footer.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard/dashboard.component';
import { AnalyticsComponent } from './components/pages/dashboard/analytics/analytics.component';
import { EcommerceComponent } from './components/pages/dashboard/ecommerce/ecommerce.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { TableComponent } from './components/pages/table/table/table.component';
import { AppEmailComponent } from './components/pages/app-email/app-email/app-email.component';
import { AuthenticationComponent } from './components/pages/authentication/authentication/authentication.component';
import { LoginWithImageComponent } from './components/pages/authentication/login-with-image/login-with-image.component';
import { RegisterComponent } from './components/pages/authentication/register/register.component';
import { ForgotPasswordComponent } from './components/pages/authentication/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/pages/authentication/reset-password/reset-password.component';
import { SessionLockScreenComponent } from './components/pages/authentication/session-lock-screen/session-lock-screen.component';
import { NotAuthorizedComponent } from './components/pages/miscellaneous/not-authorized/not-authorized.component';
import { MaintenanceComponent } from './components/pages/miscellaneous/maintenance/maintenance.component';
import { ComingSoonComponent } from './components/pages/miscellaneous/coming-soon/coming-soon.component';
import { MiscellaneousComponent } from './components/pages/miscellaneous/miscellaneous/miscellaneous.component';
import { ErrorComponent } from './components/pages/error/error/error.component';
import { WebAnalyticsComponent } from './components/charts/web-analytics/web-analytics.component';
import { EmailSendComponent } from './components/charts/email-send/email-send.component';
import { ActivityTimelineComponent } from './components/charts/activity-timeline/activity-timeline.component';
import { TrafficSourceComponent } from './components/charts/traffic-source/traffic-source.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule,  HTTP_INTERCEPTORS } from '@angular/common/http';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { AgGridModule } from 'ag-grid-angular';
import { InterceptorService } from './services/interceptor.service';
import { ToastrModule } from "ngx-toastr";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgSelectModule } from '@ng-select/ng-select';
import {DCR} from '../app/components/pages/customer_registration/customer.component';
import {DBR} from '../app/components/pages/beneficiary_registration/beneficiary.component';
import { TXN } from './components/pages/Txn/Txn.component';
import { REPORTS } from './components/pages/report/report.component';
import { ReportComponent } from './components/pages/report/report/report.component';
import { MENU } from './components/pages/submenu/submenu.component';
import { CookieService } from 'ngx-cookie-service';
import { ReportOneComponent } from './components/pages/report/report1/reportone.component';
import {TRANSFER} from './components/pages/transfer/transfer.component';
import { LANDING } from './components/pages/landing/landing.component';
import { Newuser } from './components/pages/newuser/newuser.component';
import { Demosuper } from './components/pages/demosuper/demo.component';
import { Menuone } from './components/pages/demo/menuone/menuone.component';
import { HttpClientService } from './header.service';
import { Menutwo } from './components/pages/demo/menutwo/menutwo.component';
import { Menuthree } from './components/pages/demo/menuthree/menuthree.component';
import { Menufour } from './components/pages/demo/menufour/menufour.component';
import { Menufive } from './components/pages/demo/menufive/menufive.component';
import { Menusix } from './components/pages/demo/menusix/menusix.component';
import { Menuseven } from './components/pages/demo/menuseven/menuseven.component';
import { Menueight } from './components/pages/demo/menueight/menueight.component';
import { Menunine } from './components/pages/demo/menunine/menunine.component';
import { Menuten } from './components/pages/demo/menuten/menuten.component';
import { Menueleven } from './components/pages/demo/menueleven/menueleven.component';
import { Menutwel } from './components/pages/demo/menutwelev/menutwel.component';
import { Menuparent } from './components/pages/demo/demomenuparent/menuparent.component';
import { Treasuremenuparent } from './components/pages/Treasuremenu/treasureparent/treasureparent.component';
import { treasureone } from './components/pages/Treasuremenu/treasure1/treasureone.component';
import { treasuretwo } from './components/pages/Treasuremenu/treasure2/treasuretwo.component';
import { treasurethree } from './components/pages/Treasuremenu/treasure3/treasurethree.component';
import { treasurefour } from './components/pages/Treasuremenu/treasure4/treasurefour.component';
import { treasurefive } from './components/pages/Treasuremenu/treasure5/treasurefive.component';
import { treasuresix } from './components/pages/Treasuremenu/treasure6/treasuresix.component';
import { treasureseven } from './components/pages/Treasuremenu/treasure7/treasureseven.component';
import { treasureeight } from './components/pages/Treasuremenu/treasure8/treasureeight.component';
import { treasurenine } from './components/pages/Treasuremenu/treasure9/treasurenine.component';
import { treasureten } from './components/pages/Treasuremenu/treasure10/treasureten.component';
import { treasureeleven } from './components/pages/Treasuremenu/treasure11/treasureeleven.component';
import { treasuretwel } from './components/pages/Treasuremenu/treasure12/treasuretwel.component';
import { treasurethirt } from './components/pages/Treasuremenu/treasure13/treasurethirt.component';
import { treasurefourt } from './components/pages/Treasuremenu/treasure14/treasurefourt.component';
import { treasurefift } from './components/pages/Treasuremenu/treasure15/treasurefift.component';
import { treasuresixte } from './components/pages/Treasuremenu/treasure16/treasuresixte.component';
import { treasurest } from './components/pages/Treasuremenu/treasure17/treasurest.component';
import { treasureeighteen } from './components/pages/Treasuremenu/treasure18/treasureeighteen.component';
import { treasurenineteen } from './components/pages/Treasuremenu/treasure19/treasurenineteen.component';
import { Treasuredashboard } from './components/pages/Treasuremenu/treasuredash/treasuredashboard.component';
import { Customermenuparent } from './components/pages/Customerservicemenu/customer_sparent/customerparent.component';
import { Customerone } from './components/pages/Customerservicemenu/customer1/customerone.component';
import { Customertwo } from './components/pages/Customerservicemenu/customer2/customertwo.component';
import { Customerthree } from './components/pages/Customerservicemenu/customer3/customerthree.component';
import { Customerfour } from './components/pages/Customerservicemenu/customer4/customerfour.component';
import { Customerfive } from './components/pages/Customerservicemenu/customer5/customerfive.component';
import { Customersix } from './components/pages/Customerservicemenu/customer6/customersix.component';
import { Customerseven } from './components/pages/Customerservicemenu/customer7/customerseven.component';
import { Customereight } from './components/pages/Customerservicemenu/customer8/customereight.component';
import { Customernine } from './components/pages/Customerservicemenu/customer9/customernine.component';
import { Customerten } from './components/pages/Customerservicemenu/customer10/customerten.component';
import { Customereleven } from './components/pages/Customerservicemenu/customer11/customereleven.component';
import { Customertwel } from './components/pages/Customerservicemenu/customer12/customertwel.component';
import { Customerthirt } from './components/pages/Customerservicemenu/customer13/customerthirt.component';
import { Customerfourt } from './components/pages/Customerservicemenu/customer14/customerfourt.component';
import { SummaryComponent } from './components/pages/report_summary/summary.component';
import { MSmenuparent } from './components/pages/MSmenu/MSparentmenu/msparent.component';
import { MSone } from './components/pages/MSmenu/msmenu1/msmenuone.component';
import { MStwo } from './components/pages/MSmenu/msmenu2/msmenutwo.component';
import { MSthree } from './components/pages/MSmenu/msmenu3/msmenuthree.component';
import { MSfour } from './components/pages/MSmenu/msmenu4/msmenufour.component';
import { MSfive } from './components/pages/MSmenu/msmenu5/msmenufive.component';
import { ShareddataService } from './shareddata.service';
import { MasterOneComponent } from './components/pages/masters/masterui_one/master_one.component';
import { MasterTwoComponent } from './components/pages/masters/masterui_two/mastertwo.component';
import { MasterComponent } from './components/pages/masters/master/master.component';
import { DatePipe } from '@angular/common';



@NgModule({
  declarations: [
    MasterComponent,
    MasterTwoComponent,
    MasterOneComponent,
    SummaryComponent,
    treasurenineteen,
    treasureeighteen,
    Treasuredashboard,
    MSmenuparent,
    MSone,
    MStwo,
    MSthree,
    MSfour,
    MSfive,
    Customermenuparent,
    Customerone,
    Customertwo,
    Customerthree,
    Customerfour,
    Customerfive,
    Customersix,
    Customerseven,
    Customereight,
    Customernine,
    Customerten,
    Customereleven,
    Customertwel,
    Customerthirt,
    Customerfourt,
    treasurest,
    Treasuremenuparent,
    treasureone,
    treasuretwo,
    treasurethree,
    treasurefour,
    treasurefive,
    treasuresix,
    treasureseven,
    treasureeight,
    treasurenine,
    treasureten,
    treasureeleven,
    treasuretwel,
    treasurethirt,
    treasurefourt,
    treasurefift,
    treasuresixte,
    Menuparent,
    Menufive,
    Menusix,
    Menuseven,
    Menueight,
    Menunine,
    Menuten,
    Menueleven,
    Menutwel,
    Menufour,
    Menuthree,
    Menutwo,
    Menuone,
    Demosuper,
    Newuser,
    LANDING,
    TRANSFER,
    MENU,
    TXN,
    REPORTS,
    DCR,
    DBR,
    ReportOneComponent,
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    AnalyticsComponent,
    EcommerceComponent,
    ProfileComponent,
    TableComponent,
    AppEmailComponent,
    AuthenticationComponent,
    LoginWithImageComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    SessionLockScreenComponent,
    NotAuthorizedComponent,
    MaintenanceComponent,
    ComingSoonComponent,
    MiscellaneousComponent,
    ErrorComponent,
    WebAnalyticsComponent,
    EmailSendComponent,
    ActivityTimelineComponent,
    TrafficSourceComponent,
    ReportComponent,
  ],
  imports: [
    NgSelectModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: "toast-top-center",
    }),
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FontAwesomeModule,
    ReactiveFormsModule ,
    AgGridModule.withComponents([])
  ],
  providers: [
    DatePipe,
    [ShareddataService],
    HttpClientService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpClientService,
      multi: true
    },
    CookieService,
    InterceptorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
