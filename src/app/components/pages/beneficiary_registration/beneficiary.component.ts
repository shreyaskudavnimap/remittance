import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { RootService } from 'src/app/root.service';
import * as CryptoJS from 'crypto-js';
import moment from 'moment';
import { TrackerService } from '../tracker.service';

import { filter, pairwise } from 'rxjs/operators';
import { Router, RoutesRecognized } from '@angular/router';



@Component({
  selector: 'dcr',
  templateUrl: 'beneficiary.component.html',
  styleUrls: ["beneficiary.component.css"],
  encapsulation: ViewEncapsulation.None

})



export class DBR implements OnInit, OnDestroy {

  COUNTRY
  BRANCH
  MODE
  ID
  desc
  idno
  issuedate
  expirydate
  purpose
  fname
  mname
  lname
  nickname
  mobile
  email
  adress
  zip
  relation
  city
 Numb
  generate_bene
  partnercode
  EnteredBy


  find:[];
  find1:[];
  find2:[];
  find3:[];
  find4:[];
  find5:[];
  find6:[];
  dateStart


  constructor(private root:RootService, private tracker:TrackerService, public router:Router){ }

//   const z = document.getElementsByClassName('cust') as HTMLCollectionOf<HTMLElement>;
//   for (let i = 0; i < z.length; i++) {
//       z[i].style.display = "none";
//   }


  columnDefs = [
    {headerName:'NAME OF CUSTOMER	',field:"model",sortable:true,filter:true,width:270},
    {headerName:'Country',field:"price",sortable:true,filter:true,width:270},
    {headerName:'MOBILE NUMBER',field:"number",sortable:true,filter:true,width:270},
    {headerName:'Beneficiary',field:"email",sortable:true,filter:true,width:270},
  ];
  rowData = [
    { model: "sameer badsha", price:"India", number:"9878778",email:"1"},
    { model: "sanjay badsha", price:"Bali", number:"9878778",email:"2"},
    { model: "akshay ahemad", price:"Nepal", number:"9878778",email:"1"},
    { model: "suraj bhatia", price:"UAE", number:"9878778",email:"1"},
    { model: "ikbal ahemad", price:"Dubai", number:"9878778",email:"1"},
    { model: "shababudeen", price:"Muscut", number:"9878778",email:"1"},
  ];

  ngOnInit() {
    // this.router.events                  // GETTING THE PREVIOUS PAGE ROUTE
    // .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
    // .subscribe((events: RoutesRecognized[]) => {
    //   console.log('previous url', events[0].urlAfterRedirects);
    //   this.route = events[0].urlAfterRedirects
    //   console.log('current url', events[1].urlAfterRedirects);
    // });


    this.tracker.send()

    // this.dateStart =  moment().format('hh:mm:ss')
    this.dateStart =  moment(Date.now());

    this.partnercode = localStorage.getItem('partnercode');
    this.EnteredBy = sessionStorage.getItem('userid')

    this.root.RANDOM_BENE(this.partnercode);

    this.root._BENE_COUNTRY().subscribe((res)=>{                         // THIS IS FETCHING THE PAYOUT COUNTRY
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const a = c.data[0];
      this.find = a;
    })

    this.root._BENE_PURPOSE().subscribe((res)=>{                         // THIS IS FETCHING THE PURPOSE
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
    //   console.log(c)
      const a = c.data[0];
      this.find3 = a;
    })

    this.root._BENE_ID().subscribe((res)=>{                         // THIS IS FETCHING THE ID
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const a = c.data[0];
      this.find4 = a;
    })

    this.root._BENE_RELATION().subscribe((res)=>{                         // THIS IS FETCHING THE RELATION
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const a = c.data[0];
      this.find5 = a;
    })

    this.root._RANDOM_BENE().subscribe((res)=>{                         // THIS IS FOR GENERATING THE RANDOM BENE NUMBER
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const number = c.data[0];
      this.generate_bene = number[0].Number;
      // console.log(this.generate_bene);
    })


    this.root._CUSTOMER_LIST().subscribe((res)=>{                         // THIS IS FETCHING THE REGISTERED CUSTOMER LIST
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
        const c = JSON.parse(msg);
        // console.log(c)
        const a = c.data[0];
        this.find6 = a;
    })
  }

  ngOnDestroy(): void {
    let period = moment(Date.now());
    var duration = moment.duration(period.diff(this.dateStart));
    let Seconds = duration.asSeconds();
    console.log(Seconds);
  }


  submit(){
    const Data = {
      "country":this.COUNTRY.Code,
      "mode":this.MODE.ModeOfPaymentName,
      "purpose":this.purpose.ID,
      "branch":this.BRANCH.BranchName,
      "CustomerNo":this.Numb.CustomerNo,
      "first_name":this.fname,
      "middle_name":this.mname,
      "last_name":this.lname,
      "nick_name":this.nickname,
      "mobile":this.mobile,
      "email":this.email,
      "address":this.adress,
      "zip":this.zip,
      "relation":this.relation.relation,
      "city":this.city,
      "idtype":this.ID.ID,
      "iddesc":this.desc,
      "idno":this.idno,
      "issuedate":this.issuedate,
      "expirydate":this.expirydate,
      "partnerCode":this.partnercode,
      "EnteredBy": this.EnteredBy,
      "Generate_Random":this.generate_bene,
    }
    console.log(Data);
    this.root.BENE_SUBMIT(Data);
    this.root._BENE_SUBMIT().subscribe((res)=>{                         // THIS IS SUBMIT THE FORM DATA
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      console.log(msg);
    })
  }



  onChange(val){
    console.log(val)

    this.root.BENE_BRANCH(this.COUNTRY.Code);
    this.root.BENE_MODE(this.COUNTRY.ID);

    this.root._BENE_BRANC().subscribe((res)=>{                         // THIS IS FETCHING THE PAYOUT BRANCH
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const a = c.data[0];
      this.find1 = a;
    })


    this.root._BENE_MOD().subscribe((res)=>{                         // THIS IS FETCHING THE PAYOUT MODE
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const a = c.data[0];
      this.find2 = a;
    })
  }

  Mode(val){
    console.log(val)
  }
  Purpose(val){
    console.log(val)
  }
  Payout_branch(val){
    console.log(val)
  }
  Relation(val){
    console.log(val)
  }
  Ids(val){
    console.log(val)
  }
  Number(val){
    console.log(val)
  }

}
