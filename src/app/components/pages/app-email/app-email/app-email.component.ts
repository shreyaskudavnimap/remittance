import { Component, OnInit } from '@angular/core';
import { TrackerService } from '../../tracker.service';

@Component({
  selector: 'app-email',
  templateUrl: './app-email.component.html',
  styleUrls: ['./app-email.component.scss']
})
export class AppEmailComponent implements OnInit {

  constructor(private tracker:TrackerService) { }

  ngOnInit() {
    this.tracker.send()

  }

}
