import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";


@Component({
    selector: "app-analytics",
    templateUrl: "treasuredashboard.component.html",
    styleUrls: ["treasuredashboard.component.css"]
})
export class Treasuredashboard implements OnInit {
  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor() {}

    columnDefs = [
        {
            headerName: 'Top 10 Transactions',
            children: [
              {
                field: 'Currency',
                width: 150,
                suppressSizeToFit: true,
                enableRowGroup: true,
              },
              {
                field: 'Txns',
                width: 90,
                minWidth: 75,
                maxWidth: 100,
                enableRowGroup: true,
              },
            ],
          },
      ];
      rowData = [
          {Currency:"USD $", Txns:"100"},
          {Currency:"INR", Txns:"89"},
          {Currency:"RIYAL", Txns:"75"},
          {Currency:"RINGGET", Txns:"54"},
          {Currency:"YEN", Txns:"34"},
          {Currency:"EURO", Txns:"12"},
      ];

    ngOnInit() {
    }
}
