import { Component, OnInit} from '@angular/core';
import { TrackerService } from '../tracker.service';


@Component({
  selector: 'dcr',
  templateUrl: 'submenu.component.html',
  styleUrls: ["submenu.component.scss"]
})



export class MENU implements OnInit {

  constructor(private tracker:TrackerService){}

  ngOnInit() {
    this.tracker.send()
  }

}

