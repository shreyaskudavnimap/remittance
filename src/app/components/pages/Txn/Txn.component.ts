import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RootService } from 'src/app/root.service';
import * as CryptoJS from 'crypto-js';
import { GridOptions, stringToArray } from 'ag-grid-community';
import { TrackerService } from '../tracker.service';
import axios from 'axios';
import FingerprintJS from '@fingerprintjs/fingerprintjs-pro'
import { PaymentMode, Permission } from 'src/app/contants';
import { Utility } from 'src/app/utility';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'dcr',
  templateUrl: 'Txn.component.html',
  styleUrls: ["Txn.component.css"],
  encapsulation: ViewEncapsulation.None

})



export class TXN implements OnInit {
  selection
  show
  sel
  Cit
  City
  Exchange_rate
  payin_amount
  payout_amount
  commission
  vat
  sendingCurrency
  receivingCountry
  POT
  RECEIVING_COUNTRY
  find: [];
  find1: [];
  find2: [];
  find3: [];
  find4: [];
  find5: [];
  find6: [];
  find7: [];
  partnercode
  Data = 'Appear';
  name
  country
  email
  REC
  names
  countrys
  emails
  data
  AGENT_ID
  userIP = ''
  EnteredBy
  time
  Currency_Id
  CountryID
  Receivecity
  paymentmode
  recommendedagent
  receivingcurrency
  POTransfer
  visitorId
  transaction_number
  Customer_Data
  Bene_Data
  Agent_data
  userData
  // ModeOfPayment
  modeOfPayment
  SelectedDestCountry
  SelectedPaymentMode
  receiveCity
  recommendedAgent
  receiveCurrency
  purposeOfTransfer
  payinAmount
  BankWise_ExchangeRate_Info: any;
  Payment_Info: any = {}
  AgentData: any;
  HiddenCTAHold: any;
  HiddenBankLimit: any;
  destinationCity: any;
  hidRate: any;
  hidAgentRate: any;
  ExchangeRate: any;
  AgentBranchDetails: any;
  BankWise_ExchangeRateData: any;
  hidPayOutSettlementCCYID: any;
  _SA_SC_SRC_SettelmentRate: any;
  PayOutSettlementCurrencyID: number;
  private _RA_SC_RC_SRC_SettelmentRate: any;
  ViewCTALimitRateMsgViewState: boolean;
  CalledFromCalculateButtonViewState: boolean;
  payOutAmount: any;
  Currency_Info: any;
  sendingCountry: any;
  AgentSlab_Info: any;
  SendingCurrencyDetails: any;
  HiddenSASettlementRate: any;
  hdnVATAmt: any;
  hdnVATPer: any;
  hdnCommIncVat: any;
  hidPayCommissionType: any;
  hidSPayCommissionType: any;
  hidRPayCommissionType: any;
  hidPayInCommissionType: any;
  hidPayInCurrencyType: any;
  hidPayInCommissionAmount: any;
  hidSUBPayInCommissionType: any;
  hidSUBPayInCommissionAmount: any;
  hidPayOutCommissionType: any;
  hidPayOutCurrencyType: any;
  hidPayOutCommissionAmount: any;
  hidSPercentage: any;
  hidSMinimumCommission: any;
  hidSMaximumCommission: any;
  hidRPercentage: any;
  hidRMinimumCommission: any;
  hidRMaximumCommission: any;
  PayInAmount: any;
  PayOutAmount: any;
  HiddenFixedTotComm: any;
  CommissionCharge: any;
  txtVat: any;
  lblTotalPayable: any;
  HidComm: any;
  HidSUBAgentComm: any;
  HidAgentComm: any;
  hidRCommAmtOf: any;
  hidRPayOutCurrencyType: any;
  _CommissionDetails: any;
  SAgentPercentage_Info: any;
  RAgentPercentage_Info: any;
  BaseToBaseCommissionAmount: any;
  VAT: any;
  CurrentTrxnAmt: string;
  IDNumber: any;
  Mobile: any;
  RIsdCode: any;
  Customer_Info: any;
  Bene_Info: any;
  _TempTable: any = {};
  TrDetails_Info: any;
  _OfferRate: any;
  _BidRate: any;
  AgentDetails: any;
  _Receiving_SettlementCurrency: any;
  _Receiver_OfferRate: any;
  _SenderBase_Rate: number;
  _ReceiverAgentBaseCCY: any;
  _ReceiverBase_Rate: number;
  _BaseCurrency: any;
  DailyRates: any;
  BANK_SENDING_CURRENCY_CER: any;
  ExchangeRateData: any;
  _SC_SRC_SettelmentRate: any;
  ExchangeRateDataPayin: any;
  ExchangeRateDataPayout: any;
  _RC_SRC_SettelmentRate: any;
  _Sending_SettlementCurrency: any;
  _SA_STC_SRC_SettelmentRate: any;
  _RA_STC_SRC_SettelmentRate: any;
  _RASettelmentRate: any;

  constructor(private root: RootService, private tracker: TrackerService, private util: Utility, public datepipe: DatePipe) {
    this.getGithubData();
    this.EnteredBy = sessionStorage.getItem('userid')
    this.AGENT_ID = localStorage.getItem("AGENT_ID");
    this.root.TRANSFER_CURR(this.AGENT_ID);
    this.util = util;
    this.datepipe = datepipe;
  }

  OnChangeReceivingCountry(selectedDestCountry) {
    console.log('selectedDestCountry', selectedDestCountry);
    this.sendingCountry = selectedDestCountry;
    this.modeOfPayment = null;
    this.receiveCity = null;
    this.recommendedAgent = null;
    this.DDLoadPaymentMode(selectedDestCountry);
  }

  OnChangeRecommendedAgent(recommendedAgent) {
    console.log('recommendedAgent', recommendedAgent);
    this.LoadAgentBranchDetails();
    this.DDLoadDestinationCurrency(recommendedAgent);
  }

  OnChangePaymentMode(selectedPaymentMode) {
    console.log('selectedPaymentMode', selectedPaymentMode);
    this.receiveCity = null;
    this.recommendedAgent = null;
    // this.SelectedPaymentMode = selectedPaymentMode;
    this.DDLoadDestinationCity(this.receivingCountry, selectedPaymentMode)
  }

  OnChangeReceivingCurrency(receiveCurrency) {
    console.log('receiveCurrency', receiveCurrency);
    this.SetTextExchangeRate(receiveCurrency);
  }

  async OnChangePayinAmount(payInAmount) {
    this.LoadAgentBranchDetails();

    if (this.userData.SettlementCurrency != this.userData.Base_Currency_Id) {

    }
    else {
      let requestData = {
        P1MD6nizSA: {
          fromcountryid: this.userData.CountryID,
          fromcurrencyid: this.sendingCurrency.Currency_Id,
          fromagentid: this.userData.Agent_ID,
          tocurrencyid: this.receiveCurrency.Currency_Id,
          tocountryid: this.receivingCountry.CountryID,
          active: 1,
        }
      };

      console.log('Sending Agent Currency ER Request', requestData);

      return this.root._SENDING_AGENT_CURRENCY_EXCHANGE_RATE(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
        let result = this.util.DecryptJson(res).data[0][0];
        console.log('SENDING_AGENT_CURRENCY_EXCHANGE_RATE', result);

        this.BankWise_ExchangeRateData = result;
        this._SA_SC_SRC_SettelmentRate = parseFloat(result.Settelment_Rate);

        let requestData = {
          P1MD6nizSA: {
            agentid: this.AgentBranchDetails.Agent_ID
          }
        };

        this.root._AGENT_SETTLEMENT_CURRENCY(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
          let result = this.util.DecryptJson(res).data[0][0];
          this.hidPayOutSettlementCCYID = result.SettlementCurrency;

          this.PayOutSettlementCurrencyID = parseInt(this.hidPayOutSettlementCCYID);

          if (this.PayOutSettlementCurrencyID != this.userData.Base_Currency_Id) {

          }
          else {
            let requestData = {
              P1MD6nizSA: {
                fromcountryid: this.userData.CountryID,
                fromcurrencyid: this.sendingCurrency.Currency_Id,
                tocountryid: this.receivingCountry.CountryID,
                tocurrencyid: this.receiveCurrency.Currency_Id,
                agentid: this.AgentBranchDetails.Agent_ID,
                mop: this.modeOfPayment.ModeOfpaymentId,
                active: 1
              }
            };

            this.root._BANK_SENDING_CURRENCY_CER(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
              let result = this.util.DecryptJson(res).data[0][0];
              console.log('BANK_SENDING_CURRENCY_CER', result);
              this.BANK_SENDING_CURRENCY_CER = result;

              this._RA_SC_RC_SRC_SettelmentRate = parseFloat(result.Settelment_Rate);
              var Commission = 0;

              if (this.payinAmount) {
                var CurrentTrxnAmt = parseFloat((parseFloat(this.payinAmount + Commission) / parseFloat(this._SA_SC_SRC_SettelmentRate)).toFixed(2));
                if (CurrentTrxnAmt >= this.HiddenCTAHold) {
                  if (!this.CalledFromCalculateButtonViewState) {
                    this.CalledFromCalculateButtonViewState = false;
                    this.DDLoadDestinationCurrency(this.recommendedAgent);
                  }
                  if (this.ViewCTALimitRateMsgViewState) {
                    alert("Transaction amount crossed CTA Hold Limit, so rate is changed.");
                  }
                }
                else {
                  if (this.CalledFromCalculateButtonViewState) {
                    this.DDLoadDestinationCurrency(this.recommendedAgent);
                  }
                  this.CalledFromCalculateButtonViewState = false;
                }
              }

              let requestData = {
                P1MD6nizSA: {
                  currencyid: this.sendingCurrency.Currency_Id,
                }
              }
              this.root._CURRENCY_DETAILS_1(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                let result = this.util.DecryptJson(res).data[0];
                console.log('Currency Details Sending', result);
                this.SendingCurrencyDetails = result;
                this.Payment_Info.PayInAmount = parseFloat(parseFloat(this.payinAmount).toFixed(result.Scale));


                let requestData = {
                  P1MD6nizSA: {
                    currencyid: this.receiveCurrency.Currency_Id,
                  }
                }
                this.root._CURRENCY_DETAILS_1(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                  let result = this.util.DecryptJson(res).data[0];
                  console.log('Currency Details Receving', result);
                  console.log('this.Payment_Info', this.Payment_Info);

                  if (parseInt(this.payinAmount) != 0) {
                    this.Payment_Info.PayOutAmount = this.payinAmount * parseFloat(this.ExchangeRate);
                  }
                  this.Payment_Info.PayOutAmount = parseFloat(parseFloat(this.Payment_Info.PayOutAmount).toFixed(result.Scale));

                  let requestData = {
                    P1MD6nizSA: {
                      countryid: this.userData.CountryID,
                    }
                  }

                  this.root.VAR_INFORMATION(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                    let result = this.util.DecryptJson(res); //.data[0];
                    console.log('VAT Info', result);

                    let requestData = {
                      P1MD6nizSA: {
                        toagentid: this.BankWise_ExchangeRateData.Agent_ID,
                        payincountryid: this.Payment_Info.PayInCountry,
                        payincurrencyid: this.Payment_Info.PayInCurrencyID,
                        payoutcountryid: this.sendingCountry.CountryID,
                        payoutcurrencyid: this.Payment_Info.PayOutCurrencyID,
                        mob: this.modeOfPayment.ModeOfpaymentId,
                        payinamount: this.Payment_Info.PayInAmount,
                        payoutamount: this.Payment_Info.PayOutAmount
                      }
                    }
                    console.log('Request AGENT_PERCENTAGE_WISE_COMMISSION', requestData);
                    this.root.AGENT_PERCENTAGE_WISE_COMMISSION(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                      let result = this.util.DecryptJson(res).data[0][0];
                      console.log('Response AGENT_PERCENTAGE_WISE_COMMISSION', result);
                      this.SAgentPercentage_Info = result;

                      if (!result) {

                        let requestData = {
                          P1MD6nizSA: {
                            payoutcountryid: this.sendingCountry.CountryID,
                            payoutcurrencyid: this.Payment_Info.PayOutCurrencyID,
                            toagentid: this.BankWise_ExchangeRateData.Agent_ID,
                            mob: this.modeOfPayment.ModeOfpaymentId,
                            payinamount: this.Payment_Info.PayInAmount,
                            payoutamount: this.Payment_Info.PayOutAmount
                          }
                        }

                        this.root.RECEIVING_AGENT_PERCENTAGE_WISE_COMMISSION(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                          let result = this.util.DecryptJson(res).data[0][0];
                          console.log('Response RECEIVING_AGENT_PERCENTAGE_WISE_COMMISSION', result);
                          this.RAgentPercentage_Info = result;

                          if (!result) {

                            let requestData = {
                              P1MD6nizSA: {
                                toagentid: this.BankWise_ExchangeRateData.Agent_ID,
                                payincountryid: this.Payment_Info.PayInCountry,
                                payincurrencyid: this.Payment_Info.PayInCurrencyID,
                                payoutcountryid: this.sendingCountry.CountryID,
                                payoutcurrencyid: this.Payment_Info.PayOutCurrencyID,
                                mob: this.modeOfPayment.ModeOfpaymentId,
                                receivingagentid: this.AgentBranchDetails.Agent_ID,
                                payinamount: this.Payment_Info.PayInAmount,
                                payoutamount: this.Payment_Info.PayOutAmount,
                                agentbranchid: this.userData.Agent_Branch_ID,
                              }
                            }
                            console.log('Request AGENT_WISE_COMMISSION', requestData);
                            this.root.AGENT_WISE_COMMISSION(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                              let result = this.util.DecryptJson(res).data[0][0];
                              console.log('Response AGENT_WISE_COMMISSION', result);

                              if (!result) {
                                let requestData = {
                                  P1MD6nizSA: {
                                    payincountryid: this.Payment_Info.PayInCountry,
                                    payincurrencyid: this.Payment_Info.PayInCurrencyID,
                                    payoutcountryid: this.sendingCountry.CountryID,
                                    payoutcurrencyid: this.Payment_Info.PayOutCurrencyID,
                                    mob: this.modeOfPayment.ModeOfpaymentId,
                                    payinamount: this.Payment_Info.PayInAmount,
                                    payoutamount: this.Payment_Info.PayOutAmount,
                                  }
                                }

                                this.root.VW_COMMISSION(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                                  let result = this.util.DecryptJson(res).data[0][0];
                                  console.log('Response VW_COMMISSION AgentSlab_Info', result);

                                  this.AgentSlab_Info = result;
                                  if (result) {
                                    var _IsCommissionAvailable = true;

                                    if (_IsCommissionAvailable) {
                                      if (this.Payment_Info.Commission && this.Payment_Info.Commission != 0) {

                                        var _CommCharge = 0;
                                        var _EnterCommCharge = 0;
                                        var _calCommCharge = 0;
                                        var _AgentCommVariance = 0;

                                        _CommCharge = this.AgentSlab_Info.BaseToBaseCommissionAmount;
                                        _EnterCommCharge = this.Payment_Info.Commission;
                                        _calCommCharge = _EnterCommCharge - _CommCharge;
                                        this.Payment_Info.VATAmount = _EnterCommCharge * this.Payment_Info.VATPercent / 100;
                                        this.Payment_Info.CommIncVAT = _EnterCommCharge + this.Payment_Info.VATAmount;
                                        if (_calCommCharge < 0) {
                                          this.Payment_Info.CalculatedCommission = _EnterCommCharge;
                                          this.Payment_Info.AgentCommission = _calCommCharge;
                                        }
                                        else {
                                          this.Payment_Info.CalculatedCommission = _CommCharge;
                                          this.Payment_Info.AgentCommission = _calCommCharge;
                                        }
                                        this.Payment_Info.TotalPayable = this.Payment_Info.PayInAmount + this.Payment_Info.Commission + this.Payment_Info.VATAmount;

                                      }
                                      else {
                                        this.Payment_Info.VATAmount = this.AgentSlab_Info.BaseToBaseCommissionAmount * this.Payment_Info.VATPercent / 100;
                                        this.Payment_Info.CommIncVAT = this.AgentSlab_Info.BaseToBaseCommissionAmount + this.Payment_Info.VATAmount;
                                        this.Payment_Info.Commission = this.AgentSlab_Info.BaseToBaseCommissionAmount;
                                        this.Payment_Info.CalculatedCommission = this.AgentSlab_Info.BaseToBaseCommissionAmount;
                                        this.Payment_Info.AgentCommission = 0;
                                        this.Payment_Info.TotalPayable = this.Payment_Info.PayInAmount + this.AgentSlab_Info.BaseToBaseCommissionAmount + this.Payment_Info.VATAmount;
                                      }

                                    }

                                    this.Payment_Info.TotalPayable = parseFloat(this.Payment_Info.TotalPayable.toFixed(this.SendingCurrencyDetails.Scale));
                                    this.Payment_Info.Commission = parseFloat(this.Payment_Info.Commission.toFixed(this.SendingCurrencyDetails.Scalecale));

                                    console.log('Payment_Info', this.Payment_Info);


                                    this.HiddenSASettlementRate = this._SA_SC_SRC_SettelmentRate;
                                    CurrentTrxnAmt = parseFloat((parseFloat(this.payinAmount + Commission) / this._SA_SC_SRC_SettelmentRate).toFixed(2));
                                    let Balance = parseFloat(!this.HiddenBankLimit ? 0 : this.HiddenBankLimit);

                                    if (CurrentTrxnAmt <= Balance) {
                                      this.hdnVATAmt = this.Payment_Info.VATAmount;
                                      this.hdnVATPer = this.Payment_Info.VATPercent;
                                      this.hdnCommIncVat = this.Payment_Info.CommIncVAT;

                                      this.hidPayCommissionType = this.AgentSlab_Info.PayCommissionType ? this.AgentSlab_Info.PayCommissionType : 0;
                                      this.hidSPayCommissionType = this.SAgentPercentage_Info ? this.SAgentPercentage_Info.SPayCommissionType : 0;
                                      this.hidRPayCommissionType = this.RAgentPercentage_Info ? this.RAgentPercentage_Info.RPayCommissionType : 0;

                                      this.hidPayInCommissionType = this.AgentSlab_Info ? this.AgentSlab_Info.PayInCommissionType : 0;
                                      this.hidPayInCurrencyType = this.AgentSlab_Info ? this.AgentSlab_Info.PayInCurrencyType : 0;
                                      this.hidPayInCommissionAmount = this.AgentSlab_Info ? this.AgentSlab_Info.PayInCommissionAmount : 0;
                                      this.hidSUBPayInCommissionType = this.AgentSlab_Info ? this.AgentSlab_Info.SUBPayInCommissionType : 0;
                                      this.hidSUBPayInCommissionAmount = this.AgentSlab_Info ? this.AgentSlab_Info.SUBPayInCommissionAmount : 0;
                                      this.hidPayOutCommissionType = this.AgentSlab_Info ? this.AgentSlab_Info.PayOutCommissionType : 0;
                                      this.hidPayOutCurrencyType = this.AgentSlab_Info ? this.AgentSlab_Info.PayOutCurrencyType : 0;
                                      this.hidPayOutCommissionAmount = this.AgentSlab_Info ? this.AgentSlab_Info.PayOutCommissionAmount : 0;
                                      this.hidSPercentage = this.SAgentPercentage_Info ? this.SAgentPercentage_Info.SPercentage : 0;
                                      this.hidSMinimumCommission = this.SAgentPercentage_Info ? this.SAgentPercentage_Info.SMinimumCommission : 0;
                                      this.hidSMaximumCommission = this.SAgentPercentage_Info ? this.SAgentPercentage_Info.SMaximumCommission : 0;
                                      this.hidRPercentage = this.RAgentPercentage_Info ? this.RAgentPercentage_Info.RPercentage : 0;
                                      this.hidRMinimumCommission = this.RAgentPercentage_Info ? this.RAgentPercentage_Info.RMinimumCommission : 0;
                                      this.hidRMaximumCommission = this.RAgentPercentage_Info ? this.RAgentPercentage_Info.RMaximumCommission : 0;
                                      this.payinAmount = this.Payment_Info.PayInAmount;
                                      this.payOutAmount = this.Payment_Info.PayOutAmount;

                                      this.HiddenFixedTotComm = this.BaseToBaseCommissionAmount = this.SAgentPercentage_Info && this.SAgentPercentage_Info.TotalCommission != 0 ? this.SAgentPercentage_Info.TotalCommission : this.AgentSlab_Info.BaseToBaseCommissionAmount;
                                      this.CommissionCharge = this.Payment_Info.Commission;
                                      //VAT 
                                      this.VAT = this.hdnVATAmt != NaN ? 0.00 : this.hdnVATAmt;

                                      this.lblTotalPayable = this.Payment_Info.TotalPayable;

                                      this.HidComm = this.Payment_Info.CalculatedCommission;

                                      if (this.userData.IsSubAgent) {
                                        this.HidSUBAgentComm = this.Payment_Info.AgentCommission;
                                      }
                                      else {
                                        this.HidAgentComm = this.Payment_Info.AgentCommission;
                                      }
                                      this.hidRCommAmtOf = this.RAgentPercentage_Info ? this.RAgentPercentage_Info.RCommAmtOf : 0;
                                      this.hidRPayOutCurrencyType = this.RAgentPercentage_Info ? this.RAgentPercentage_Info.RPayOutCurrencyType : 0;

                                      this.PayOutAmount = parseFloat((!this.PayOutAmount) ? this.PayOutAmount : "0");
                                      this.PayInAmount = parseFloat((!this.PayInAmount) ? this.PayInAmount : "0");

                                      this.PayInAmount_TextChanged();
                                    }
                                    else {
                                      alert("Agent Account does not have enough credit limit");
                                    }
                                  }
                                })

                              }

                            })

                          }

                        })

                      }
                    })


                  })
                })





              })



              // let requestData1 = {
              //   P1MD6nizSA: {
              //     agentid: this.userData.Agent_ID,
              //     PayInBranchID: this.userData.Agent_Branch_ID,
              //     PayOutBranchID: this.recommendedAgent.AgentBranch_ID,
              //     PayInCountryID: this.userData.CountryID,
              //     PayInCurrencyID: this.sendingCurrency.Currency_Id,
              //     PayOutCountryID: this.receivingCountry.CountryID,
              //     PayOutCurrencyID: this.receiveCurrency.Currency_Id,
              //     ModeOfPayment_ID: this.modeOfPayment.ModeOfpaymentId,
              //     PayInAmount: parseInt(this.payinAmount),
              //     PayOutAmount: this.payOutAmount ? this.payOutAmount : 0,
              //     AgentRate: this.ExchangeRate,
              //     Rate: this.hidRate,
              //     ReceivingAgent: this.recommendedAgent.AgentBranch_ID,
              //     Commission: Commission,
              //     SA_SC_SRC_SettelmentRate: this._SA_SC_SRC_SettelmentRate,
              //     RA_SC_RC_SRC_SettelmentRate: this._RA_SC_RC_SRC_SettelmentRate
              //   }
              // }
              // console.log('Commission', requestData);
              // var _CardType = "1";



            })
          }

        })



      });

    }



  }
  PayInAmount_TextChanged() {
    if (!this.ExchangeRate && this.ExchangeRate != "0") {

    }
  }

  LoadAgentBranchDetails() {
    let requestData = {
      P1MD6nizSA: {
        agentbranchid: this.recommendedAgent.AgentBranch_ID,
      }
    };

    this.root._AGENT_BRANCH_DETAILS(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0][0];
      console.log('AGENT_BRANCH_DETAILS', result);
      this.AgentBranchDetails = result;
    })

  }


  // SendingAgent_CurrencyExchangeRate() {
  //   let requestData = {
  //     P1MD6nizSA: {
  //       id: this.BankWise_ExchangeRate_Info.ID,
  //     }
  //   };

  //   this.root._SENDING_AGENT_CURRENCY_EXCHANGE_RATE(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
  //     let result = this.util.DecryptJson(res);
  //     console.log(result);
  //   })
  // }

  // IsAnyIDExist() {
  //   let requestData = {
  //     P1MD6nizSA: {
  //       fromcountryid: this.userData.CountryID,
  //       fromcurrencyid: this.sendingCurrency.Currency_Id,
  //       fromagentid: this.userData.Agent_ID,
  //       // tocountryid: this.receivingCountry.CountryID,
  //       active: 1,
  //     }
  //   };
  //   console.log('IsAnyIDExist', requestData);
  //   return this.root._SENDING_AGENT_CURRENCY_EXCHANGE_RATE(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
  //     let foo = this.util.DecryptJson(res);
  //     let result = this.util.DecryptJson(res).data[0];
  //     console.log('SENDING_AGENT_CURRENCY_EXCHANGE_RATE', result);
  //     // if to country missing handle check for other logic
  //     this.BankWise_ExchangeRate_Info = result.filter(x => x.FromCountry_ID == this.userData.CountryID
  //       && x.FromCurrency_Id == this.sendingCurrency.Currency_Id && x.Agent_ID == this.userData.Agent_ID)[0];
  //     return this.BankWise_ExchangeRate_Info ? true : false;
  //   });

  // }

  SetTextExchangeRate(receiveCurrency) {
    let requestData = {
      P1MD6nizSA: {
        agentbranchid: this.recommendedAgent.AgentBranch_ID,
      }
    };

    this.root._AGENT_DETAILS(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0][0];

      this.AgentDetails = result;

      let requestData = {
        P1MD6nizSA: {
          basecountry: this.userData.AgentCountry,
          baseagent: this.userData.Agent_ID,
          baseagentbranch: this.userData.Agent_Branch_ID,
          targetcountry: this.receivingCountry.CountryID,
          targetagent: result.Agent_ID,
          basecurrency: this.sendingCurrency.Currency_Id,
          targetcurrency: receiveCurrency.Currency_Id,
          basemop: this.modeOfPayment.ModeOfpaymentId,  // diff
          targetmop: this.modeOfPayment.ModeOfpaymentId,  // diff
          basesettelmentcurrency: 0,
          targetsettelmentcurrency: 0,
          permission: 5,
        }
      };
      console.log('Exchange Rate', requestData);


      this.root._DAILY_RATES(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
        let result = this.util.DecryptJson(res).data[0][0];
        // this.Exchange_rate = result;
        if (result) {
          this.Payment_Info.CostRate = parseFloat(result.FinalRate);
          this.Payment_Info.AgencyMargin = result.AgencyMargin;
          this.Payment_Info.CalculatedAgencyMargin = result.CalculatedAgencyMargin;
          this.Payment_Info.Rate = result.AgencyRate;
          this.Payment_Info.SendingAgentMargin = result.SendingAgentMargin;
          this.Payment_Info.SAVariance = result.SAVariance;
          this.Payment_Info.ExchangeRate = result.AgencyRateIncludingMarginVariance;
          this.Payment_Info.MinARate = result.MinARate;
          this.Payment_Info.SUBAMargin = result.SUBAMargin;
          this.Payment_Info.SUBAV = result.SUBAV;
          this.Payment_Info.SUBExchangeRate = result.SUBARate;
          this.Payment_Info.PayInCountry = result.PayInCountry;
          this.Payment_Info.PayInCurrencyID = result.PayInCurrencyID;
          this.Payment_Info.PayInCurrency = result.PayInCurrency;
          this.Payment_Info.PayOutCurrency = result.PayOutCurrency;
          this.Payment_Info.PayOutCurrencyID = result.PayOutCurrencyID;
          this.Payment_Info.SRSettelment_Rate = result.SRSettelment_Rate;
          this.Payment_Info.AgentRate = this.Payment_Info.ExchangeRate;
          this.Payment_Info.SubAgentRate = this.Payment_Info.SUBExchangeRate;

          console.log('Payment_Info', this.Payment_Info);

          var _CTAHoldLimit = parseFloat(this.HiddenCTAHold);
          if (_CTAHoldLimit > 0) {
            _CTAHoldLimit = -1 * _CTAHoldLimit;

          }
          if (_CTAHoldLimit <= 0) {
            let requestData = {
              P1MD6nizSA: {
                rate: this.Payment_Info.Rate,
                agentrate: this.Payment_Info.AgentRate,
                minrate: this.Payment_Info.MinARate,
                subagentrate: this.Payment_Info.SubAgentRate,
                costrate: this.Payment_Info.CostRate,
                calculateagentrate: this.Payment_Info.CalculatedAgencyMargin,
                payincountryid: this.userData.AgentCountry,
                payincurrencyid: this.sendingCurrency.Currency_Id,
                payoutcountryid: this.destinationCity.CityID,
                payoutcurrencyid: this.receiveCurrency.Currency_Id,
              }
            };
            console.log('Rate per credit limit', requestData);

            this.root._RATE_PER_CREDIT_LIMIT(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
              var response = this.util.DecryptJson(res).data[0][0];

              var _Rate = response.Rate;
              var _AgentRate = response.AgentRate;
              var _MinARate = response.MinARate;
              var _SubAgentRate = response.SubAgentRate;
              var _CostRate = parseFloat(response.CostRate);
              var _CalculatedAgencyMargin = response.CalculatedAgencyMargin;

              this.hidRate = _Rate;
              this.hidAgentRate = _AgentRate;

              if (this.userData.IsSubAgent) {

              }
              else {
                let _MinimumValue = _MinARate;
                let _MaximumValue = this.hidRate;
                this.ExchangeRate = (_MinARate == this.hidAgentRate) ? _MaximumValue : this.hidAgentRate;
              }

            })
          }

        }

      })

    })


  }


  OnChangeDestinationCity(selectedDestCity) {
    console.log('selectedDestCity', selectedDestCity);
    this.destinationCity = selectedDestCity;
    this.DDLoadRecommendedAgent(this.receivingCountry, this.modeOfPayment, selectedDestCity);
  }

  // DDLoadDestinationCurrency(selectedDestCountry) {
  //   let requestData = {
  //     P1MD6nizSA: {
  //       sendcountryid: this.userData.CountryID,
  //       countryid: selectedDestCountry.CountryID,
  //       cityid: selectedDestCity.CityID,
  //       mopid: selectedPaymentMode.ModeOfpaymentId
  //     }
  //   };

  //   this.root._DD_RECOMMENDED_AGENT(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
  //     let result = this.util.DecryptJson(res).data[0];
  //     this.find5 = result;
  //   })
  // }

  DDLoadRecommendedAgent(selectedDestCountry, selectedPaymentMode, selectedDestCity) {
    let requestData = {
      P1MD6nizSA: {
        sendcountryid: this.userData.CountryID,
        countryid: selectedDestCountry.CountryID,
        cityid: selectedDestCity.CityID,
        mopid: selectedPaymentMode.ModeOfpaymentId
      }
    };

    this.root._DD_RECOMMENDED_AGENT(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0];
      this.find5 = result;
    })
  }

  DDLoadPaymentMode(selectedDestCountry) {
    let requestData = {
      P1MD6nizSA: {
        Form: "Bank",
        countryid: selectedDestCountry.CountryID,
        PaymentMode: PaymentMode.Bank_Transfer
      }
    };

    this.root._DD_PAYMENT_MODE(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0];
      this.find3 = result;
    })
  }

  DDLoadDestinationCity(selectedDestCountry, selectedPaymentMode) {
    let requestData = {
      P1MD6nizSA: {
        sendcountryid: this.userData.CountryID,
        countryid: selectedDestCountry.CountryID,
        mopid: selectedPaymentMode.ModeOfpaymentId
      }
    };

    this.root._DD_DESTINATION_CITY(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0];
      this.find2 = result;
    })
  }

  changeFn(val) {
    this.CountryID = val.CountryID

    this.root.TRANSFER_CID(this.sel.CountryID);                        // SENDING THE COUNRY_ID TO THE ROOT SERVICE
    this.root.TRANSFER_MOP(this.sel.CountryID);                        // SENDING THE COUNRY_ID TO THE ROOT SERVICE

    if (this.sel.CountryID == ' ' || this.sel.CountryID == null) {
      console.log("Select a value");
    } else {
      // this.root._TRANSFER_CITY().subscribe((res) => {                       // THIS IS FETCHING THE CITY ACC. TO THE COUNTRY
      //   const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      //   const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      //   // console.log(msg);
      //   const c = JSON.parse(msg);
      //   const a = c.data[0];
      //   this.find2 = a;
      //   // console.log(this.find2);
      // })

      // this.root._TRANSFER_MOP().subscribe((res) => {                         // THIS IS FETCHING THE PAYMENT MODE
      //   const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      //   const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      //   // console.log(msg);
      //   const c = JSON.parse(msg);
      //   const a = c.data[0];
      //   this.find3 = a;
      //   // console.log(this.find3);
      // })
    }

  }
  OnChangeSendingCurrency(sendingCurrency) {
    // this.Currency_Id = sendingCurrency.Currency_Id
    console.log('sendingCurrency', sendingCurrency);
  }

  onChange(val) {
    this.paymentmode = val.ModeOfPayment_ID
    // console.log(this.selection.ModeOfPayment_Name);
    this.show = this.selection.ModeOfPayment_Name;
  }

  change(val) {
    this.Receivecity = val.CityID

    console.log(this.City.CityID);
    const Data = {
      "CityID": this.City.CityID,
      "CountryID": this.sel.CountryID
    };
    this.root.RECCOMND(Data);
    if (this.City.CityID == ' ' || this.City.CityID == null) {
      console.log("Select a value");
    } else {
      this.root.TRANSFER_RECCO().subscribe((res) => {                       // THIS IS FETCHING THE RECCOMENDING AGENT
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find5 = a;
        // console.log(this.find5);
      })
    }
  }
  changeagent(val) {
    this.recommendedagent = val.RecommAgent_Id

    this.root.TRANSFER_RECEIVE_CURRENCY(this.Cit.RecommAgent_Id);
    if (this.Cit.RecommAgent_Id == ' ' || this.Cit.RecommAgent_Id == null) {
      console.log("Select a value");
    } else {
      this.root.TRANSFERCURRENCY().subscribe((res) => {                       // THIS IS FETCHING THE RECCOMENDING AGENT
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[3];
        this.find6 = a;
        // console.log(this.find6);
      })
    }
  }
  receivecurrency(val) {
    this.receivingcurrency = val.Currency_Id
  }
  pot(val) {
    this.POTransfer = val.PurposeOfTransfer_ID
  }

  columnDefs = [
    { headerName: 'Customer No', field: "CustomerNo", },
    { headerName: 'Beneficiary No', field: "BeneficiaryNo", },
    { headerName: 'FirstName', field: "FirstName", },
    { headerName: 'LastName', field: "LastName", },
    { headerName: 'DateOfBirth', field: "DateOfBirth", },
    { headerName: 'Gender', field: "Gender", },
    { headerName: 'Telephone', field: "Telephone", },
    { headerName: 'EmailID', field: "EmailID", },
    { headerName: 'Address', field: "Address", },
    { headerName: 'IsKYC_Approved', field: "IsKYC_Approved", },
    { headerName: 'IsLocked', field: "IsLocked", },
  ];

  defaultColDef = {
    filter: true,
    sortable: true,
  };
  rowData = [];
  // columnDefs = [];
  gridOptions = <GridOptions>{
    columnDefs: this.columnDefs,
    rowData: this.rowData,
  }


  // app.component.ts
  getVisitorID(): void {
    const fpPromise = FingerprintJS.load({
      apiKey: 'F4XmDdtddUV3YImFL1DK', region: 'ap'
    })
    fpPromise
      .then(fp => fp.get())
      .then((result) => {
        this.visitorId = result.visitorId
      })
  }


  ngOnInit(): void {
    this.userData = JSON.parse(sessionStorage.getItem("userdata"));
    console.log('userData', this.userData);

    //  GETTING THE CUSTOMER DATA   & BENE DATA
    this.Customer_Data = localStorage.getItem('CustmoreNo')
    this.Customer_Data = JSON.parse(this.Customer_Data);
    console.log('Customer_Data', this.Customer_Data);

    // this.Bene_Data = localStorage.getItem('Benedata')
    // console.log('Bene_Data', this.Bene_Data);

    this.Agent_data = sessionStorage.getItem('userdata')

    this.getVisitorID()

    this.partnercode = localStorage.getItem('partnercode');


    this.tracker.send()

    const send = localStorage.getItem("CustmoreNo");
    const sender = JSON.parse(send);
    this.name = sender.FirstName;
    this.country = sender.CountryName;
    this.email = sender.EmailID;
    this.root.CUSTOMER_BENE_DETAILS(sender.CustomerNo);

    this.root._TRANSFER_POT().subscribe(res => {                // THIS IS FETCHING THE PURPOSE OF TRANSFER
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      this.find = c.data[0];
      console.log(this.find);
      // console.log(msg);
    })

    // this.root._TRANSFER_RC().subscribe(res => {                // THIS IS FETCHING THE RECEIVING COUNTRIES
    //   const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
    //   const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
    //   const c = JSON.parse(msg);
    //   this.find1 = c.data[0];
    //   this.RECEIVING_COUNTRY = c.data[0];         //THIS SETTING UP THE DEFAULT VALUE
    //   // console.log(this.find1);
    // })

    this.root.TRANSFER_CUR().subscribe(res => {                // THIS IS FETCHING THE RECEIVING CURRENCY
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      //   console.log(c)
      this.find4 = c.data[1];
      //   this.find4 = c.data[3];
      //   this.sending_currency = c.data[3][0].CurrencyShortName;    //THIS SETTING UP THE DEFAULT VALUE
      //   console.log(this.sending_currency)
      // console.log(this.find4);
    })

    this.root.TRANS_NUM(this.partnercode);
    this.root.TRANSACTION_NUM().subscribe(res => {                // THIS IS FETCHING THE TRANSACTION NUMBER
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const number = c.data[0]
      const transaction_num = number[0].TransactionNo
      // console.log(transaction_num);
      this.transaction_number = transaction_num
    })

    this.root._BENE_LIST().subscribe(res => {                // THIS IS FETCHING THE REGISTERED BENEFICIARY
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      this.find7 = c.data;
      //   console.log(this.find7);


      //---------------- THIS IS FOR SETTING THE DYNAMIC COLUMN & ROWS
      const col = JSON.stringify(this.find7);
      const column = JSON.parse(col);
      const z = column[0];

      //  const colDefs = this.gridOptions.api.getColumnDefs();
      //  colDefs.length=0;
      //  const keys = Object.keys(column[0])
      //  keys.forEach(key => colDefs.push({field : key}));
      //  this.gridOptions.api.setColumnDefs(colDefs);
      //  this.gridOptions.api.setRowData(column);
      this.gridOptions.api.setRowData(z);
    })

    this.LoadSenderAvailableCreditLimit();
    this.DDLoadDestionationCountry();
    this.DDLoadPurposeOfTransfer();

  }
  LoadSenderAvailableCreditLimit() {
    let requestData = {
      P1MD6nizSA: {
        agentid: this.userData.Agent_ID,
      }
    };

    this.root._LOAD_SENDER_AVAIL_CREDIT_LIMIT(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      var result = this.util.DecryptJson(res).data[0][0];
      console.log('Agent Data', result)

      if (result) {
        this.AgentData = result;
        if (this.userData.IsSubAgent) {

        }
        else {
          this.HiddenBankLimit = this.AgentData.BankLimit;
          this.HiddenCTAHold = this.AgentData.CTAHoldLimit;
          this.ViewCTALimitRateMsgViewState = this.HiddenCTAHold <= 0 ? false : true;
        }
      }
    })
  }

  // ngOnInit Ends

  DDLoadDestinationCurrency(recommendedAgent) {
    let requestData = {
      P1MD6nizSA: {
        agentid: recommendedAgent.AgentBranch_ID,
      }
    };
    //call this 
    //sp_View_AgentBranchDetails
    // @AgentBranch_ID
    // this passed received Agentid to below API

    this.root._DD_DESTINATION_CURRENCY(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      var response = this.util.DecryptJson(res);
      console.log('Receiving Currency', response)
      let result = this.util.DecryptJson(res).data[1];
      this.find6 = result;
    })
  }

  DDLoadPurposeOfTransfer() {
    let requestData = {
      P1MD6nizSA: {
        basetypeid: PaymentMode.Bank_Transfer,
      }
    };

    this.root._DD_PURPOSE_OF_TRANSFER(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0];
      this.find = result;
    })
  }

  DDLoadDestionationCountry() {
    let requestData = {
      P1MD6nizSA: {
        Form: "Bank",
        BaseCountryID: this.userData.CountryID,
        PermissionID: Permission.Send
      }
    };

    this.root._DD_DESTINATION_COUNTRY(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0];
      this.find1 = result;
    })

  }

  async getGithubData() {
    let res = await axios.get('https://api.ipify.org/?format=json');
    this.userIP = res.data.ip;
  }

  calc_time() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1.; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    var time = dateObj.getHours() + ":" + dateObj.getMinutes() + ":" + dateObj.getSeconds();
    const newdate = year + "-" + month + "-" + day + " " + time;
    // console.log(newdate);
    this.time = newdate
  }

  submit() {

    this._SA_SC_SRC_SettelmentRate = parseFloat(this.HiddenSASettlementRate);
    this.CurrentTrxnAmt = (parseFloat(this.lblTotalPayable) / parseFloat(this._SA_SC_SRC_SettelmentRate)).toFixed(2);



    // if (this.IDNumber) {

    //   if (this.Mobile) {

    //     let requestData = {
    //       P1MD6nizSA: {
    //         asondate: "2022-10-06",
    //         customermobile: this.RIsdCode,
    //       }
    //     };

    //     this.root.GET_PAYIN_USD_AMT(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
    //       let result = this.util.DecryptJson(res);

    //       if (result.TotalUSDAmt >= 2500) {
    //         if (parseFloat(this.CurrentTrxnAmt) >= 2500) {
    //           //this.lblIDRequired = "ID Required to send " + PayInAmount.ToString() + " " + this.lblPayInCurrency + " amount.";
    //           alert("ID Required to send " + this.payinAmount + " " + this.sendingCurrency.Currency + " amount.");
    //         }
    //         else {
    //           alert("ID Required because sending Amount is greater than 2500 USD within 30 Days");
    //         }
    //       }


    //     })

    //   }


    // }

    this.PreProcess();

  }

  PreProcess() {

    // Skipped DOB validation logic
    {
      this._TempTable.Agent_ID = this.userData.Agent_ID;
      this._TempTable.RecommendedAgent_ID = this.recommendedAgent.AgentBranch_ID;
      this._TempTable.TrnCodeForSAgent = true;
      this._TempTable.TrnCodeForRAgent = true;
      this._TempTable.ManualTransactionNo = this.userData.IsManualEntry ? "" : "";
      this._TempTable.PartnerCode = this.partnercode;

      let requestData = {
        P1MD6nizSA: {
          partnercode: this.partnercode,
          recommagentid: this.AgentBranchDetails.Agent_ID
        }
      };

      this.root.GET_RANDOM_TRANSACTION_NUM(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
        let result = this.util.DecryptJson(res);
        this._TempTable.TransactionNumber = result.data[0][0].TransactionNo;

        var TransactionType = {
          BT: 1,
          CT: 2,
          DT: 3,
          BTR: 4,
          CTR: 5,
          DTR: 6,
          FD: 12,
          TC: 13,
          PFD: 14,
          DL: 15,
          SFD: 24,
          CE: 22,
          JV: 7,
          BP: 8,
          BR: 9,
          CP: 10,
          CR: 11,
          UP: 29,
        }

        this._TempTable.TrTypeID = TransactionType.BT;
        this._TempTable.ModeOfPayment_ID = this.modeOfPayment.ModeOfpaymentId;
        this._TempTable.PurposeOfTransfer = this.purposeOfTransfer.PurposeOfTransfer_ID;
        this._TempTable.PurposeSpecify = "";
        this._TempTable.TrDate = new Date().toLocaleString();
        this._TempTable.TrTm = new Date().toLocaleString();
        this._TempTable.ManualSuspected = 1;
        this._TempTable.SourceOfIncome = "";
        this._TempTable.SourceOfFund = this.Customer_Data.SourceOfFund;
        this._TempTable.ProofOfFund = "";
        this._TempTable.IsArebic = false;
        this._TempTable.SenderAgentBaseCCY = this.userData.AgentCurrency;
        this._TempTable.Sending_SettlementCurrency = this.userData.SettlementCurrency;
        this._TempTable.BaseCountry = this.userData.CountryID;
        this._TempTable.BaseCurrency = this.userData.Base_Currency_Id;
        this._TempTable.Ex_Branch_ID = this.userData.Ex_Branch_ID;
        this._TempTable.StartTime = new Date().toLocaleString();
        this._TempTable.EndTime = new Date().toLocaleString();

        this._TempTable.Customer_Name_F = this.Customer_Info.FirstName;
        this._TempTable.Customer_Name_M = this.Customer_Info.MiddleName;
        this._TempTable.Customer_Name_L = this.Customer_Info.LastName;
        this._TempTable.Risk_Level = 0; // or 1
        this._TempTable.Customer_Gender = this.Customer_Info.Gender;
        this._TempTable.Customer_Type = this.Customer_Info.CustomerType ? this.Customer_Info.CustomerType : 0;
        this._TempTable.Customer_Resident = "";
        this._TempTable.Cust_IDPlaceOfIssue = "";
        this._TempTable.Cust_IDIssueCountry = "";
        this._TempTable.Customer_Address = this.Customer_Info.Address;
        this._TempTable.Customer_Mobile = this.Customer_Info.Telephone;
        this._TempTable.Customer_Phone = this.Customer_Info.Telephone;
        this._TempTable.Customer_ZipCode = null; // prob -- not maintaining 
        this._TempTable.Customer_NationalityID = this.Customer_Info.Nationality; // prob -- getting ''

        this._TempTable.Customer_DOB = this.Customer_Info.DateOfBirth;
        this._TempTable.Customer_Relation = this.Bene_Info.CustomerRelation;   // prob

        this._TempTable.Customer_Email = "";
        this._TempTable.Customer_MaritalStatus = "";
        this._TempTable.Customer_WorkAddress = "";
        this._TempTable.Customer_PlaceofBirth = "";
        this._TempTable.Customer_City = "";
        this._TempTable.Customer_State = "";
        this._TempTable.IDISSUERCountry = "";
        this._TempTable.Beneficiary_Name_F = this.Bene_Info.FirstName;
        this._TempTable.Beneficiary_Name_M = this.Bene_Info.MiddleName;
        this._TempTable.Beneficiary_Name_L = this.Bene_Info.LastName;
        this._TempTable.Beneficiary_Address = this.Bene_Info.Address;
        this._TempTable.Beneficiary_Mobile = this.Bene_Info.Telephone;
        this._TempTable.Beneficiary_Phone = "";
        this._TempTable.Beneficiary_NationalityID = this.Bene_Info.Nationality;
        this._TempTable.Beneficiary_MessageToBen = "";
        this._TempTable.Beneficiary_MessageToAgent = "";
        this._TempTable.Beneficiary_LandMark = this.Bene_Info.LandMark;
        this._TempTable.Beneficiary_Gender = null;
        this._TempTable.Ben_Type = 0;
        this._TempTable.Beneficiary_Email = this.Bene_Info.EmailID;
        this._TempTable.Beneficiary_MaritalStatus = "";
        this._TempTable.Beneficiary_WorkAddress = "";
        this._TempTable.Beneficiary_DeliveryAddress = "";
        this._TempTable.Beneficiary_City = "";
        this._TempTable.Beneficiary_Region = "";
        this._TempTable.Beneficiary_ZipCode = this.Bene_Info.ZipCode;

        this._TempTable.Type_ID = this.Bene_Info.IDType;
        this._TempTable.IDNumber = this.Bene_Info.IDNumber;
        this._TempTable.PayOutCurrency = this.receiveCurrency.Currency_Id;
        this._TempTable.PayOutCountry = this.receivingCountry.CountryID;
        this._TempTable.PayOutCityID = this.receiveCity.CityID;
        this._TempTable.PayOutLocationID = 0; //confirm
        this._TempTable.BDO_Branch_ID = this.AgentBranchDetails.AgentBranch_ID;

        this._TempTable.AgentFC_Amount = parseFloat(this.payOutAmount);
        this._TempTable.AgentLC_Amount = parseFloat(this.payinAmount);
        if (this.userData.IsSubAgent) {
          this._TempTable.ActualAgentRate = parseFloat(this.hidAgentRate);
        }
        else {
          this._TempTable.ActualAgentRate = parseFloat(this.ExchangeRate);
        }
        this._TempTable.AgentRate = this.ExchangeRate;
        this._TempTable.Rate = parseFloat(this.hidRate);
        this._TempTable.CostRate = parseFloat(this.Payment_Info.CostRate);
        this._TempTable.BaseToBaseCommissionAmount = parseFloat(this.HidComm);
        this._TempTable.AgentCommVariance = (this.HidAgentComm) ? 0 : parseFloat(this.HidAgentComm);
        this._TempTable.SUBAgentCommVariance = (this.HidSUBAgentComm) ? 0 : parseFloat(this.HidSUBAgentComm);
        this._TempTable.SRSettelment_Rate = parseFloat(this.Payment_Info.SRSettelment_Rate);

        this._TempTable.PayCommissionType = parseInt((this.hidPayCommissionType) ? this.hidPayCommissionType : 0);
        this._TempTable.PayInCommissionType = parseInt((this.hidPayInCommissionType) ? this.hidPayInCommissionType : 0);
        this._TempTable.PayInCurrencyType = parseInt((this.hidPayInCurrencyType) ? this.hidPayInCurrencyType : 0);
        this._TempTable.PayInCommissionAmount = parseFloat(this.hidPayInCommissionAmount);
        this._TempTable.SPercentage = parseFloat(this.hidSPercentage);
        this._TempTable.SPayCommissionType = parseInt(this.hidSPayCommissionType);
        this._TempTable.SMinimumComm = parseFloat(this.hidSMinimumCommission);
        this._TempTable.SMaximumComm = parseFloat(this.hidSMaximumCommission);
        this._TempTable.SUBPayInCommissionType = parseInt((this.hidSUBPayInCommissionType) ? this.hidSUBPayInCommissionType : 0);
        this._TempTable.SUBPayInCommissionAmount = parseFloat(this.hidSUBPayInCommissionAmount);

        this._TempTable.PayOutCommissionType = parseInt((this.hidPayOutCommissionType) ? this.hidPayOutCommissionType : 0);
        this._TempTable.PayOutCurrencyType = parseInt((this.hidPayOutCurrencyType) ? this.hidPayOutCurrencyType : 0);
        this._TempTable.PayOutCommissionAmount = parseFloat(this.hidPayOutCommissionAmount);
        this._TempTable.RPercentage = parseFloat(this.hidRPercentage);
        this._TempTable.RPayCommissionType = parseInt(this.hidRPayCommissionType);
        this._TempTable.RMinimumComm = parseFloat(this.hidRMinimumCommission);
        this._TempTable.RMaximumComm = parseFloat(this.hidRMaximumCommission);
        this._TempTable.RCommAmtOf = parseInt(this.hidRCommAmtOf);
        this._TempTable.RPayOutCurrencyType = parseInt(this.hidRPayOutCurrencyType);

        this._TempTable.CourierAddress = "";
        this._TempTable.CourierRemark = "";

        this._TempTable.BankAccountNo = (this.Bene_Info.AccountNumber) ? this.Bene_Info.AccountNumber : "";
        this._TempTable.BankName = (this.Bene_Info.BankName) ? this.Bene_Info.BankName : "";
        this._TempTable.BankBranch = (this.Bene_Info.BankBranch) ? this.Bene_Info.BankBranch : "";
        this._TempTable.BankAddress = (this.Bene_Info.Address) ? this.Bene_Info.Address : "";
        this._TempTable.BankCode = (this.Bene_Info.BankCoder) ? this.Bene_Info.BankCoder : "";
        this._TempTable.IFSCType = "";

        this._TempTable.DDNumber = "";
        this._TempTable.DDBank = "";
        this._TempTable.DDBranch = "";
        this._TempTable.DDBankAddress = "";

        this._TempTable.StudentName = "";
        this._TempTable.StudentID = "";
        this._TempTable.StudReferenceName = "";
        this._TempTable.StudentEnrollNO = "";
        this._TempTable.EnteredBy = this.userData.UserId;

        this._TempTable.VATAmount = parseFloat((this.hdnVATAmt) ? this.hdnVATAmt : 0);
        this._TempTable.VATPercentage = parseFloat(this.hdnVATPer ? this.hdnVATPer : 0);
        this._TempTable.CommIncVAT = parseFloat(this.AgentSlab_Info.BaseToBaseCommissionAmount) + parseFloat(this.Payment_Info.VATAmount ? this.Payment_Info.VATAmount : 0);
        
        let requestData = {
          P1MD6nizSA: {
            fromcountryid: this.userData.CountryID,
            fromcurrencyid: this.sendingCurrency.Currency_Id,
            tocountryid: this.receivingCountry.CountryID,
            tocurrencyid: this.receiveCurrency.Currency_Id,
            agentid: this.AgentBranchDetails.Agent_ID,
            mop: this.modeOfPayment.ModeOfpaymentId,
            active: 1
          }
        };


        
        this.ProcessForTrDetails();



      })
    }
  }

  ProcessForTrDetails() {
    let requestData = {
      P1MD6nizSA: {
        basecountry: this.userData.AgentCountry,
        baseagent: this.userData.Agent_ID,
        targetcountry: this.receivingCountry.CountryID,
        targetagent: this.AgentDetails.Agent_ID,
        basecurrency: this.sendingCurrency.Currency_Id,
        targetcurrency: this.receiveCurrency.Currency_Id,
        basemop: this.modeOfPayment.ModeOfpaymentId,
        targetmop: this.modeOfPayment.ModeOfpaymentId,
      }
    };

    console.log('vw_DailyRates', JSON.stringify(requestData));

    return this.root.DAILY_RATES_2(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0][0];
      console.log('vw_DailyRates', result);

      this._OfferRate = parseFloat(result.RateForSendingCurrency);
      this._BidRate = parseFloat(result.RateForReceivingCurrency);
      this._Receiver_OfferRate = result.RateForSendingCurrency;
      this.DailyRates = result;

      let requestData = {
        P1MD6nizSA: {
          agentid: this.AgentBranchDetails.Agent_ID,
        }
      };

      return this.root.RECEIVE_AGENT_SETTLEMENT_CURR(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
        let result = this.util.DecryptJson(res).data[0][0];
        console.log('RECEIVE_AGENT_SETTLEMENT_CURR Result', result);
        this._Receiving_SettlementCurrency = result.SettlementCurrency;

        // skipped if logic
        this._SenderBase_Rate = 1;

        let requestData = {
          P1MD6nizSA: {
            countryid: this.receivingCountry.CountryID
          }
        };

        return this.root.COUNTRY(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
          let result = this.util.DecryptJson(res).data[0][0];
          console.log('COUNTRY Result', result);

          this._ReceiverAgentBaseCCY = result.BaseCurrencyID;

          if (this.Payment_Info.PayOutCurrencyID == this._ReceiverAgentBaseCCY) {
            this._ReceiverBase_Rate = 1;
          }
          else {

          }

          let requestData = {
            P1MD6nizSA: {
              branchcode: this.recommendedAgent.AgentBranch_ID
            }
          };

          return this.root.EXCHANGE_BRANCH_PAYOUT(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
            let result = this.util.DecryptJson(res).data[0][0];
            console.log('EXCHANGE_BRANCH_PAYOUT Result', result);

            this._BaseCurrency = result.Base_Currency_Id;
            this._Sending_SettlementCurrency = this._Receiving_SettlementCurrency;

            if (this._Sending_SettlementCurrency != this._BaseCurrency) {

            }
            else {
              this.BankWise_ExchangeRateData;
              this._SA_SC_SRC_SettelmentRate = parseFloat(this.BankWise_ExchangeRateData.Settelment_Rate);
              this._RA_SC_RC_SRC_SettelmentRate = parseFloat(this.BankWise_ExchangeRateData.Settelment_Rate);

              if (this.DailyRates.PayInCurrencyID != 1 && this.BANK_SENDING_CURRENCY_CER.SettlementCurrency != 1) {

              }
              else {

                let requestData = {
                  P1MD6nizSA: {
                    "settlementcurrency": this.BANK_SENDING_CURRENCY_CER.SettlementCurrency,
                    "currencyid": this.DailyRates.PayInCurrencyID,
                  }
                };

                return this.root.CURR_US_WISE_EXCHANGE_RATE(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                  let result = this.util.DecryptJson(res).data[0][0];
                  console.log('CURR_US_WISE_EXCHANGE_RATE Payin Result', result);
                  this.ExchangeRateDataPayin = result;
                  this._SC_SRC_SettelmentRate = parseFloat(this.ExchangeRateDataPayin.Settelment_Rate);

                  let requestData = {
                    P1MD6nizSA: {
                      "settlementcurrency": this.BANK_SENDING_CURRENCY_CER.SettlementCurrency,
                      "currencyid": this.DailyRates.PayOutCurrencyID,
                    }
                  };

                  return this.root.CURR_US_WISE_EXCHANGE_RATE(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                    let result = this.util.DecryptJson(res).data[0][0];
                    console.log('CURR_US_WISE_EXCHANGE_RATE Payout Result', result);
                    this.ExchangeRateDataPayout = result;
                    this._RC_SRC_SettelmentRate = parseFloat(this.ExchangeRateDataPayout.Settelment_Rate);

                    let requestData = {
                      P1MD6nizSA: {
                        "settlementcurrency": this._Sending_SettlementCurrency,
                        "currencyid": this._BaseCurrency,
                      }
                    };

                    return this.root.CURR_US_WISE_EXCHANGE_RATE(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                      let result = this.util.DecryptJson(res).data[0][0];
                      console.log('CURR_US_WISE_EXCHANGE_RATE _SA_STC_SRC_SettelmentRate Result', result);
                      this._SA_STC_SRC_SettelmentRate = parseFloat(result.Settelment_Rate);

                      let requestData = {
                        P1MD6nizSA: {
                          "settlementcurrency": this._Receiving_SettlementCurrency,
                          "currencyid": this._BaseCurrency,
                        }
                      };

                      return this.root.CURR_US_WISE_EXCHANGE_RATE(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                        let result = this.util.DecryptJson(res).data[0][0];
                        console.log('CURR_US_WISE_EXCHANGE_RATE _RA_STC_SRC_SettelmentRate Result', result);
                        this._RA_STC_SRC_SettelmentRate = parseFloat(result.Settelment_Rate);


                        if (this._Receiving_SettlementCurrency == this._BaseCurrency) {
                          this._RASettelmentRate = parseFloat(this._RA_SC_RC_SRC_SettelmentRate);
                        }
                        else {
                          this._RASettelmentRate = parseFloat(this._RA_STC_SRC_SettelmentRate);
                        }

                        // let requestData = {
                        //   P1MD6nizSA: {
                        //     "srtrno": this._TempTable.TransactionNumber,
                        //   }
                        // };

                        // return this.root.SP_TRMASTER(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
                        //   let result = this.util.DecryptJson(res);//.data[0][0];
                        //   console.log('SP_TRMASTER Result', result);

                        // })

                        this.TR_MASTER_TEMP();

                      })




                    })

                  })


                });

              }

            }

          })





        })

      })


    });
  }

  TR_MASTER_TEMP() {
    let requestData = {
      P1MD6nizSA: {
        TransactionNumber: this._TempTable.TransactionNumber,
        MemberShipID: 0,
        PadalaCardNo: "", // prob
        ThirdPartyCode: 0,
        Customer_Name_F: this._TempTable.Customer_Name_F,
        Customer_Name_M: this._TempTable.Customer_Name_M,
        Customer_Name_L: this._TempTable.Customer_Name_L,
        Customer_Address: this._TempTable.Customer_Address,
        Customer_Mobile: this._TempTable.Customer_Mobile,
        Customer_Phone: "",
        Customer_ZipCode: this._TempTable.Customer_ZipCode,
        Customer_NationalityID: this._TempTable.Customer_NationalityID,
        MembershipType: 0,
        Customer_DOB: this.datepipe.transform(this._TempTable.Customer_DOB, 'yyyy-MM-dd'),
        Customer_Relation: this._TempTable.Customer_Relation,
        Customer_Gender: this._TempTable.Customer_Gender,
        Customer_Email: this._TempTable.Customer_PlaceofBirth,
        Customer_MaritalStatus: 0,
        Customer_WorkAddress: 0,
        Customer_PlaceofBirth: 0,
        Customer_PassportID: 0,
        Beneficiary_Name_F: this._TempTable.Beneficiary_Name_F,
        Beneficiary_Name_M: this._TempTable.Beneficiary_Name_M,
        Beneficiary_Name_L: this._TempTable.Beneficiary_Name_L,
        Beneficiary_Address: this._TempTable.Beneficiary_Address,
        Beneficiary_Mobile: this._TempTable.Beneficiary_Mobile,
        Beneficiary_Phone: "",
        Beneficiary_NationalityID: this._TempTable.Beneficiary_NationalityID,
        Beneficiary_MessageToBen: "",
        Beneficiary_MessageToAgent: "",
        Beneficiary_LandMark: this._TempTable.Beneficiary_LandMark,
        Beneficiary_Gender: this._TempTable.Beneficiary_Gender,  // prob -- not available
        Beneficiary_Email: "",
        Beneficiary_MaritalStatus: "",
        Beneficiary_WorkAddress: "",
        Beneficiary_DeliveryAddress: "",
        Beneficiary_City: "",
        Beneficiary_Region: "",
        Beneficiary_ZipCode: this._TempTable.Beneficiary_ZipCode,
        Beneficiary_DOB: this.datepipe.transform(new Date(), 'yyyy-MM-dd'), //this.Bene_Info.DateOfBirth,
        Type_ID: this._TempTable.Type_ID,
        IDNumber: this._TempTable.IDNumber,
        IDExpDt: this.datepipe.transform(this.Bene_Info.IDExpiryDate, 'yyyy-MM-dd'),
        IDIssueDt: this.datepipe.transform(this.Bene_Info.IDIssueDate, 'yyyy-MM-dd'),
        ReceiverIDType: 0, // prob
        ReceiverIDNumber: 0, // prob
        ReceiverIDExpDt: this.datepipe.transform(new Date(), 'yyyy-MM-dd'),
        ReceiverIDIssueDt: this.datepipe.transform(new Date(), 'yyyy-MM-dd'),
        TrTypeID: this._TempTable.TrTypeID,
        PayInCountry: this.Payment_Info.PayInCountry,
        PayInCurrency: this.Payment_Info.PayInCurrencyID,
        AgentBranch_ID: this.userData.Agent_Branch_ID,
        SendingAgentMargin: 0,
        AgencyMargin: this.Payment_Info.CalculatedAgencyMargin,
        PayOutCurrency: this._TempTable.PayOutCurrency,
        PayOutCountry: this._TempTable.PayOutCountry,
        PayOutCityID: this._TempTable.PayOutCityID,
        PayOutLocationID: this.AgentBranchDetails.LocationID,
        RecommendedAgent_ID: this.AgentBranchDetails.Agent_ID,
        BDO_Branch_ID: this._TempTable.BDO_Branch_ID,
        AgentFC_Amount: this._TempTable.AgentFC_Amount,
        AgentLC_Amount: this._TempTable.AgentLC_Amount,
        AgentRate: this._TempTable.AgentRate,
        ActualAgentRate: this._TempTable.ActualAgentRate,
        RASettelmentRate: parseFloat(this._RASettelmentRate),
        Rate: parseFloat(this._TempTable.Rate),
        CostRate: this._TempTable.CostRate,
        BaseToBaseCommissionAmount: parseFloat(this._TempTable.BaseToBaseCommissionAmount),
        AgentCommissionVariance: this._TempTable.AgentCommVariance,
        SUBAgentCommissionVariance: this._TempTable.SUBAgentCommVariance ? this._TempTable.SUBAgentCommVariance : 0,
        ModeOfPayment_ID: this._TempTable.ModeOfPayment_ID,
        PurposeOfTransfer: this._TempTable.PurposeOfTransfer,
        PurposeSpecify: this._TempTable.PurposeSpecify,
        PayCommissionType: this._TempTable.PayCommissionType,
        PayInCommissionType: this._TempTable.PayInCommissionType,
        SUBPayInCommissionType: this._TempTable.SUBPayInCommissionType ? this._TempTable.SUBPayInCommissionType : 0,
        PayInCurrencyType: this._TempTable.PayInCurrencyType,
        PayInCommissionAmount: this._TempTable.PayInCommissionAmount,
        SUBPayInCommissionAmount: this._TempTable.SUBPayInCommissionAmount ? this._TempTable.SUBPayInCommissionAmount : 0,
        senderpercentcomm: "",
        SPercentage: this._TempTable.SPercentage,
        SPayCommissionType: this._TempTable.SPayCommissionType,
        SMinimumComm: this._TempTable.SMinimumComm,
        SMaximumComm: this._TempTable.SMaximumComm,
        PayOutCommissionType: this._TempTable.PayOutCommissionType,
        PayOutCurrencyType: this._TempTable.PayOutCurrencyType,
        PayOutCommissionAmount: this._TempTable.PayOutCommissionAmount,
        RPercentage: this._TempTable.RPercentage,
        RPayCommissionType: this._TempTable.RPayCommissionType,
        RMinimumComm: this._TempTable.RMinimumComm,
        RMaximumComm: this._TempTable.RMaximumComm,
        RCommAmtOf: this._TempTable.RCommAmtOf,
        RPayOutCurrencyType: this._TempTable.RPayOutCurrencyType,
        receivepercomm: "", //  optional
        TrDate: this.datepipe.transform(this._TempTable.TrDate, 'yyyy-MM-dd HH:mm:ss'),
        TrTm: this.datepipe.transform(this._TempTable.TrTm, 'yyyy-MM-dd HH:mm:ss'),
        CourierAddress: 0, //  optional
        CourierRemark: 0, //  optional
        ManualSuspected: 1,
        SourceOfIncome: "", //  optional
        SourceOfFund: 0, //  optional
        ProofOfFund: 0, //  optional
        IsArebic: this._TempTable.IsArebic,
        BankAccountNo: 0, //  optional
        BankName: 0, //  optional
        BankBranch: 0, //  optional
        BankAddress: 0, //  optional
        BankCode: 0, //  optional
        IFSCType: 0, //  optional
        DDNumber: 0, //  optional
        DDBank: 0, //  prob
        DDBranch: 0, //  optional
        DDBankAddress: 0, //  optional
        EnteredBy: this._TempTable.EnteredBy,
        ProcessComplete: 0,
        SendAgent: this.userData.Agent_ID,
        SenderAgentBaseCCY: this._TempTable.SenderAgentBaseCCY,
        Sending_SettlementCurrency: this._TempTable.Sending_SettlementCurrency,
        BaseCountry: this._TempTable.BaseCountry,
        BaseCurrency: this._TempTable.BaseCurrency,
        Ex_Branch_ID: this._TempTable.Ex_Branch_ID,
        IsSendingToSR1: 0, // prob, what field?
        SRSettelment_Rate: this._TempTable.SRSettelment_Rate,
        SendToSR1: 0,
        StartTime: this.datepipe.transform(this._TempTable.StartTime, 'yyyy-MM-dd HH:mm:ss'),
        EndTime: this.datepipe.transform(this._TempTable.EndTime, 'yyyy-MM-dd HH:mm:ss'),
        Receiver_OfferRate: parseFloat(this.DailyRates.RateForSendingCurrency),
        SenderBase_Rate: this._SenderBase_Rate,
        ReceiverBase_Rate: this._ReceiverBase_Rate,
        SA_SC_SRC_SettelmentRate: parseFloat(this._SA_SC_SRC_SettelmentRate),
        SA_STC_SRC_SettelmentRate: parseFloat(this._SA_STC_SRC_SettelmentRate),
        SC_SRC_SettelmentRate: parseFloat(this._SC_SRC_SettelmentRate),
        RA_SC_RC_SRC_SettelmentRate: parseFloat(this._RA_SC_RC_SRC_SettelmentRate),
        RA_STC_SRC_SettelmentRate: parseFloat(this._RA_STC_SRC_SettelmentRate),
        RC_SRC_SettelmentRate: parseFloat(this._RC_SRC_SettelmentRate),
        BidRate: this._BidRate,
        OfferRate: this._OfferRate,
        IsCancelReq: 0,
        CancelRemark: 0,
        IsForPayOut: 0,
        PayOut_Branch_ID: 0,
        PayOut_By: 0,
        PaidDate: this.datepipe.transform(new Date(), 'yyyy-MM-dd'),
        ROtherType_ID: 0,
        BOtherType_ID: 0,
        StudentName: 0,
        StudentID: 0,
        StudReferenceNO: 0,
        StudentEnrollNO: 0,
        Customer_City: 0,
        Customer_State: 0,
        Customer_IDIssuer: 0,
        CardEarnPoints: 0,
        CardDiscountAmount: 0,
        VATAmount: this._TempTable.VATAmount ? this._TempTable.VATAmount : 0,
        VATPerCentage: this._TempTable.VATPercentage ? this._TempTable.VATPercentage : 0,
        CommIncVAT: parseFloat(this._TempTable.CommIncVAT),
        ActualRecommendedBranchID: parseInt(this.AgentBranchDetails.Agent_ID),    // prob
        BeneBankBranchCode: 0,
        RiskLevel: 'NATURAL PERSON', // prob -- set null
        Cust_Type: 'INDIVIDUAL',
        Ben_Type: 'INDIVIDUAL',
        Customer_Resident: this._TempTable.Customer_Resident,
        Cust_IDPlaceOfIssue: this._TempTable.Cust_IDPlaceOfIssue,
        Cust_IDIssueCountry: this._TempTable.Cust_IDIssueCountry,
        PartnerCode: this.partnercode,
      }
    };

    console.log('Final request', JSON.stringify(requestData));
    return this.root.SUBMIT_TR_MASTER_TEMP(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res);//.data[0][0];
      console.log('Finale Result', result);

    })

  }

  IsNotNullOrEmpty(value) {
    if (value != null && value != "") {
      return true;
    }
    return false;
  }


  onRowClicked(event: any) {
    const data = event.data;
    localStorage.setItem('Benedata', JSON.stringify(data));
    console.log('Bene_Data', data);

    document.getElementById('container').style.height = "120vh"  //CHNAGED THE CONTAINER HEIGHT

    this.Data = JSON.stringify(event.data);
    const now = JSON.parse(this.Data);
    this.names = now.FirstName;
    this.countrys = now.CountryName;
    this.emails = now.EmailID;

    let requestData = {
      P1MD6nizSA: {
        custid: this.Customer_Data.CustomerNo,
        partnercode: this.Customer_Data.PARTNERCODE
      }
    };

    this.root.CUSTOMER_DATA(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
      let result = this.util.DecryptJson(res).data[0][0];
      this.Customer_Info = result;
      console.log('Customer API Data', result);


      let Bene_Data = JSON.parse(localStorage.getItem('Benedata'));
      console.log('Bene_Data', Bene_Data);

      let requestData = {
        P1MD6nizSA: {
          custid: this.Customer_Data.CustomerNo,
          beneid: Bene_Data.BeneficiaryNo
        }
      };

      this.root.BENEFICIARY_DATA(this.util.Crypt(JSON.stringify(requestData))).subscribe(res => {
        let result = this.util.DecryptJson(res).data[0][0];
        this.Bene_Info = result;
        console.log('Bene API Data', result);
      })

    })

    // this.sel = this.countrys;
  }

}


