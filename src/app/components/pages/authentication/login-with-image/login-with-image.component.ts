import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RootService } from 'src/app/root.service';
import * as CryptoJS from 'crypto-js';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { ShareddataService } from 'src/app/shareddata.service';
import { TrackerService } from '../../tracker.service';




@Component({
  selector: 'app-login-with-image',
  templateUrl: './login-with-image.component.html',
  styleUrls: ['./login-with-image.component.scss']
})
export class LoginWithImageComponent implements OnInit {


  password;
  name: any;

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    password: new FormControl('', Validators.required)
  });

  get f(){
    return this.form.controls;
  }



  constructor(private router: Router,
    private root:RootService,
    private datashare:ShareddataService,
    private tracker:TrackerService) { }

  ngOnInit() {
    this.tracker.send()
    // const obs$ = interval(10000);
    //  obs$.subscribe(() => {
    //    const c = document.cookie.match(/^(.*;)?\s*connect.sid\s*=\s*[^;]+(.*)?$/)
    //    console.log(c)
    //    if (c == null) {
    //     this.router.navigateByUrl('/');
    //   }
    //   else{}
    // });

  }

  submit(){
    console.log(this.form.value);
  }

  getvalues(val){
    this.name = val.name;
    this.password = val.password;
    console.log(this.name + this.password);
    // this.datashare.setData(this.name);   //PASSING THE VALUE TO THE SIDEBAR COMPONENT


    //-------DEFAULT CODE--------
    // if(this.name == 'DemoTeller' && this.password == 'TestDemo12'){
    //   this.router.navigateByUrl('landing');
    // }
    // if(this.name == 'Complaince' && this.password == 'TestDemo12'){
    //   // this.router.navigateByUrl('newuser');
    //   this.router.navigateByUrl('menu/newuser');
    // }
    // if(this.name == 'supervisor' && this.password == 'TestDemo12'){
    //   this.router.navigateByUrl('demo');
    // }
    // if(this.name == 'Treasure' && this.password == 'TestDemo12'){
    //   this.router.navigateByUrl('treasuremenu/treasuredashboard');
    // }
    // if(this.name == 'customerservice' && this.password == 'TestDemo12'){
    //   this.router.navigateByUrl('customermenu');
    // }
    // if(this.name == 'marketing' && this.password == 'TestDemo12'){
    //   this.router.navigateByUrl('msmenu');
    // }
    //-------DEFAULT CODE--------


    this.root.value(val)


    this.root.send().subscribe((response)=>{
    //   console.log(response);
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(response, pass.trim()).toString(CryptoJS.enc.Utf8);
    //   console.log(msg);
      const y = JSON.parse(msg)
      const z = y.data[0];
    //   console.log(z[0]);
    const userdata = z[0];
    sessionStorage.setItem("userdata",JSON.stringify(userdata));


      const s = z[0].Agent_ID                           //  FETCHING & SENDING THE AGENT_ID
      const Code = z[0].PartnerCode                           //  FETCHING & SENDING THE PARTNERCODE
      localStorage.setItem("partnercode",Code);
      const userid = z[0].UserId;                           //  FETCHING & SENDING THE PARTNERCODE
      // localStorage.setItem("userid",userid);
      sessionStorage.setItem("userid",userid);

      this.root.setUserId(s);
      localStorage.setItem("AGENT_ID", s);
      const AGENT_NAME = z[0].User_Name;
      sessionStorage.setItem("AGENT_NAME",AGENT_NAME);    //  GETTING THE AGENT_NAME
      const l = z[0].CountryID;                         //  FETCHING & SENDING THE COUNTRY_ID
      this.root.setBest(l);

      if(y.status_msg == "success") {
        this.root.get_countriesforclock().subscribe((response)=>{
          const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
          const msga = CryptoJS.AES.decrypt(response, pass.trim()).toString(CryptoJS.enc.Utf8);
          const send = JSON.parse(msga);
        //   console.log(send.data);
          // this.datashare.setData(send.data);
          this.datashare.setData(msga);
        })
        if(AGENT_NAME == 'DemoTeller'){
        this.router.navigateByUrl('app-email/landing');
        }else{
        this.router.navigateByUrl('dashboard/analytics');
        }
      }
    },(error)=>{
      console.log(error.error.status_msg.message);
      if (error.error.status_msg.message == "Unauthorized User.") {
        this.router.navigateByUrl('/miscellaneous/not-authorized-with-image');
      } else {
        console.log("Try Something Else!!")
      }
    }
    );


  }
}


