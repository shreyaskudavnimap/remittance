import { Component, OnInit } from '@angular/core';
import { TrackerService } from '../../tracker.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  constructor(private tracker:TrackerService) { }

  ngOnInit() {
    this.tracker.send()
  }

}
