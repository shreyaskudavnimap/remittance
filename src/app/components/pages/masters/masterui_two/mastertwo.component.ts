import { Component, OnInit } from '@angular/core';
import { TrackerService } from '../../tracker.service';


@Component({
  selector: 'dcr',
  templateUrl: 'mastertwo.component.html',
  styleUrls: ["mastertwo.component.scss"],

})



export class MasterTwoComponent implements OnInit {

  constructor(private tracker:TrackerService){}

  ngOnInit(): void{
  this.tracker.send()
  }

}


