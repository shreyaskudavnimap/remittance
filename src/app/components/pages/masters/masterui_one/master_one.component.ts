import { Component, OnInit } from '@angular/core';
import { TrackerService } from '../../tracker.service';

@Component({
  selector: 'app-table',
  templateUrl: './master_one.component.html',
  styleUrls: ['./master_one.component.css']
})
export class MasterOneComponent implements OnInit {

  constructor(private tracker:TrackerService) { }

  ngOnInit() {
    this.tracker.send()
  }

}
