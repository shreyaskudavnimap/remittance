import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { RootService } from "src/app/root.service";
import * as CryptoJS from 'crypto-js';


@Component({
    selector: "app-analytics",
    templateUrl: "customertwel.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Customertwel implements OnInit {
  closeResult = '';

  find:[];
  find1:[];
  AGENT_ID:any;


  PAYOUT_COUNTRY:any;
  PAYOUT_AGENT:any;
  PAYOUT_AGENT_BRANCH:any;
  FROM_DATE:any;
  TO_DATE:any;


  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor(private modalService: NgbModal, private root:RootService) {}

    columnDefs = [
        {headerName:'Username',field:"make",sortable:true,filter:true},
        {headerName:'Branch Name',field:"model",sortable:true,filter:true},
        {headerName:'SRTR No',field:"price",sortable:true,filter:true},
        {headerName:'Date',field:"number",sortable:true,filter:true},
        {headerName:'Sender',field:"email",sortable:true,filter:true},
        {headerName:'Receiver',field:"receiver",sortable:true,filter:true},
        {headerName:'Contact No.',field:"country",sortable:true,filter:true},
        {headerName:'Payment Mode',field:"amount",sortable:true,filter:true},
        {headerName:'Payout country',field:"rcountry",sortable:true,filter:true},
        {headerName:'Payout Amount',field:"ramount",sortable:true,filter:true},
        {headerName:'Payout Curr. Code',field:"mode",sortable:true,filter:true},
      ];
      rowData = [
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
      ];
    

    ngOnInit() {
      this.root.COMPLAINCE_C().subscribe((resp)=>{
        // console.log(resp);
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find = a;
        console.log(this.find);
      })

    }

    changeFn(val) {
      const id = this.PAYOUT_COUNTRY;
      this.AGENT_ID = id.CountryID;
      this.root.COMPLAINCE_AGENT(this.AGENT_ID);

      if (this.AGENT_ID == ' '  || this.AGENT_ID == null) {
        console.log("Select a value");
      } else {
        this.root.COMPLAINCE_AG().subscribe((res)=>{
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find1 = a;
        console.log(this.find1);
      })
      
      }
    }

    
    getrows(){
    }

    setValue() {
      const data = {
        'FROM_DATE': this.FROM_DATE,
        'TO_DATE': this.TO_DATE,
        'PAYOUT_COUNTRY': this.PAYOUT_COUNTRY,
        'PAYOUT_AGENT': this.PAYOUT_AGENT,
        'PAYOUT_AGENT_BRANCH': this.PAYOUT_AGENT_BRANCH
      }
     //  console.log(data);
      // this.root.Complaince_user(data);
      // this.root.COMPLAINCE().subscribe((res)=>{
      //   console.log(res);
      // })
     }


    op(filter) {
      this.modalService.open(filter,
     {ariaLabelledBy: 'modal-basic-title',centered:true, windowClass:'custom-class'}).result.then((result) => {
       this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = 
           `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }

}
