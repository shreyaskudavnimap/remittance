import { Component, OnInit} from "@angular/core";
import { TrackerService } from "../../tracker.service";

@Component({
    selector: "app-analytics",
    templateUrl: "customerparent.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Customermenuparent implements OnInit {

    constructor(private tracker:TrackerService) {}
    
    ngOnInit() {
    this.tracker.send()
    }

}
