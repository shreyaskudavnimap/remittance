import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { RootService } from "src/app/root.service";
import * as CryptoJS from 'crypto-js';


@Component({
    selector: "app-analytics",
    templateUrl: "customerfourt.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Customerfourt implements OnInit {
  closeResult = '';
  PAYOUT_COUNTRY
  SEND_AGENT
  RECEIVE_AGENT
  FROM_DATE
  TO_DATE
  To_Amount
  From_Amount
  Benfc
  cust


  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    find: [];
    find1: [];
    find2: [];

    constructor(private modalService: NgbModal,private root:RootService){}

    columnDefs = [
        {headerName:'Username',field:"make",sortable:true,filter:true},
        {headerName:'Branch Name',field:"model",sortable:true,filter:true},
        {headerName:'SRTR No',field:"price",sortable:true,filter:true},
        {headerName:'Date',field:"number",sortable:true,filter:true},
        {headerName:'Sender',field:"email",sortable:true,filter:true},
        {headerName:'Receiver',field:"receiver",sortable:true,filter:true},
        {headerName:'Contact No.',field:"country",sortable:true,filter:true},
        {headerName:'Payment Mode',field:"amount",sortable:true,filter:true},
        {headerName:'Payout country',field:"rcountry",sortable:true,filter:true},
        {headerName:'Payout Amount',field:"ramount",sortable:true,filter:true},
        {headerName:'Payout Curr. Code',field:"mode",sortable:true,filter:true},
      ];
      rowData = [
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
      ];
    

    ngOnInit() {
      this.root._COMPLAINCE_MENUTHREE().subscribe((resp)=>{
        // console.log(resp);
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find = a;
        console.log(this.find);
      })

    }

    changeFn(val){
      if (this.PAYOUT_COUNTRY.Currency_Id == ' ' || this.PAYOUT_COUNTRY.Currency_Id == null) {
        console.log("select something!");
      } else {
        // console.log(this.PAYOUT_COUNTRY.Currency_Id); 
        this.root.COMPLAINCE_MENUTHREE(this.PAYOUT_COUNTRY.Currency_Id);
        this.root.COMPLAINCE_MENUFOUR(this.PAYOUT_COUNTRY.Currency_Id);

        this.root._COMPLAINCE_MENUTHREETWO().subscribe((resp)=>{
          const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
          const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
          // console.log(msg);
          const c = JSON.parse(msg);
          const a = c.data[0];
          this.find1 = a;
          console.log(this.find1);
        })


        this.root._COMPLAINCE_FILTERFOUR().subscribe((resp)=>{
          const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
          const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
          // console.log(msg);
          const c = JSON.parse(msg);
          const a = c.data[0];
          this.find2 = a;
          console.log(this.find2);
        })

      }

    }

    setValue(){
      const data ={
          "LCCCY" : this.PAYOUT_COUNTRY.Currency_Id,    
          "PayInAgent" : '-1',
          "FCCCY" : '',
          "PayOutAgent" : '-1',    
          "CustomerName" : this.cust,    
          "BenfName" : this.Benfc,    
          "FromAmount" : this.From_Amount,    
          "Toamount" : this.To_Amount,    
          "IsAmountinUSD" : "0",    
          "StartDate" : this.FROM_DATE,    
          "ToDate" : this.TO_DATE,     
        }
        // console.log(data);
        this.root.COMPLAINCE_SUBMIT(data);

        this.root._COMPLAINCE_SUBMIT().subscribe((resp)=>{
          const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
          const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
          console.log(msg);
          // const c = JSON.parse(msg);
          // const a = c.data[0];
          // this.find2 = a;
          // console.log(this.find2);
        })
    }
    getrows(){
    }

    op(filter) {
      this.modalService.open(filter,
     {ariaLabelledBy: 'modal-basic-title',centered:true, windowClass:'custom-class'}).result.then((result) => {
       this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = 
           `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }

}
