import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";
import { TrackerService } from "../../tracker.service";


@Component({
    selector: "app-analytics",
    templateUrl: "menuparent.component.html",
    // styleUrls: ["menuparent.component.css"]
})
export class Menuparent implements OnInit {
  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor(private tracker:TrackerService) {}

    columnDefs = [
        {headerName:'Sr No.',field:"make",sortable:true,filter:true,width:70},
        {headerName:'Ch',field:"model",sortable:true,filter:true,width:60,checkboxSelection:true},
        {headerName:'Agent',field:"price",sortable:true,filter:true,width:100},
        {headerName:'Txn No',field:"number",sortable:true,filter:true,width:90},
        {headerName:'Date',field:"email",sortable:true,filter:true,width:100},
        {headerName:'Payin country',field:"receiver",sortable:true,filter:true,width:120},
        {headerName:'Payout Country',field:"country",sortable:true,filter:true,width:130},
        {headerName:'Payin Amount',field:"amount",sortable:true,filter:true,width:120},
        {headerName:'Payout Amount',field:"rcountry",sortable:true,filter:true,width:120},
        {headerName:'Commision',field:"ramount",sortable:true,filter:true,width:120},
        {headerName:'Status',field:"mode",sortable:true,filter:true,width:120},
        {headerName:'Remarks',field:"status",sortable:true,filter:true,width:120},
        {headerName:'Bank Account No.',field:"remark",sortable:true,filter:true,width:120},
        {headerName:'Bank name',field:"remark",sortable:true,filter:true,width:120},
        {headerName:'Branch Name',field:"remark",sortable:true,filter:true,width:120},
        {headerName:'Username',field:"remark",sortable:true,filter:true,width:120},
      ];
      rowData = [
        {make:"1", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"2", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"3", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"4", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"5", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom"
        ,amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"6", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"7", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"8", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"9", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"10", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"11", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"12", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
        {make:"13", model: "", price:'Test Test2', number:"162837458987",email:"05/10/2021",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"20000",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"TA123HCG34"},
      ];

    ngOnInit() {
    this.tracker.send()
    }
    getrows(){
      // const Selectednodes = this.agGrid.api.getSelectedNodes();
      // const Selecteddata = Selectednodes.map(node => node.data);
      // const Selecteddatastring = Selecteddata.map(node => node.make + ' ' + node.model).join(',');
      // alert(`${Selecteddatastring}`);
    }
}
