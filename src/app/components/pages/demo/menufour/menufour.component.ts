import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";


@Component({
    selector: "app-analytics",
    templateUrl: "menufour.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Menufour implements OnInit {
  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor() {}

    columnDefs = [
        {headerName:'Approved/View',field:"make",sortable:true,filter:true,width:70},
        {headerName:'Sending Agent',field:"model",sortable:true,filter:true},
        {headerName:'Transaction no.',field:"price",sortable:true,filter:true},
        {headerName:'date',field:"number",sortable:true,filter:true},
        {headerName:'Remiteer name',field:"email",sortable:true,filter:true},
        {headerName:'Beneficiary Name',field:"receiver",sortable:true,filter:true},
        {headerName:'Payin Amount',field:"country",sortable:true,filter:true},
        {headerName:'Payout amount',field:"amount",sortable:true,filter:true},
        {headerName:'Status',field:"rcountry",sortable:true,filter:true},
        {headerName:'remark',field:"ramount",sortable:true,filter:true},
        {headerName:'transfer purpose',field:"mode",sortable:true,filter:true},
        {headerName:'other Puprose',field:"status",sortable:true,filter:true},
        {headerName:'Log',field:"remark",sortable:true,filter:true},
        {headerName:'In complaince',field:"remark",sortable:true,filter:true},
        {headerName:'OFAC  Log Detail',field:"remark",sortable:true,filter:true},
        {headerName:'...',field:"remark",sortable:true,filter:true},
      ];
      rowData = [
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View0", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View1", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View2", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"View3", model: "TheUnifiedBIRMINGHAM", price:'TPAY1000605725', number:"07/05/2021	",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",
        amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
      ];
    

    ngOnInit() {
    }
    getrows(){
    }

}
