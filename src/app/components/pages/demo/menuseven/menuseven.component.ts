import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";


@Component({
    selector: "app-analytics",
    templateUrl: "menuseven.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Menuseven implements OnInit {
  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor() {}

    columnDefs = [
        {headerName:'Chk',field:"make",sortable:true,filter:true,width:70},
        {headerName:'Txn Number',field:"model",sortable:true,filter:true},
        {headerName:'Txn Date',field:"price",sortable:true,filter:true},
        {headerName:'Remitter Name Beneficiary name',field:"number",sortable:true,filter:true},
        {headerName:'Payout country',field:"email",sortable:true,filter:true},
        {headerName:'Reason',field:"receiver",sortable:true,filter:true},
        {headerName:'Remark',field:"country",sortable:true,filter:true},
      ];
      rowData = [
        {make:"1", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"2", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"3", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"4", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"5", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"6", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"7", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"8", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"9", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"10", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"11", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"12", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
        {make:"13", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom"},
      ];
    

    ngOnInit() {
    }
    getrows(){
    }

}
