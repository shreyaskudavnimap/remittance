import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { RootService } from "src/app/root.service";
import * as CryptoJS from 'crypto-js';
import { GridOptions } from 'ag-grid-community';




@Component({
    selector: "app-analytics",
    templateUrl: "menutwo.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Menutwo implements OnInit {
  PAYOUT_COUNTRY
  BRANCHID
  closeResult = '';
  find:[];
  find1:[];
  find2:[];
  FROM_DATE
  TO_DATE
  AGENT_ID


  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor(private modalService: NgbModal,private root:RootService) {}

    columnDefs = [];
    gridOptions = <GridOptions>{
      columnDefs: this.columnDefs,
    }

    // columnDefs = [
    //     {headerName:'Agent Name',field:"make",sortable:true,filter:true,width:70},
    //     {headerName:'Payout amt LC',field:"model",sortable:true,filter:true},
    //     {headerName:'Commision',field:"price",sortable:true,filter:true},
    //     {headerName:'Total AMt Lc',field:"number",sortable:true,filter:true},
    //     {headerName:'Earnings',field:"email",sortable:true,filter:true},
    //     {headerName:'Earnings USD',field:"receiver",sortable:true,filter:true},
    //     {headerName:'Total Amt USD',field:"country",sortable:true,filter:true},
    //     {headerName:'Transactions',field:"amount",sortable:true,filter:true},
    //   ];
      rowData = [
        {make:"1", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"2", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"3", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"4", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"5", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"6", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"7", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"8", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"9", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"10", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"11", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"12", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
        {make:"13", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)"},
      ];
    

    ngOnInit() {
      this.root._COMPLAINCE_TWO().subscribe((resp)=>{
        // console.log(resp);
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find = a;
        console.log(this.find);
      })

    }

    changeFn(val){

      if (this.PAYOUT_COUNTRY.Agent_ID == ' ' || this.PAYOUT_COUNTRY.Agent_ID == null) {
        console.log("select something!");
      } else {
        this.AGENT_ID = this.PAYOUT_COUNTRY.Agent_ID;
      }

      this.root.COMPLAINCE_MENUTWO(this.PAYOUT_COUNTRY.Agent_ID);
      this.root._COMPLAINCE_MENUTWO().subscribe((resp)=>{
        // console.log(resp);
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find1 = a;
        console.log(this.find1);
      })

    }

    setValue(){
      const data = {
        "FROM_DATE" : this.FROM_DATE,
        "TO_DATE" : this.TO_DATE,
        "AGENT_ID" : this.AGENT_ID,
        "BRANCH_ID" : '',
      }
      this.root.SEND_COMPLAINCE(data);
      // console.log(data);
      this.root._COMPLAINCE_MENUTWOSUBMIT().subscribe((resp)=>{
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
        console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[1];
        this.find2 = a;
        // console.log(this.find2);
        
        const ca =JSON.stringify(this.find2);
        const ac = JSON.parse(ca);
        const aca = ac[0]
        for(var key in aca){
          this.columnDefs.push({headerName: key, field: key});
          this.gridOptions.api.setColumnDefs(this.columnDefs)
        }
      })
    }

    getrows(){
    }

    op(filter) {
      this.modalService.open(filter,
     {ariaLabelledBy: 'modal-basic-title',centered:true, windowClass:'custom-class'}).result.then((result) => {
       this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = 
           `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }

}
