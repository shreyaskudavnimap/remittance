import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";


@Component({
    selector: "app-analytics",
    templateUrl: "menusix.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Menusix implements OnInit {
  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor() {}

    columnDefs = [
        {headerName:'Remitter Name',field:"make",sortable:true,filter:true,width:70},
        {headerName:'Beneficiary Name',field:"model",sortable:true,filter:true},
        {headerName:'Agent Name',field:"price",sortable:true,filter:true},
        {headerName:'Transaction Number',field:"number",sortable:true,filter:true},
        {headerName:'Payin Date',field:"email",sortable:true,filter:true},
        {headerName:'payin Currency',field:"receiver",sortable:true,filter:true},
        {headerName:'Payin Amount',field:"country",sortable:true,filter:true},
        {headerName:'debit Amount',field:"amount",sortable:true,filter:true},
        {headerName:'debit USD',field:"rcountry",sortable:true,filter:true},
        {headerName:'Sett rate',field:"ramount",sortable:true,filter:true},
        {headerName:'Payout currency',field:"mode",sortable:true,filter:true},
        {headerName:'payout Amount',field:"status",sortable:true,filter:true},
        {headerName:'Status',field:"remark",sortable:true,filter:true},
      ];
      rowData = [
        {make:"1", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"2", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"3", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"4", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"5", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"6", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"7", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"8", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"9", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"10", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"11", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"12", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"13", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
      ];
    

    ngOnInit() {
    }
    getrows(){
    }

}
