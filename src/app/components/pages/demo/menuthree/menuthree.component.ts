import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { RootService } from "src/app/root.service";
import * as CryptoJS from 'crypto-js';



@Component({
    selector: "app-analytics",
    templateUrl: "menuthree.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Menuthree implements OnInit {
  closeResult = '';
  FC_CURRENCY
  PAYOUT_COUNTRY
  SEND_AGENT
  RECEIVE_AGENT
  FROM_DATE
  TO_DATE
  To_Amount
  From_Amount
  Benfc
  cust


  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    find: [];
    find1: [];
    find2: [];
    find3: [];



    constructor(private modalService: NgbModal,private root:RootService) {}

    columnDefs = [
        {headerName:'Agent Name',field:"make",sortable:true,filter:true,width:70},
        {headerName:'Transaction Number',field:"model",sortable:true,filter:true},
        {headerName:'payIn Date',field:"price",sortable:true,filter:true},
        {headerName:'InCurrency',field:"number",sortable:true,filter:true},
        {headerName:'OutCurrency',field:"email",sortable:true,filter:true},
        {headerName:'Payin Amount',field:"receiver",sortable:true,filter:true},
        {headerName:'Exch Gain',field:"country",sortable:true,filter:true},
        {headerName:'Debit  Amount',field:"amount",sortable:true,filter:true},
        {headerName:'Debit USD',field:"rcountry",sortable:true,filter:true},
        {headerName:'comm',field:"ramount",sortable:true,filter:true},
        {headerName:'VAT Amount',field:"mode",sortable:true,filter:true},
        {headerName:'Sett. Rate',field:"status",sortable:true,filter:true},
        {headerName:'Speed card/Customer ID',field:"remark",sortable:true,filter:true},
        {headerName:'Remitter Full Name',field:"remark",sortable:true,filter:true},
        {headerName:'Beneficiary Full Name',field:"remark",sortable:true,filter:true},
        {headerName:'Bank',field:"remark",sortable:true,filter:true},
        {headerName:'Status',field:"remark",sortable:true,filter:true},
      ];
      rowData = [
        {make:"1", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"2", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"3", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"4", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"5", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"6", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"7", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"8", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"9", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"10", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"11", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"12", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"13", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
      ];
    

    ngOnInit() {
      this.root._COMPLAINCE_MENUTHREE().subscribe((resp)=>{
        // console.log(resp);
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find = a;
        console.log(this.find);
      })
      

      this.root._COMPLAINCE_FILTERTHREE().subscribe((resp)=>{
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find3 = a;
        console.log(this.find3);
      })

    }

    changeFn(val){
      if (this.PAYOUT_COUNTRY.Currency_Id == ' ' || this.PAYOUT_COUNTRY.Currency_Id == null) {
        console.log("select something!");
      } else {
        this.root.COMPLAINCE_MENUTHREE(this.PAYOUT_COUNTRY.Currency_Id);

        this.root._COMPLAINCE_MENUTHREETWO().subscribe((resp)=>{
          const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
          const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
          // console.log(msg);
          const c = JSON.parse(msg);
          const a = c.data[0];
          this.find1 = a;
          console.log(this.find1);
        })
      }

      if (this.FC_CURRENCY.Currency_Id == ' ' || this.FC_CURRENCY.Currency_Id == null) {
        console.log("select something!");
      } else {
        this.root.COMPLAINCE_MENUFOUR(this.FC_CURRENCY.Currency_Id);
        console.log(this.FC_CURRENCY.Currency_Id);

        this.root._COMPLAINCE_FILTERFOUR().subscribe((resp)=>{
          const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
          const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
          // console.log(msg);
          const c = JSON.parse(msg);
          const a = c.data[0];
          this.find2 = a;
          console.log(this.find2);
        })

      }

    }

    setValue(){
      const data ={
          "LCCCY" : this.PAYOUT_COUNTRY.Currency_Id,    
          "PayInAgent" : '-1',
          "FCCCY" : '',
          "PayOutAgent" : '-1',    
          "CustomerName" : this.cust,    
          "BenfName" : this.Benfc,    
          "FromAmount" : this.From_Amount,    
          "Toamount" : this.To_Amount,    
          "IsAmountinUSD" : "0",    
          "StartDate" : this.FROM_DATE,    
          "ToDate" : this.TO_DATE,     
        }
        // console.log(data);
        this.root.COMPLAINCE_SUBMIT(data);

        this.root._COMPLAINCE_SUBMIT().subscribe((resp)=>{
          const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
          const msg = CryptoJS.AES.decrypt(resp, pass.trim()).toString(CryptoJS.enc.Utf8);
          console.log(msg);
          // const c = JSON.parse(msg);
          // const a = c.data[0];
          // this.find2 = a;
          // console.log(this.find2);
        })
    }

    getrows(){
    }

    op(filter) {
      this.modalService.open(filter,
     {ariaLabelledBy: 'modal-basic-title',centered:true, windowClass:'custom-class'}).result.then((result) => {
       this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = 
           `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }

}
