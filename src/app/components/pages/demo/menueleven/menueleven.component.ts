import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";


@Component({
    selector: "app-analytics",
    templateUrl: "menueleven.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Menueleven implements OnInit {
  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor() {}

    columnDefs = [
        {headerName:'Date',field:"price",sortable:true,filter:true},
        {headerName:'Txn No',field:"model",sortable:true,filter:true},
        {headerName:'Agent',field:"make",sortable:true,filter:true,width:70},
        {headerName:'Country of origin',field:"country",sortable:true,filter:true},
        {headerName:'Sender',field:"email",sortable:true,filter:true},
        {headerName:'Nationality',field:"rcountry",sortable:true,filter:true},
        {headerName:'Source of Income',field:"number",sortable:true,filter:true},
        {headerName:'beneficiary',field:"receiver",sortable:true,filter:true},
        {headerName:'Nationality',field:"rcountry",sortable:true,filter:true},
        {headerName:'purpose',field:"amount",sortable:true,filter:true},
        {headerName:'Amount in sending currency',field:"ramount",sortable:true,filter:true},
        {headerName:'Amount in receiving currency',field:"mode",sortable:true,filter:true},
        {headerName:'Remark',field:"remark",sortable:true,filter:true},
      ];
      rowData = [
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM0", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM1", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM2", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
        {make:"TheunifiedBIRHIM3", model: "TPAY1002951090", price:'05/10/2021', number:"salary from emplyment",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",remark:"-"},
      ];
    

    ngOnInit() {
    }
    getrows(){
    }

}
