import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";


@Component({
    selector: "app-analytics",
    templateUrl: "menueight.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Menueight implements OnInit {
  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor() {}

    columnDefs = [
        {headerName:'Sr No.',field:"make",sortable:true,filter:true,width:70},
        {headerName:'Transaction Number',field:"model",sortable:true,filter:true},
        {headerName:'Date',field:"price",sortable:true,filter:true},
        {headerName:'Remitter Number',field:"number",sortable:true,filter:true},
        {headerName:'Sender',field:"email",sortable:true,filter:true},
        {headerName:'Receiver',field:"receiver",sortable:true,filter:true},
        {headerName:'Sending Country',field:"country",sortable:true,filter:true},
        {headerName:'Sending Amount',field:"amount",sortable:true,filter:true},
        {headerName:'Receiving country',field:"rcountry",sortable:true,filter:true},
        {headerName:'Receiving Amount',field:"ramount",sortable:true,filter:true},
        {headerName:'mode',field:"mode",sortable:true,filter:true},
        {headerName:'Status',field:"status",sortable:true,filter:true},
        {headerName:'Remark',field:"remark",sortable:true,filter:true},
      ];
      rowData = [
        {make:"1", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"2", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"3", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"4", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"5", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"6", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"7", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"8", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"9", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"10", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"11", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"12", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
        {make:"13", model: "TPAY1002951090", price:'05/10/2021', number:"",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"10.00(GBP)",rcountry:"Yemen",ramount:"9.76(USD)",mode:"cash over counter",status:"complaince bloack",remark:"-"},
      ];
    

    ngOnInit() {
    }
    getrows(){
    }

}
