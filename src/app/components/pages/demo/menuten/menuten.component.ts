import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";


@Component({
    selector: "app-analytics",
    templateUrl: "menuten.component.html",
    // styleUrls: ["demo.component.css"]
})
export class Menuten implements OnInit {
  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    constructor() {}

    columnDefs = [
        {headerName:'Date',field:"price",sortable:true,filter:true},
        {headerName:'Txn No.',field:"model",sortable:true,filter:true},
        {headerName:'Agent',field:"make",sortable:true,filter:true},
        {headerName:'Country Of Origin',field:"country",sortable:true,filter:true},
        {headerName:'Sender',field:"email",sortable:true,filter:true},
        {headerName:'Nationality',field:"rcountry",sortable:true,filter:true},
        {headerName:'Source of Income',field:"number",sortable:true,filter:true},
        {headerName:'Beneficiary',field:"receiver",sortable:true,filter:true},
        {headerName:'Nationality',field:"rcountry",sortable:true,filter:true},
        {headerName:'Purpose',field:"amount",sortable:true,filter:true},
        {headerName:' Amount sending',field:"ramount",sortable:true,filter:true},
        {headerName:'Amount receiving',field:"mode",sortable:true,filter:true},
        {headerName:'Remark',field:"remark",sortable:true,filter:true},
      ];
      rowData = [
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY0", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY1", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY2", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
        {make:"PAYMENT GATEWAY3", model: "TPAY1002951090", price:'05/10/2021', number:"others",email:"Test Test2",receiver:"Test Test2Khan",country:"United Kingdom",amount:"",rcountry:"Yemen",ramount:"9.76(USD)",mode:"10.00(GBP)",remark:"-"},
      ];
    

    ngOnInit() {
    }
    getrows(){
    }

}
