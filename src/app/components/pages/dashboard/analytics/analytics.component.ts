import { Component, OnInit } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { TrackerService } from "../../tracker.service";


@Component({
    selector: "app-analytics",
    templateUrl: "./analytics.component.html",
    styleUrls: ["./analytics.component.scss"]
})
export class AnalyticsComponent implements OnInit {
    faInfo =faInfo;
    constructor(private tracker:TrackerService) {}

    ngOnInit() {
        this.tracker.send()
    }
}
