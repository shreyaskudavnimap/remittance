import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import {faFilter} from '@fortawesome/free-solid-svg-icons';
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import {faGlasses} from '@fortawesome/free-solid-svg-icons';
import { RootService } from 'src/app/root.service';
import { GridApi, GridOptions } from 'ag-grid-community';
import { TrackerService } from '../../tracker.service';


@Component({
  selector: 'app-ecommerce',
  templateUrl: './ecommerce.component.html',
  styleUrls: ['./ecommerce.component.scss']
})
export class EcommerceComponent implements OnInit {
  private gridApi!: GridApi;

  faSyncAlt =faSyncAlt;
  faFilter =faFilter;
  faInfo =faInfo;
  faGlasses =faGlasses;


  closeResult = '';
  searchText
  data


  genreValueGetter(params) {
    const arr = params.data.message;
    let a = arr.split(" ")
    return JSON.parse(a).userid;
  }

  requestid(params) {
    const arr = params.data.message;
    let a = arr.split(" ")
    return JSON.parse(a).Route;
  }

  ipaddress(params) {
    const arr = params.data.message;
    let a = arr.split(" ")
    return JSON.parse(a).ipaddress;
  }

  request(params) {
    const arr = params.data.message;
    let a = arr.split(" ")
    return JSON.parse(a).loggedfrom;
  }

  useragent(params) {
    const arr = params.data.message;
    let a = arr.split(" ")
    return JSON.parse(a).useragent;
  }

  columnDefs = [
    {headerName:'Id',field:"_id",sortable:true,filter:true,width:200},
    {headerName:'Level',field:"level",sortable:true,filter:true,width:100},
    {headerName:'Timestamp',field:"timestamp",sortable:true,filter:true,width:200},
    {headerName:'UserId',valueGetter: this.genreValueGetter,sortable:true,filter:true,width:200},
    {headerName:'Menu',valueGetter: this.requestid,sortable:true,filter:true,width:200},
    {headerName:'Ipaddress',valueGetter: this.ipaddress,sortable:true,filter:true,width:200},
    {headerName:'Username',valueGetter: this.request,sortable:true,filter:true,width:200},
    {headerName:'UserAgent',valueGetter: this.useragent,sortable:true,filter:true,width:200},
  ];

  rowData:any={};
  gridOptions = <GridOptions>{
    rowData: this.rowData,
  }



  constructor(private root:RootService, private tracker:TrackerService) {}
  ngOnInit(){   
    this.tracker.send()
    
  this.root._LOG().subscribe((res:any)=>{
    console.log(res);
    this.data = res.data
    this.gridOptions.api.setRowData(this.data);
  })
}



onGridReady(params){
  this.gridApi  = params.api;
  console.log(this.gridApi)
}
onFilterTextBoxChanged() {
  this.gridOptions.api.setQuickFilter(
    (document.getElementById('filter-text-box') as HTMLInputElement).value
  );
}

  open() {
    this.root._LOG().subscribe((res:any)=>{
      console.log(res);
      this.data = res.data
      // this.gridOptions.api.setRowData(this.data);
      this.gridOptions.api.showLoadingOverlay()
      setTimeout(() => {
        this.gridOptions.api.setRowData(this.data);
     }, 1000);
    })
    this.gridOptions.api.refreshCells();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
