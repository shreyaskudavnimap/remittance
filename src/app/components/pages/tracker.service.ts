import { Injectable, OnInit } from '@angular/core';
import {Router } from '@angular/router';
import { HttpClient, HttpHeaders  } from '@angular/common/http'; 
import * as Bowser from "bowser";
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})

export class TrackerService implements OnInit{


    httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          "Access-Control-Allow-Origin": "*",
          'log':'yes'
          
        } ),responseType: 'text' as 'json'
      };

    currentPage = '';
    userIP = ''
    Browser
    browser = Bowser.getParser(window.navigator.userAgent);
    userid = sessionStorage.getItem('userid');
    AGENT_NAME = sessionStorage.getItem('AGENT_NAME')


      async  getGithubData() {
        let res = await axios.get('https://api.ipify.org/?format=json');
        this.userIP = res.data.ip; 
    }

    constructor(private router:Router, private http:HttpClient){
        this.getGithubData();
    }
        
    public send(){
        const url = window.location.href
        this.currentPage = url;
        setTimeout(() => {
                this.Browser = this.browser.getBrowserName()
                const data ={"type": "I",
                "message": {
                        "userid":this.userid,
                        "loggedfrom":this.AGENT_NAME,
                        "useragent":this.Browser,
                        "ipaddress":this.userIP,
                        "Route":this.currentPage,
                    }
                }
                console.log(data);
                if(this.currentPage == url){
                    this.http.post('http://tutapi.apicalin.com/api/logdata',data,this.httpOptions).subscribe((res)=>{
                        console.log(res);
                })
                }
        }, 2000);

    }
        
    ngOnInit(): void {}



}
