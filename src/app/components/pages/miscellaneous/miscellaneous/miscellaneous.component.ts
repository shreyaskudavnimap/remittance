import { Component, OnInit } from '@angular/core';
import { TrackerService } from '../../tracker.service';

@Component({
  selector: 'app-miscellaneous',
  templateUrl: './miscellaneous.component.html',
  styleUrls: ['./miscellaneous.component.scss']
})
export class MiscellaneousComponent implements OnInit {

  constructor(private tracker:TrackerService) { }

  ngOnInit() {
    this.tracker.send()
  }

}
