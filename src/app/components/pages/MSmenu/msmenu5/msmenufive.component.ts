import { Component, OnInit, ViewChild } from "@angular/core";
declare let $: any;
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { AgGridAngular } from "ag-grid-angular";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { RootService } from "src/app/root.service";
import * as CryptoJS from 'crypto-js';

@Component({
    selector: "app-analytics",
    templateUrl: "msmenufive.component.html",
    // styleUrls: ["demo.component.css"]
})
export class MSfive implements OnInit {
  closeResult = '';

  find:[];
  find1:[];
  AGENT_ID:any;

  @ViewChild('agGrid',{static:false}) agGrid: AgGridAngular
    faInfo =faInfo;
    PAYOUT_COUNTRY:any;
    PAYOUT_AGENT:any;
    PAYOUT_AGENT_BRANCH:any;
    FROM_DATE:any;
    TO_DATE:any;


    constructor(private modalService: NgbModal, private root:RootService) {}

    columnDefs = [
        {headerName:'Username',field:"make",sortable:true,filter:true},
        {headerName:'Branch Name',field:"model",sortable:true,filter:true},
        {headerName:'SRTR No',field:"price",sortable:true,filter:true},
        {headerName:'Date',field:"number",sortable:true,filter:true},
        {headerName:'Sender',field:"email",sortable:true,filter:true},
        {headerName:'Receiver',field:"receiver",sortable:true,filter:true},
        {headerName:'Contact No.',field:"country",sortable:true,filter:true},
        {headerName:'Payment Mode',field:"amount",sortable:true,filter:true},
        {headerName:'Payout country',field:"rcountry",sortable:true,filter:true},
        {headerName:'Payout Amount',field:"ramount",sortable:true,filter:true},
        {headerName:'Payout Curr. Code',field:"mode",sortable:true,filter:true},
      ];
      rowData = [
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
        {make:"TOTIPAY", model: "PAYMENT GATEWAY", price:'TPAY1002951090', number:"05/10/2021",email:"Test Test2",receiver:"Test Test2Khan",country:"98126354",amount:"LOAD WALLET",rcountry:"Yemen",ramount:"9.76(USD)",mode:"GBP"},
      ];
    

    ngOnInit() {
    }
    getrows(){
    }

    changeFn(val){}
    setValue(){}
    op(filter) {
      this.modalService.open(filter,
     {ariaLabelledBy: 'modal-basic-title',centered:true, windowClass:'custom-class'}).result.then((result) => {
       this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = 
           `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }

}
