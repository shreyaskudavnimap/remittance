import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {faMale} from '@fortawesome/free-solid-svg-icons';
import {faFemale} from '@fortawesome/free-solid-svg-icons';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import { RootService } from 'src/app/root.service';
import * as CryptoJS from 'crypto-js';
import { TrackerService } from '../tracker.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'dcr',
  templateUrl: './customer.component.html',
  styleUrls: ["./customer.component.css"],
  encapsulation: ViewEncapsulation.None
})



export class DCR implements OnInit {

  headers = new HttpHeaders({
    'flask': 'yes'});
  options = { headers: this.headers };

  faMale =faMale;
  faFemale =faFemale;
  faPlus =faPlus;

  gender = 'M';

  generate_customer
  currentdate

  SOURCE
  selection
  sel
  ID
  first_name
  middle_name
  last_name
  dob
  locality
  mobile
  email
  id_des
  id_no
  issue_date
  expiry_date
  source_details
  monthly_income
  kyc_id
  kyc_details

  partnercode
  EnteredBy

  find:[];
  find1:[];
  find2:[];
  find3:[];

  constructor(private root : RootService, private tracker:TrackerService, private http:HttpClient){}

  ngOnInit() {
    this.tracker.send()

    this.partnercode = localStorage.getItem('partnercode');
    // this.EnteredBy = localStorage.getItem('userid');
    this.EnteredBy = sessionStorage.getItem('userid')

    this.root.RANDOM_CUST(this.partnercode);

    this.root.CUSTOMER_NATIONALITY().subscribe((res)=>{                       // THIS IS FETCHING THE NATIONALITY
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const a = c.data[0];
      this.find = a;
      // console.log(this.find);
    })

    this.root.CUSTOMER_COUNTRIES().subscribe((res)=>{                       // THIS IS FETCHING THE  COUNTRIES
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const a = c.data[0];
      this.find1 = a;
      // console.log(this.find1);
    })

    this.root._CUSTOMER_SOI().subscribe(res=>{                // THIS IS FETCHING THE SOURCE OF INCOME
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      this.find3 = c.data[0];
      // console.log(this.find3);
    })

    this.root._RANDOM_CUSTOMER().subscribe((res)=>{                         // THIS IS FOR GENERATING THE RANDOM CUSTOMER NUMBER
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      console.log(c)
      const number = c.data[0];
      this.generate_customer = number[0].Number;
      console.log(this.generate_customer);
    })
  }

  Areyousure(){
    const checkBox = document.getElementById('toggle-checkbox') as HTMLInputElement;
    if (checkBox.checked !== true){
      this.gender = 'M';
    } else {
      this.gender = 'F';
    }
  }

  onChange(val){
    console.log(val)   //GETTING THE NATIONALITY FROM THE DROPDOWN
  }
  IDs(val){
    console.log(val)   //GETTING THE NATIONALITY FROM THE DROPDOWN
  }
  Source(val){
    console.log(val)   //GETTING THE NATIONALITY FROM THE DROPDOWN
  }
  Change(val){
    console.log(val)     // GETTING THE COUNTRY FROM THE DROPDOWN

    this.root.CUSTOMER_ID(this.sel.Code);
    if (this.sel.Code == ' '  || this.sel.Code == null) {
      console.log("Select a value");
    } else {
        this.root.CUSTOMER_IDTYPE().subscribe((res)=>{                       // THIS IS FETCHING THE IDTYPE
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
        const c = JSON.parse(msg);
        const a = c.data[0];
        this.find2 = a;
        // console.log(this.find2);
      })
    }
  }

  submit(){
    var date = new Date();
    this.currentdate = (date.getMonth()+1) +'/'+date.getDate()+'/'+ date.getFullYear()

    const Data = {
      "First_name":this.first_name,
      "Middle_name":this.middle_name,
      "Last_name":this.last_name,
      "DateofBirth":this.dob,
      "Nationality":this.selection.NationalityID,
      "Country":this.sel.ID,
      "Address":this.locality,
      "Mobile_Number":this.mobile,
      "Email":this.email,
      "ID_Type":this.ID.IDType_ID,
      "ID_Desc":this.id_des,
      "ID_No":this.id_no,
      "ID_Issuedate":this.issue_date,
      "ID_Expirydate":this.expiry_date,
      "SourceofFund":this.SOURCE.SourceOfIncome_ID,
      "Source_Details":this.source_details,
      "Monthly_Income":this.monthly_income,
      "KYC_ID":this.kyc_id,
      "KYC_Details":this.kyc_details,
      "Gender":this.gender,
      "Generate_Random":this.generate_customer,
      "partnerCode":this.partnercode,
      "EnteredBy": this.EnteredBy,
      "Regdate":this.currentdate
    }
    console.log(this.EnteredBy)
    console.log("customer Registration Form Data", Data);

    this.root.CUSTOMER_REGISTER(Data);
    this.root._CUSTOMER_REGISTRATION().subscribe((res)=>{                       // THIS IS SENDING CUSTOMER REGISTRATION DATA
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      console.log(msg);
      const number = msg.data[0];               // setting the customer number from the response
      const customer = number[0].Number;
      console.log(customer);
      sessionStorage.setItem('custno',customer)

      // const c = JSON.parse(msg);
      // const a = c.data[0];
      // this.find2 = a;
      // console.log(this.find2);
    },(error)=>console.log(error)
    );

    const flask_data = {
      "First_name":this.first_name,
      "Address":this.locality,
      "Gender":this.gender,
    }
    this.http.post<any>('http://127.0.0.1:5000/', flask_data, this.options).subscribe((res) => {
      console.log(res);
  })
  }

  getValues(val){
    // this.root.setTest(val);
    // console.log(val);

    // this.root.postAPIData().subscribe((response)=>{
    //   console.log('response from post data is ', response);
    // },(error)=>{
    //   console.log('error during post is ', error)
    //   alert('ERROR: make sure to add atleast 3 digit name and valid email!')
    // })
  }

}
