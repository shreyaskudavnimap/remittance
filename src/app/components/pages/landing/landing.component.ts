import { Component, OnInit } from '@angular/core';
import { RootService } from 'src/app/root.service';
import * as CryptoJS from 'crypto-js';
import { GridOptions } from 'ag-grid-community';
import { ShareddataService } from 'src/app/shareddata.service';
import { TrackerService } from '../tracker.service';

@Component({
  selector: 'dcr',
  templateUrl: 'landing.component.html',
})



export class LANDING implements OnInit {

  find:[];


  constructor(private root:RootService, private share:ShareddataService,private tracker:TrackerService){}

  columnDefs = [
    {headerName:'Customer No',field:"CustomerNo",
    cellRenderer: function(params){
      return "<a href='/app-email/txn"
        // + params.value
        + "'>"+params.value+"</a>";
    }},
    {headerName:'FirstName',field:"FirstName",},
    {headerName:'MiddleName',field:"MiddleName",},
    {headerName:'LastName',field:"LastName",},
    {headerName:'DateOfBirth',field:"DateOfBirth",},
    {headerName:'Gender',field:"Gender",},
    {headerName:'Telephone',field:"Telephone",},
    {headerName:'EmailID',field:"EmailID",},
    {headerName:'Address',field:"Address",},
    {headerName:'IsKYC_Approved',field:"IsKYC_Approved"},
    // {headerName:'IsKYC_Approved',field:"IsKYC_Approved",hide: true,suppressToolPanel: true},
    {headerName:'IsLocked',field:"IsLocked",},
  ];

  defaultColDef = {
    filter: true,
    sortable:true,
  };
  // columnDefs: [];
  rowData = [];
  gridOptions = <GridOptions>{
    columnDefs: this.columnDefs,
    rowData: this.rowData,
  }



  onRowClicked(event: any) {
    const data = event.data;
    localStorage.setItem('CustmoreNo', JSON.stringify(data));
  }

  ngOnInit() {
    this.tracker.send()

    this.root._CUSTOMER_LIST().subscribe((res)=>{                         // THIS IS FETCHING THE REGISTERED CUSTOMER LIST
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(res, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      const a = c.data[0];
      this.find = a;
      console.log('Customer List', this.find);

      //---------------- THIS IS FOR SETTING THE DYNAMIC COLUMN & ROWS
        const col = JSON.stringify(this.find);
        const column = JSON.parse(col);
        this.gridOptions.api.setRowData(column);
    })
  }

}


