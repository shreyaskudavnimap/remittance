import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {faFilter} from '@fortawesome/free-solid-svg-icons';
import {faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as data from '../../../../../assets/config/config.json'
import {Data} from '../report/data'
import { RootService } from 'src/app/root.service';
import * as CryptoJS from 'crypto-js';
import { GridOptions } from 'ag-grid-community';




@Component({
  selector: 'app-report',
  templateUrl: './reportone.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./reportone.component.scss']
})
export class ReportOneComponent implements OnInit {
  FromDate
  ToDate
  Agentlist
  BranchList

  find:Data[];
  find1:Data[];

  formVar: FormGroup;

  faFilter =faFilter;
  faSyncAlt =faSyncAlt;
  closeResult = '';

  // public gridOptions: GridOptions;

   msg
   public selection
   public select


constructor(private modalService: NgbModal, private root:RootService,
  private fb: FormBuilder,){}


  rowData = [];
  columnDefs = [];

  gridOptions = <GridOptions>{
    columnDefs: this.columnDefs,
  }


// columnDefs = [
//   {headerName:'CUSTOMER DETAILS',field:"make",sortable:true,filter:true},
//   {headerName:'NAME OF CUSTOMER	',field:"model",sortable:true,filter:true},
//   {headerName:'ID',field:"price",sortable:true,filter:true},
//   {headerName:'MOBILE NUMBER',field:"number",sortable:true,filter:true},
//   {headerName:'EMAIL',field:"email",sortable:true,filter:true},
// ];
// rowData = [
//   {make:"Toyota", model: "Celica", price:35000, number:"9878778",email:"example@gmail.com"},
//   {make:"Ford", model: "Best", price:35000, number:"9878778",email:"example@gmail.com"},
//   {make:"Porsche", model: "Great", price:35000, number:"9878778",email:"example@gmail.com"},
// ];



  ngOnInit(): void {
    this.formVar = this.fb.group({
      selectedCurrency:["", [Validators.required, Validators.min(1)]],
      selecteNationality:["", [Validators.required, Validators.min(1)]],
      time:'',
      timeto:''
    });


    this.root.filter_two().subscribe(data => {
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(data, pass.trim()).toString(CryptoJS.enc.Utf8);
      const c = JSON.parse(msg);
      this.find = c.data[0];
      console.log(this.find);
      console.log(msg) 
  })



  
      this.root.filter_one().subscribe(data => {
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        this.msg = CryptoJS.AES.decrypt(data, pass.trim()).toString(CryptoJS.enc.Utf8);
        const c = JSON.parse(this.msg);
        this.find1 = c.data[0];
        console.log(this.msg); 
    })



}

  onSubmit() {
    console.log(this.formVar.value);
    this.root.filt(this.formVar.value)
    this.root.filter_final().subscribe(data => {
      const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
      const msg = CryptoJS.AES.decrypt(data, pass.trim()).toString(CryptoJS.enc.Utf8);
      // console.log(msg)
      const z = JSON.parse(msg)
      const y = z.data
      const x = y[1]
      const w = x[0]
      // console.log(w)

      for(var key in w){
        this.columnDefs.push({headerName: key, field: key});
        this.gridOptions.api.setColumnDefs(this.columnDefs)
      }
  })
  }

  op(filter) {
    this.modalService.open(filter,
   {ariaLabelledBy: 'modal-basic-title',centered:true, windowClass:'custom-class'}).result.then((result) => {
     this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = 
         `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
