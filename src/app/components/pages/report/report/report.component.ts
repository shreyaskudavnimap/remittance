import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {faFilter} from '@fortawesome/free-solid-svg-icons';
import {faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as data from '../../../../../assets/config/config.json'
import {Data} from './data'
import { RootService } from 'src/app/root.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  find:Data[];
  find1:Data[];

  formVar: FormGroup;

  faFilter =faFilter;
  faSyncAlt =faSyncAlt;
  closeResult = '';

  public selection
  public select





constructor(private modalService: NgbModal, private root:RootService,
  private fb: FormBuilder,
   private http: HttpClient){}

columnDefs = [
  {headerName:'CUSTOMER DETAILS',field:"make",sortable:true,filter:true},
  {headerName:'NAME OF CUSTOMER	',field:"model",sortable:true,filter:true},
  {headerName:'ID',field:"price",sortable:true,filter:true},
  {headerName:'MOBILE NUMBER',field:"number",sortable:true,filter:true},
  {headerName:'EMAIL',field:"email",sortable:true,filter:true},
];
rowData = [
  {make:"Toyota", model: "Celica", price:35000, number:"9878778",email:"example@gmail.com"},
  {make:"Ford", model: "Best", price:35000, number:"9878778",email:"example@gmail.com"},
  {make:"Porsche", model: "Great", price:35000, number:"9878778",email:"example@gmail.com"},
];

  ngOnInit(): void {
    this.formVar = this.fb.group({
      selectedCurrency:["", [Validators.required, Validators.min(1)]],
      selecteNationality:["", [Validators.required, Validators.min(1)]],
      time:'',
      timeto:''
    });

    

    //DATA ENCRYPTION & DECRYPTION PART 
  this.root.reportfilterone().subscribe(data => {
        const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
        const msg = CryptoJS.AES.decrypt(data, pass.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(msg);
        const c = JSON.parse(msg);
        this.find = c.data[0];
        // console.log(this.find);
    })


    this.root.reportfilter().subscribe(data => {
          const pass = '9EB0EF699C3A41B9BA2E66CC75453670'
          const msg = CryptoJS.AES.decrypt(data, pass.trim()).toString(CryptoJS.enc.Utf8);
          const c = JSON.parse(msg);
          this.find1 = c.data[0];
          // console.log(this.find1); 
      })

}

  onSubmit() {
    console.log(this.formVar.value);
  }

  

  op(filter) {
    this.modalService.open(filter,
   {ariaLabelledBy: 'modal-basic-title',centered:true, windowClass:'custom-class'}).result.then((result) => {
     this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = 
         `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
