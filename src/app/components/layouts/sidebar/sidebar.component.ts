import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, Event, NavigationEnd } from "@angular/router";
declare let $: any;
import { HttpClient} from '@angular/common/http';
import { RootService } from "src/app/root.service";
import * as CryptoJS from 'crypto-js';
import { dataSource } from "src/assets/config/menu";
import { ShareddataService } from 'src/app/shareddata.service';
import { Timestamp } from "rxjs/internal/operators/timestamp";



@Component({
    selector: "app-sidebar",
    templateUrl: "./sidebar.component.html",
    styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {

    // public field:Object ={ dataSource: dataSource, id: 'UR_FUNCTIONID', text: 'FunctionName', child: 'submenus' };

    userType: string = '';




        constructor(private router: Router, private http: HttpClient,private root:RootService,private datashare:ShareddataService) {
            router.events.subscribe((event: Event) => {
                if (event instanceof NavigationEnd) {
                    $(".responsive-burger-menu").removeClass("active");
                    $(".sidemenu-area").removeClass("active-sidemenu-area");
                }
            });
        }



    find:any;

    ngOnInit() {
    this.userType = sessionStorage.getItem("AGENT_NAME");        //GETTING THE USERNAME
    
        // Burger Menu JS
        $(".burger-menu").on("click", function() {
            $(this).toggleClass("active");
            $(".main-content").toggleClass("hide-sidemenu-area");
            $(".sidemenu-area").toggleClass("toggle-sidemenu-area");
            $(".top-navbar").toggleClass("toggle-navbar-area");
        });
        $(".responsive-burger-menu").on("click", function() {
            $(".responsive-burger-menu").toggleClass("active");
            $(".sidemenu-area").toggleClass("active-sidemenu-area");
        });
    }
}
