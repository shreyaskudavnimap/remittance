import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ShareddataService } from 'src/app/shareddata.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit,AfterViewInit {

  @ViewChild("clock",{static:false, read: ElementRef})clock: ElementRef;
  @ViewChild("clocks",{static:false, read: ElementRef})clocks: ElementRef;
  @ViewChild("clockss",{static:false, read: ElementRef})clockss: ElementRef;
  @ViewChild("clocks4",{static:false, read: ElementRef})clocks4: ElementRef;
  @ViewChild("clocks5",{static:false, read: ElementRef})clocks5: ElementRef;
  @ViewChild("clocks6",{static:false, read: ElementRef})clocks6: ElementRef;
  data

  show = "";
  userType: any;
  setlocalstorage
  getlocalstorage

    constructor(private datashare:ShareddataService){

      // setTimeout(()=>{
      //   this.datashare.apiData$.subscribe(data => this.data = data);
      //   const w = JSON.parse(this.data);
      //   // const a = w.data[0];
      //   // console.log(a);

      //   for(var i = 0; i < a.length; i++) {
      //     // console.log(a[i].CountryName);
      //     this.show += a[i].CountryName + ","
      // }

      // },20000)
    } 


    ngOnInit():void {
      this.getlocalstorage = sessionStorage.getItem("AGENT_NAME");        //GETTING THE USERNAME
    }


    ngAfterViewInit(): void {     
      setInterval(()=>{
        //forloop for time
        const clocks = document.getElementsByClassName("clock");
        for (let clock of clocks) {
          let timezones = clock.id
          var d = new Date().toLocaleString('en-US', {
            hour: '2-digit',
            minute:'2-digit',
            hour12: false,
             timeZone: timezones });

          clock.textContent = d;
          var dm = d.substring(0, d.length-3);
          var z:number
          z= +dm 
          // console.log(w)

        // console.clear()
        var w:any = parseInt(document.getElementById("Asia/Shanghai").innerHTML);
        if(w > '18'){
          this.clock.nativeElement.style.border = `10px solid #555`;
        }else{
          this.clock.nativeElement.style.border = `10px solid #fff`;
        }

        var x:any = parseInt(document.getElementById("Asia/Bangkok").innerHTML);
        if(x > '18'){
          this.clocks.nativeElement.style.border = `10px solid #555`;
        }else{
          this.clocks.nativeElement.style.border = `10px solid #fff`;
        }

        var y:any = parseInt(document.getElementById("America/New_York").innerHTML);
        if(y > '18'){
          this.clockss.nativeElement.style.border = `10px solid #555`;
        }else{
          this.clockss.nativeElement.style.border = `10px solid #fff`;
        }

        var a:any = parseInt(document.getElementById("Europe/Berlin").innerHTML);
        if(a > '18'){
          this.clocks4.nativeElement.style.border = `10px solid #555`;
        }else{
          this.clocks4.nativeElement.style.border = `10px solid #fff`;
        }

        var b:any = parseInt(document.getElementById("Asia/Bangkok").innerHTML);
        if(b > '18'){
          this.clocks5.nativeElement.style.border = `10px solid #555`;
        }else{
          this.clocks5.nativeElement.style.border = `10px solid #fff`;
        }

        var c:any = parseInt(document.getElementById("Asia/Bangkok").innerHTML);
        if(c > '18'){
          this.clocks6.nativeElement.style.border = `10px solid #555`;
        }else{
          this.clocks6.nativeElement.style.border = `10px solid #fff`;
        }


      }
      },1000);

    }

    logout(){localStorage.clear();}
    
}
