
export const Permission =
{
    None: 0,
    Receive: 1,
    Send: 2
}

export const PaymentMode =
{
    None: 0,
    Bank_Transfer: 1,
    Cash: 2,
    Draft: 3,
    Bank_Receive: 4,
    Cash_Receive: 5,
    Draft_Receive: 6
}