export let dataSource: { [key: string]: Object }[] = [
    {
      "UR_FUNCTIONID": 75,
      "FunctionName": "Master",
      "Parent": "0",
      "NavigationURL": null,
      "LevelID": 1,
      "SequenceID": 0,
      "submenus": [
        {
          "UR_FUNCTIONID": 147,
          "FunctionName": "Global",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 0
        },
        {
          "UR_FUNCTIONID": 148,
          "FunctionName": "Global Credit Limit",
          "Parent": "Global",
          "NavigationURL": "frmViewCreditLimit_ForGlobal.aspx",
          "LevelID": 3,
          "SequenceID": 148
        },
        {
          "UR_FUNCTIONID": 167,
          "FunctionName": "Global Transaction Limit",
          "Parent": "Global",
          "NavigationURL": "frmViewTransactionGlobalLimit.aspx",
          "LevelID": 3,
          "SequenceID": 167
        },
        {
          "UR_FUNCTIONID": 5,
          "FunctionName": "Currency",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 1
        },
        {
          "UR_FUNCTIONID": 71,
          "FunctionName": "Currency Management",
          "Parent": "Currency",
          "NavigationURL": "frmViewCurrency.aspx",
          "LevelID": 3,
          "SequenceID": 2
        },
        {
          "UR_FUNCTIONID": 1,
          "FunctionName": "Country",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 3
        },
        {
          "UR_FUNCTIONID": 2,
          "FunctionName": "Country Management",
          "Parent": "Country",
          "NavigationURL": "frmViewCountry.aspx",
          "LevelID": 3,
          "SequenceID": 4
        },
        {
          "UR_FUNCTIONID": 3,
          "FunctionName": "Mode of Payment",
          "Parent": "Country",
          "NavigationURL": "frmViewCountryModeOfPayment.aspx",
          "LevelID": 3,
          "SequenceID": 5
        },
        {
          "UR_FUNCTIONID": 4,
          "FunctionName": "Permission List",
          "Parent": "Country",
          "NavigationURL": "frmViewCountryPermissionList.aspx",
          "LevelID": 3,
          "SequenceID": 6
        },
        {
          "UR_FUNCTIONID": 17,
          "FunctionName": "City Management",
          "Parent": "Country",
          "NavigationURL": "frmViewCity.aspx",
          "LevelID": 3,
          "SequenceID": 7
        },
        {
          "UR_FUNCTIONID": 18,
          "FunctionName": "Location Management",
          "Parent": "Country",
          "NavigationURL": "frmViewLocation.aspx",
          "LevelID": 3,
          "SequenceID": 8
        },
        {
          "UR_FUNCTIONID": 69,
          "FunctionName": "Country Mandatory Field ",
          "Parent": "Country",
          "NavigationURL": "frmAddMandetoryField.aspx",
          "LevelID": 3,
          "SequenceID": 9
        },
        {
          "UR_FUNCTIONID": 145,
          "FunctionName": "Country Credit Limit",
          "Parent": "Country",
          "NavigationURL": "frmViewCreditLimit_ForCountry.aspx",
          "LevelID": 3,
          "SequenceID": 145
        },
        {
          "UR_FUNCTIONID": 1330,
          "FunctionName": "Country ID Registration",
          "Parent": "Country",
          "NavigationURL": "frmCountryIDRegistration.aspx",
          "LevelID": 3,
          "SequenceID": 327
        },
        {
          "UR_FUNCTIONID": 6,
          "FunctionName": "Purpose of Transfer",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 10
        },
        {
          "UR_FUNCTIONID": 72,
          "FunctionName": "Purpose of Transfer Management",
          "Parent": "Purpose of Transfer",
          "NavigationURL": "frmViewPurposeOfTransfer.aspx",
          "LevelID": 3,
          "SequenceID": 11
        },
        {
          "UR_FUNCTIONID": 7,
          "FunctionName": "Exchange Rates",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 12
        },
        {
          "UR_FUNCTIONID": 8,
          "FunctionName": "Agency Rates",
          "Parent": "Exchange Rates",
          "NavigationURL": "frmViewUSDWiseExchangeRateTest.aspx",
          "LevelID": 3,
          "SequenceID": 13
        },
        {
          "UR_FUNCTIONID": 9,
          "FunctionName": "Bank Rates",
          "Parent": "Exchange Rates",
          "NavigationURL": "frmViewRatesTest.aspx",
          "LevelID": 3,
          "SequenceID": 14
        },
        {
          "UR_FUNCTIONID": 10,
          "FunctionName": "Daily Rate",
          "Parent": "Exchange Rates",
          "NavigationURL": "frmDailyRates1.aspx",
          "LevelID": 3,
          "SequenceID": 15
        },
        {
          "UR_FUNCTIONID": 60,
          "FunctionName": "Agent Rates",
          "Parent": "Exchange Rates",
          "NavigationURL": "frmViewSendingAgentExchangeRateTest.aspx",
          "LevelID": 3,
          "SequenceID": 17
        },
        {
          "UR_FUNCTIONID": 247,
          "FunctionName": "RATES (MIN - MAX)",
          "Parent": "Exchange Rates",
          "NavigationURL": "FrmEditMinMaxRate.aspx",
          "LevelID": 3,
          "SequenceID": 213
        },
        {
          "UR_FUNCTIONID": 281,
          "FunctionName": "Daily Rate(s..)",
          "Parent": "Exchange Rates",
          "NavigationURL": "FrmLoadDailyRpt.aspx",
          "LevelID": 3,
          "SequenceID": 275
        },
        {
          "UR_FUNCTIONID": 287,
          "FunctionName": "Auto Update Bank Rates",
          "Parent": "Exchange Rates",
          "NavigationURL": "frmAutoUpdateBankRate.aspx",
          "LevelID": 3,
          "SequenceID": 276
        },
        {
          "UR_FUNCTIONID": 1333,
          "FunctionName": "Sending Agent Rates",
          "Parent": "Exchange Rates",
          "NavigationURL": "frmUpdateSpecificSendingAgentExchangeRate.aspx",
          "LevelID": 3,
          "SequenceID": 333
        },
        {
          "UR_FUNCTIONID": 1351,
          "FunctionName": "Exchange Rate(s)",
          "Parent": "Exchange Rates",
          "NavigationURL": "frmViewGatewayRates.aspx",
          "LevelID": 3,
          "SequenceID": 348
        },
        {
          "UR_FUNCTIONID": 11,
          "FunctionName": "Margins",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 19
        },
        {
          "UR_FUNCTIONID": 12,
          "FunctionName": "Agency Margins",
          "Parent": "Margins",
          "NavigationURL": "frmViewMargins.aspx",
          "LevelID": 3,
          "SequenceID": 20
        },
        {
          "UR_FUNCTIONID": 61,
          "FunctionName": "Agent Margins",
          "Parent": "Margins",
          "NavigationURL": "frmSendingAgentMarginAgencyUser.aspx",
          "LevelID": 3,
          "SequenceID": 21
        },
        {
          "UR_FUNCTIONID": 13,
          "FunctionName": "Premium Rate",
          "Parent": "Margins",
          "NavigationURL": "frmViewAgentVariance.aspx",
          "LevelID": 3,
          "SequenceID": 22
        },
        {
          "UR_FUNCTIONID": 173,
          "FunctionName": "Bulk Premium Rate",
          "Parent": "Margins",
          "NavigationURL": "frmViewAgentVariance_New.aspx",
          "LevelID": 3,
          "SequenceID": 173
        },
        {
          "UR_FUNCTIONID": 174,
          "FunctionName": "Agent - SubAgent Margin",
          "Parent": "Margins",
          "NavigationURL": "frmSubSendingAgentMargin.aspx",
          "LevelID": 3,
          "SequenceID": 174
        },
        {
          "UR_FUNCTIONID": 230,
          "FunctionName": "CTA Hold Margin",
          "Parent": "Margins",
          "NavigationURL": "FrmEditCTAHoldMargin.aspx",
          "LevelID": 3,
          "SequenceID": 176
        },
        {
          "UR_FUNCTIONID": 14,
          "FunctionName": "Commission",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 23
        },
        {
          "UR_FUNCTIONID": 15,
          "FunctionName": "Slabs",
          "Parent": "Commission",
          "NavigationURL": "frmViewCommissionSlabTest.aspx",
          "LevelID": 3,
          "SequenceID": 24
        },
        {
          "UR_FUNCTIONID": 16,
          "FunctionName": "Agent Wise Slabs",
          "Parent": "Commission",
          "NavigationURL": "frmViewAgentCommissionSlabTest.aspx",
          "LevelID": 3,
          "SequenceID": 25
        },
        {
          "UR_FUNCTIONID": 118,
          "FunctionName": "Receiving Agent Min Max Slabs",
          "Parent": "Commission",
          "NavigationURL": "frmViewReceivingAgentPerCommission.aspx",
          "LevelID": 3,
          "SequenceID": 27
        },
        {
          "UR_FUNCTIONID": 1332,
          "FunctionName": "BackEnd Charges",
          "Parent": "Commission",
          "NavigationURL": "frmViewBackEndCharges.aspx",
          "LevelID": 3,
          "SequenceID": 333
        },
        {
          "UR_FUNCTIONID": 19,
          "FunctionName": "Agent",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 28
        },
        {
          "UR_FUNCTIONID": 20,
          "FunctionName": "Agent Management",
          "Parent": "Agent",
          "NavigationURL": "frmViewExchangeBranch1.aspx",
          "LevelID": 3,
          "SequenceID": 29
        },
        {
          "UR_FUNCTIONID": 21,
          "FunctionName": "Edit Mode of Payment",
          "Parent": "Agent",
          "NavigationURL": "frmViewAgentModeOfPayment1.aspx",
          "LevelID": 3,
          "SequenceID": 30
        },
        {
          "UR_FUNCTIONID": 73,
          "FunctionName": "Agent Branch Management",
          "Parent": "Agent",
          "NavigationURL": "frmViewAgentBranch1.aspx",
          "LevelID": 3,
          "SequenceID": 31
        },
        {
          "UR_FUNCTIONID": 146,
          "FunctionName": "Agent Credit Limit",
          "Parent": "Agent",
          "NavigationURL": "frmViewCreditLimit_ForAgent.aspx",
          "LevelID": 3,
          "SequenceID": 146
        },
        {
          "UR_FUNCTIONID": 177,
          "FunctionName": "SubAgent Credit Limit",
          "Parent": "Agent",
          "NavigationURL": "frmSubAgentCreditLimit.aspx",
          "LevelID": 3,
          "SequenceID": 177
        },
        {
          "UR_FUNCTIONID": 64,
          "FunctionName": "Account Management",
          "Parent": "Master",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 32
        },
            {
                "UR_FUNCTIONID": 65,
                "FunctionName": "Account Details",
                "Parent": "Account Management",
                "NavigationURL": "frmViewAccountDetails.aspx",
                "LevelID": 3,
                "SequenceID": 33
              }          
      ]
    },
    {
      "UR_FUNCTIONID": 22,
      "FunctionName": "Settings",
      "Parent": "0",
      "NavigationURL": null,
      "LevelID": 1,
      "SequenceID": 38,
      "submenus": [
        {
          "UR_FUNCTIONID": 144,
          "FunctionName": "Machine Registration Management",
          "Parent": "Settings",
          "NavigationURL": "frmRegisteredMacAdd.aspx",
          "LevelID": 2,
          "SequenceID": 144
        },
        {
          "UR_FUNCTIONID": 322,
          "FunctionName": "User Configuration",
          "Parent": "Settings",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 318
        },
        {
          "UR_FUNCTIONID": 323,
          "FunctionName": "User Groups",
          "Parent": "User Configuration",
          "NavigationURL": "User_Group.aspx",
          "LevelID": 3,
          "SequenceID": 319
        },
        {
          "UR_FUNCTIONID": 324,
          "FunctionName": "Users",
          "Parent": "User Configuration",
          "NavigationURL": "Users.aspx",
          "LevelID": 3,
          "SequenceID": 320
        },
        {
          "UR_FUNCTIONID": 325,
          "FunctionName": "User Rights",
          "Parent": "User Configuration",
          "NavigationURL": "User_Rights.aspx",
          "LevelID": 3,
          "SequenceID": 321
        },
        {
          "UR_FUNCTIONID": 1331,
          "FunctionName": "API User Access",
          "Parent": "User Configuration",
          "NavigationURL": "APIUserAccess.aspx",
          "LevelID": 3,
          "SequenceID": 332
        }
      ]
    },
    {
      "UR_FUNCTIONID": 23,
      "FunctionName": "Others",
      "Parent": "0",
      "NavigationURL": null,
      "LevelID": 1,
      "SequenceID": 44,
      "submenus": [
        {
          "UR_FUNCTIONID": 117,
          "FunctionName": "Release Transactions Payout",
          "Parent": "Others",
          "NavigationURL": "frmViewCTAHoldTransaction.aspx",
          "LevelID": 2,
          "SequenceID": 16
        },
        {
          "UR_FUNCTIONID": 53,
          "FunctionName": "Edit Transaction",
          "Parent": "Others",
          "NavigationURL": "frmEditTransaction.aspx",
          "LevelID": 2,
          "SequenceID": 47
        },
        {
          "UR_FUNCTIONID": 132,
          "FunctionName": "Download Transaction(s)",
          "Parent": "Others",
          "NavigationURL": "DownloadTransaction.aspx",
          "LevelID": 2,
          "SequenceID": 49
        },
        {
          "UR_FUNCTIONID": 54,
          "FunctionName": "View Transaction(s) Status",
          "Parent": "Others",
          "NavigationURL": "frmViewTransactionStatus.aspx",
          "LevelID": 2,
          "SequenceID": 49
        },
        {
          "UR_FUNCTIONID": 68,
          "FunctionName": "Stop Payment",
          "Parent": "Others",
          "NavigationURL": "frmViewStopPayment.aspx",
          "LevelID": 2,
          "SequenceID": 53
        },
        {
          "UR_FUNCTIONID": 63,
          "FunctionName": "Funding",
          "Parent": "Others",
          "NavigationURL": "frmFunding.aspx",
          "LevelID": 2,
          "SequenceID": 54
        },
        {
          "UR_FUNCTIONID": 133,
          "FunctionName": "Cancellation Request",
          "Parent": "Others",
          "NavigationURL": "frmCancelRequest.aspx",
          "LevelID": 2,
          "SequenceID": 131
        },
        {
          "UR_FUNCTIONID": 134,
          "FunctionName": "Edit Transaction(s)",
          "Parent": "Others",
          "NavigationURL": "frmEditTransactionByAgent.aspx",
          "LevelID": 2,
          "SequenceID": 132
        },
        {
          "UR_FUNCTIONID": 136,
          "FunctionName": "View Cancel Transaction(s)",
          "Parent": "Others",
          "NavigationURL": "frmViewCancelRequestStatus.aspx",
          "LevelID": 2,
          "SequenceID": 135
        },
        {
          "UR_FUNCTIONID": 138,
          "FunctionName": "Approve Transaction",
          "Parent": "Others",
          "NavigationURL": "frmViewApproveTransactionBySupervisor.aspx",
          "LevelID": 2,
          "SequenceID": 138
        },
        {
          "UR_FUNCTIONID": 139,
          "FunctionName": "View Pending for Approval",
          "Parent": "Others",
          "NavigationURL": "frmViewPendingApproval.aspx",
          "LevelID": 2,
          "SequenceID": 139
        },
        {
          "UR_FUNCTIONID": 140,
          "FunctionName": "View Transaction Status",
          "Parent": "Others",
          "NavigationURL": "frmViewTransactionStatusForAgent.aspx",
          "LevelID": 2,
          "SequenceID": 140
        },
        {
          "UR_FUNCTIONID": 150,
          "FunctionName": "View Receive Transaction",
          "Parent": "Others",
          "NavigationURL": "frmViewTransactionStatusForRAgent.aspx",
          "LevelID": 2,
          "SequenceID": 150
        },
        {
          "UR_FUNCTIONID": 169,
          "FunctionName": "Complaint Management",
          "Parent": "Others",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 169
        },
        {
          "UR_FUNCTIONID": 171,
          "FunctionName": "Transaction Complaints",
          "Parent": "Complaint Management",
          "NavigationURL": "ComplaintMgnt.aspx",
          "LevelID": 3,
          "SequenceID": 171
        },
        {
          "UR_FUNCTIONID": 172,
          "FunctionName": "Pending Complaints",
          "Parent": "Complaint Management",
          "NavigationURL": "FrmViewComplaintSolve.aspx",
          "LevelID": 3,
          "SequenceID": 172
        },
        {
          "UR_FUNCTIONID": 176,
          "FunctionName": "Complaint Reports",
          "Parent": "Complaint Management",
          "NavigationURL": "RptComplaint.aspx",
          "LevelID": 3,
          "SequenceID": 176
        },
        {
          "UR_FUNCTIONID": 179,
          "FunctionName": "Pending Complaint (UserGroupwise)",
          "Parent": "Complaint Management",
          "NavigationURL": "FrmViewComplaintSolveSubj.aspx",
          "LevelID": 3,
          "SequenceID": 179
        },
        {
          "UR_FUNCTIONID": 198,
          "FunctionName": "Update CS Complaint",
          "Parent": "Complaint Management",
          "NavigationURL": "FrmCRExternalUpdateComplaint.aspx",
          "LevelID": 3,
          "SequenceID": 198
        },
        {
          "UR_FUNCTIONID": 261,
          "FunctionName": "Agentwise Complaint Report",
          "Parent": "Complaint Management",
          "NavigationURL": "FrmgetAgentWisePendingComplaint.aspx",
          "LevelID": 3,
          "SequenceID": 256
        },
        {
          "UR_FUNCTIONID": 180,
          "FunctionName": "SubAgent Funding",
          "Parent": "Others",
          "NavigationURL": "frmSubAgentFunding.aspx",
          "LevelID": 2,
          "SequenceID": 180
        },
        {
          "UR_FUNCTIONID": 181,
          "FunctionName": "Transaction Process Log",
          "Parent": "Others",
          "NavigationURL": "FrmTransProcessLog.aspx",
          "LevelID": 2,
          "SequenceID": 181
        },
        {
          "UR_FUNCTIONID": 187,
          "FunctionName": "View Pending forAutorization",
          "Parent": "Others",
          "NavigationURL": "frmViewPendingAuthorization.aspx",
          "LevelID": 2,
          "SequenceID": 187
        },
        {
          "UR_FUNCTIONID": 191,
          "FunctionName": "Pay Out Cash Transaction",
          "Parent": "Others",
          "NavigationURL": "frmPaidTransactionWS_ForSR2.aspx",
          "LevelID": 2,
          "SequenceID": 191
        },
        {
          "UR_FUNCTIONID": 194,
          "FunctionName": "Mobile Topup Transfer",
          "Parent": "Others",
          "NavigationURL": "frmMobileToppouTransaction.aspx",
          "LevelID": 2,
          "SequenceID": 194
        },
        {
          "UR_FUNCTIONID": 195,
          "FunctionName": "Block Transaction",
          "Parent": "Others",
          "NavigationURL": "frmBlockTransactionByAgent.aspx",
          "LevelID": 2,
          "SequenceID": 195
        },
        {
          "UR_FUNCTIONID": 196,
          "FunctionName": "Block Unblock Transaction",
          "Parent": "Others",
          "NavigationURL": "frmViewTransactionStatusForAgent1.aspx",
          "LevelID": 2,
          "SequenceID": 196
        },
        {
          "UR_FUNCTIONID": 200,
          "FunctionName": "Cancel Paid Transaction",
          "Parent": "Others",
          "NavigationURL": "frmViewPaidCancelRequestStatus.aspx",
          "LevelID": 2,
          "SequenceID": 200
        },
        {
          "UR_FUNCTIONID": 201,
          "FunctionName": "CTA Verify Transaction",
          "Parent": "Others",
          "NavigationURL": "frmViewCTAVerifyTransaction.aspx",
          "LevelID": 2,
          "SequenceID": 201
        },
        {
          "UR_FUNCTIONID": 249,
          "FunctionName": "View Downloaded Trxn File",
          "Parent": "Others",
          "NavigationURL": "FrmDownloadFileView.aspx",
          "LevelID": 2,
          "SequenceID": 215
        },
        {
          "UR_FUNCTIONID": 215,
          "FunctionName": "View Receive Transaction(s)",
          "Parent": "Others",
          "NavigationURL": "frmReceiveTransactionStatusForAgency.aspx",
          "LevelID": 2,
          "SequenceID": 215
        },
        {
          "UR_FUNCTIONID": 250,
          "FunctionName": "Receiving Agent Trxn List",
          "Parent": "Others",
          "NavigationURL": "frmReceivingAgentTrxnList.aspx",
          "LevelID": 2,
          "SequenceID": 216
        },
        {
          "UR_FUNCTIONID": 251,
          "FunctionName": "Receive Transaction(S)",
          "Parent": "Others",
          "NavigationURL": "frmReceiveTransactionCashBank2.aspx",
          "LevelID": 2,
          "SequenceID": 217
        },
        {
          "UR_FUNCTIONID": 258,
          "FunctionName": "CTA Hold Summary",
          "Parent": "Others",
          "NavigationURL": "frmViewCTAHoldTrxnSummary.aspx",
          "LevelID": 2,
          "SequenceID": 243
        },
        {
          "UR_FUNCTIONID": 259,
          "FunctionName": "Receive Transaction Status Summary",
          "Parent": "Others",
          "NavigationURL": "frmViewReceiveTransactionStatusSummary.aspx",
          "LevelID": 2,
          "SequenceID": 244
        },
        {
          "UR_FUNCTIONID": 274,
          "FunctionName": "My Card Registration",
          "Parent": "Others",
          "NavigationURL": "FrmSpeedClubCard.aspx",
          "LevelID": 2,
          "SequenceID": 269
        },
        {
          "UR_FUNCTIONID": 280,
          "FunctionName": "Edit Paid Transactions",
          "Parent": "Others",
          "NavigationURL": "FrmTransactionEditForRedownload.aspx",
          "LevelID": 2,
          "SequenceID": 274
        },
        {
          "UR_FUNCTIONID": 284,
          "FunctionName": "Agent SOA Email",
          "Parent": "Others",
          "NavigationURL": "EmailSOA.aspx",
          "LevelID": 2,
          "SequenceID": 278
        },
        {
          "UR_FUNCTIONID": 289,
          "FunctionName": "View Suspected Transactions",
          "Parent": "Others",
          "NavigationURL": "frmViewSuspectedTrxn.aspx",
          "LevelID": 2,
          "SequenceID": 282
        },
        {
          "UR_FUNCTIONID": 320,
          "FunctionName": "Vostro Account Balance",
          "Parent": "Others",
          "NavigationURL": "frmViewVOSTROBalance.aspx",
          "LevelID": 2,
          "SequenceID": 316
        },
        {
          "UR_FUNCTIONID": 1327,
          "FunctionName": "ReqRespReport",
          "Parent": "Others",
          "NavigationURL": "REq_Resp_Report.aspx",
          "LevelID": 2,
          "SequenceID": 324
        },
        {
          "UR_FUNCTIONID": 1334,
          "FunctionName": "View API Transaction Status",
          "Parent": "Others",
          "NavigationURL": "frmViewTransactionStatusFor_API_Agent.aspx",
          "LevelID": 2,
          "SequenceID": 334
        },
        {
          "UR_FUNCTIONID": 1336,
          "FunctionName": "View B2B Swift Transfer",
          "Parent": "Others",
          "NavigationURL": "frmViewCustomers.aspx",
          "LevelID": 2,
          "SequenceID": 336
        }
      ]
    },
    {
      "UR_FUNCTIONID": 50,
      "FunctionName": "Financial Accounting",
      "Parent": "0",
      "NavigationURL": null,
      "LevelID": 1,
      "SequenceID": 62,
      "submenus": [
        {
          "UR_FUNCTIONID": 24,
          "FunctionName": "Journal Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 63
        },
        {
          "UR_FUNCTIONID": 25,
          "FunctionName": "J Voucher",
          "Parent": "Journal Voucher",
          "NavigationURL": "NEWJournalVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 64
        },
        {
          "UR_FUNCTIONID": 26,
          "FunctionName": "JV Authorization",
          "Parent": "Journal Voucher",
          "NavigationURL": "Authorization_JV_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 65
        },
        {
          "UR_FUNCTIONID": 27,
          "FunctionName": "Edit Journal Voucher",
          "Parent": "Journal Voucher",
          "NavigationURL": "Edit_JournalVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 66
        },
        {
          "UR_FUNCTIONID": 226,
          "FunctionName": "J Voucher(S)",
          "Parent": "Journal Voucher(S)",
          "NavigationURL": "FrmCancelTransaction.Aspx",
          "LevelID": 3,
          "SequenceID": 226
        },
        {
          "UR_FUNCTIONID": 227,
          "FunctionName": "Jounral Voucher(s) Authorization",
          "Parent": "Journal Voucher(S)",
          "NavigationURL": "Frm_CancelAuthoTransaction.Aspx",
          "LevelID": 3,
          "SequenceID": 227
        },
        {
          "UR_FUNCTIONID": 28,
          "FunctionName": "Bank Receipt Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 67
        },
        {
          "UR_FUNCTIONID": 29,
          "FunctionName": "BR Voucher",
          "Parent": "Bank Receipt Voucher",
          "NavigationURL": "NEWBankReceiptVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 68
        },
        {
          "UR_FUNCTIONID": 30,
          "FunctionName": "BR Authorization",
          "Parent": "Bank Receipt Voucher",
          "NavigationURL": "Authorization_BR_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 69
        },
        {
          "UR_FUNCTIONID": 31,
          "FunctionName": "Edit Bank Receipt",
          "Parent": "Bank Receipt Voucher",
          "NavigationURL": "Edit_BankReceiptVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 70
        },
        {
          "UR_FUNCTIONID": 32,
          "FunctionName": "Bank Payment Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 71
        },
        {
          "UR_FUNCTIONID": 33,
          "FunctionName": "BP Voucher",
          "Parent": "Bank Payment Voucher",
          "NavigationURL": "NEWBankPaymentVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 72
        },
        {
          "UR_FUNCTIONID": 34,
          "FunctionName": "BP Authorization",
          "Parent": "Bank Payment Voucher",
          "NavigationURL": "Authorization_BP_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 73
        },
        {
          "UR_FUNCTIONID": 35,
          "FunctionName": "Edit Bank Payment",
          "Parent": "Bank Payment Voucher",
          "NavigationURL": "Edit_BankPaymentVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 74
        },
        {
          "UR_FUNCTIONID": 36,
          "FunctionName": "Cash Receipt Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 75
        },
        {
          "UR_FUNCTIONID": 37,
          "FunctionName": "CR Voucher",
          "Parent": "Cash Receipt Voucher",
          "NavigationURL": "NEWCashReceiptVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 76
        },
        {
          "UR_FUNCTIONID": 38,
          "FunctionName": "CR Authorization",
          "Parent": "Cash Receipt Voucher",
          "NavigationURL": "Authorization_CR_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 77
        },
        {
          "UR_FUNCTIONID": 39,
          "FunctionName": "Edit Cash Receipt",
          "Parent": "Cash Receipt Voucher",
          "NavigationURL": "Edit_CashReceiptVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 78
        },
        {
          "UR_FUNCTIONID": 40,
          "FunctionName": "Cash Payment Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 79
        },
        {
          "UR_FUNCTIONID": 41,
          "FunctionName": "CP Voucher",
          "Parent": "Cash Payment Voucher",
          "NavigationURL": "NEWCashPaymentVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 80
        },
        {
          "UR_FUNCTIONID": 42,
          "FunctionName": "CP Authorization",
          "Parent": "Cash Payment Voucher",
          "NavigationURL": "Authorization_CP_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 81
        },
        {
          "UR_FUNCTIONID": 43,
          "FunctionName": "Edit Cash Payment",
          "Parent": "Cash Payment Voucher",
          "NavigationURL": "Edit_CashPaymentVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 82
        },
        {
          "UR_FUNCTIONID": 112,
          "FunctionName": "Contra Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 83
        },
        {
          "UR_FUNCTIONID": 113,
          "FunctionName": "Contra Entry",
          "Parent": "Contra Voucher",
          "NavigationURL": "NEWContraVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 84
        },
        {
          "UR_FUNCTIONID": 114,
          "FunctionName": "Contra Authorization",
          "Parent": "Contra Voucher",
          "NavigationURL": "Authorization_Contra_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 85
        },
        {
          "UR_FUNCTIONID": 119,
          "FunctionName": "Edit Contra",
          "Parent": "Contra Voucher",
          "NavigationURL": "Edit_ContraVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 86
        },
        {
          "UR_FUNCTIONID": 184,
          "FunctionName": "Bank Reconciliation",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 184
        },
        {
          "UR_FUNCTIONID": 189,
          "FunctionName": "Upload Bank Reco File",
          "Parent": "Bank Reconciliation",
          "NavigationURL": "FrmBankRecoUpload.aspx",
          "LevelID": 3,
          "SequenceID": 187
        },
        {
          "UR_FUNCTIONID": 190,
          "FunctionName": "View Bank Reconciliation",
          "Parent": "Bank Reconciliation",
          "NavigationURL": "frmBankReco.aspx",
          "LevelID": 3,
          "SequenceID": 188
        },
        {
          "UR_FUNCTIONID": 203,
          "FunctionName": "Financial Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 203
        },
        {
          "UR_FUNCTIONID": 204,
          "FunctionName": "Voucher Listing",
          "Parent": "Financial Voucher",
          "NavigationURL": "FrmVoucherDetails.aspx",
          "LevelID": 3,
          "SequenceID": 204
        },
        {
          "UR_FUNCTIONID": 205,
          "FunctionName": "Duplicate Voucher Receipt",
          "Parent": "Financial Voucher",
          "NavigationURL": "FrmDuplicateVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 205
        },
        {
          "UR_FUNCTIONID": 214,
          "FunctionName": "Transactions Cancellation Listing",
          "Parent": "Financial Voucher",
          "NavigationURL": "FrmCancelTransactionRpt.aspx",
          "LevelID": 3,
          "SequenceID": 214
        },
        {
          "UR_FUNCTIONID": 254,
          "FunctionName": "Duplicate Voucher Receipt As Per TR1",
          "Parent": "Financial Voucher",
          "NavigationURL": "FrmDuplicateVoucherSR1.aspx",
          "LevelID": 3,
          "SequenceID": 240
        },
        {
          "UR_FUNCTIONID": 211,
          "FunctionName": "Deal Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 210
        },
        {
          "UR_FUNCTIONID": 107,
          "FunctionName": "Deal-Voucher",
          "Parent": "Deal Voucher",
          "NavigationURL": "frmDealVoucher.aspx",
          "LevelID": 3,
          "SequenceID": 56
        },
        {
          "UR_FUNCTIONID": 210,
          "FunctionName": "Deal Authorization",
          "Parent": "Deal Voucher",
          "NavigationURL": "Authorization_DL_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 211
        },
        {
          "UR_FUNCTIONID": 212,
          "FunctionName": "Deal Cancel",
          "Parent": "Deal Voucher",
          "NavigationURL": "FrmDealReve.aspx",
          "LevelID": 3,
          "SequenceID": 212
        },
        {
          "UR_FUNCTIONID": 213,
          "FunctionName": "Cancel Deal Authorization",
          "Parent": "Deal Voucher",
          "NavigationURL": "AuthoriZation_ReviseDL_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 213
        },
        {
          "UR_FUNCTIONID": 216,
          "FunctionName": "Commission Balance Transfer",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 216
        },
        {
          "UR_FUNCTIONID": 219,
          "FunctionName": "Commission Transfer",
          "Parent": "Commission Balance Transfer",
          "NavigationURL": "frmCommissionTransferToPrincipal.aspx",
          "LevelID": 3,
          "SequenceID": 219
        },
        {
          "UR_FUNCTIONID": 220,
          "FunctionName": "CT Authorization",
          "Parent": "Commission Balance Transfer",
          "NavigationURL": "Authorization_CT_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 220
        },
        {
          "UR_FUNCTIONID": 217,
          "FunctionName": "Principal Balance Transfer",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 217
        },
        {
          "UR_FUNCTIONID": 221,
          "FunctionName": "Principal Transfer",
          "Parent": "Principal Balance Transfer",
          "NavigationURL": "frmPrincipalTransferBalance.aspx",
          "LevelID": 3,
          "SequenceID": 221
        },
        {
          "UR_FUNCTIONID": 222,
          "FunctionName": "PT Authorization",
          "Parent": "Principal Balance Transfer",
          "NavigationURL": "Authorization_TR_Approved.aspx",
          "LevelID": 3,
          "SequenceID": 222
        },
        {
          "UR_FUNCTIONID": 218,
          "FunctionName": "Transfer To USD A/C",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 218
        },
        {
          "UR_FUNCTIONID": 223,
          "FunctionName": "Transfer To USD A/C (Paid Transaction)",
          "Parent": "Transfer To USD A/C",
          "NavigationURL": "frmTransferToUSD.aspx",
          "LevelID": 3,
          "SequenceID": 223
        },
        {
          "UR_FUNCTIONID": 225,
          "FunctionName": "Journal Voucher(S)",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 225
        },
        {
          "UR_FUNCTIONID": 231,
          "FunctionName": "BR Voucher",
          "Parent": "Bank Receipt(S) Voucher",
          "NavigationURL": "FrmAutoBankReceipt.aspx",
          "LevelID": 3,
          "SequenceID": 232
        },
        {
          "UR_FUNCTIONID": 233,
          "FunctionName": "Bank Receipt(S) Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 230
        },
        {
          "UR_FUNCTIONID": 232,
          "FunctionName": "BP Voucher",
          "Parent": "Bank Payment(S) Voucher",
          "NavigationURL": "FrmAutoBankPayment.aspx",
          "LevelID": 3,
          "SequenceID": 233
        },
        {
          "UR_FUNCTIONID": 234,
          "FunctionName": "Bank Payment(S) Voucher",
          "Parent": "Financial Accounting",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 231
        }
      ]
    },
    {
      "UR_FUNCTIONID": 93,
      "FunctionName": "Reports",
      "Parent": "0",
      "NavigationURL": null,
      "LevelID": 1,
      "SequenceID": 87,
      "submenus": [
        {
          "UR_FUNCTIONID": 95,
          "FunctionName": "TR",
          "Parent": "Reports",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 89
        },
        {
          "UR_FUNCTIONID": 96,
          "FunctionName": "Finance & Accounts",
          "Parent": "TR",
          "NavigationURL": null,
          "LevelID": 3,
          "SequenceID": 90
        },
        {
          "UR_FUNCTIONID": 98,
          "FunctionName": "Commission & Income",
          "Parent": "Finance & Accounts",
          "NavigationURL": "Finance_Accounts_Reports\\FinanceAndAccounts_Reports.aspx",
          "LevelID": 4,
          "SequenceID": 91
        },
        {
          "UR_FUNCTIONID": 120,
          "FunctionName": "Financial & Accounting",
          "Parent": "Finance & Accounts",
          "NavigationURL": "AccountingReports.aspx",
          "LevelID": 4,
          "SequenceID": 92
        },
        {
          "UR_FUNCTIONID": 206,
          "FunctionName": "TR1 Trial Balance",
          "Parent": "Finance & Accounts",
          "NavigationURL": "frmTrialBalance_SR1.aspx",
          "LevelID": 4,
          "SequenceID": 206
        },
        {
          "UR_FUNCTIONID": 99,
          "FunctionName": "MIS & Audit",
          "Parent": "TR",
          "NavigationURL": "MIS_Reports\\CountryWiseSendReceiveTrxn_Reports.aspx",
          "LevelID": 3,
          "SequenceID": 94
        },
        {
          "UR_FUNCTIONID": 102,
          "FunctionName": "Customer Service",
          "Parent": "TR",
          "NavigationURL": "CustomerService_Reports\\CustomerService_Reports.aspx",
          "LevelID": 3,
          "SequenceID": 97
        },
        {
          "UR_FUNCTIONID": 244,
          "FunctionName": "Corridor wise Trxn Count(As on date)",
          "Parent": "TR",
          "NavigationURL": "AgencySR_Reports\\CountryCorridorwiseRpt.aspx",
          "LevelID": 3,
          "SequenceID": 210
        },
        {
          "UR_FUNCTIONID": 245,
          "FunctionName": "Daily Txns count (Corridor Wise)",
          "Parent": "TR",
          "NavigationURL": "AgencySR_Reports\\RptDailyTxnscountCorridorWise.aspx",
          "LevelID": 3,
          "SequenceID": 211
        },
        {
          "UR_FUNCTIONID": 246,
          "FunctionName": "Deal - Ex Gain (Corridor Wise)",
          "Parent": "TR",
          "NavigationURL": "AgencySR_Reports\\frmDealExGainCorridorWise.aspx",
          "LevelID": 3,
          "SequenceID": 212
        },
        {
          "UR_FUNCTIONID": 267,
          "FunctionName": "Transaction Exchange Gain",
          "Parent": "TR",
          "NavigationURL": "AgencySR_Reports/frmExchangeGainReport.aspx",
          "LevelID": 3,
          "SequenceID": 262
        },
        {
          "UR_FUNCTIONID": 94,
          "FunctionName": "Agent Reports",
          "Parent": "Reports",
          "NavigationURL": "Agent_Reports\\AgentWise_TrxnSentPaid.aspx",
          "LevelID": 2,
          "SequenceID": 94
        },
        {
          "UR_FUNCTIONID": 125,
          "FunctionName": "All Sent Transactions",
          "Parent": "Reports",
          "NavigationURL": "Reports\\AgentWise_TrxnSent.aspx",
          "LevelID": 2,
          "SequenceID": 100
        },
        {
          "UR_FUNCTIONID": 130,
          "FunctionName": "Agent Balances",
          "Parent": "Reports",
          "NavigationURL": "Reports\\AgentBalances.aspx",
          "LevelID": 2,
          "SequenceID": 101
        },
        {
          "UR_FUNCTIONID": 143,
          "FunctionName": "All Paid Transactions",
          "Parent": "Reports",
          "NavigationURL": "Reports\\AgentWise_TrxnPaid.aspx",
          "LevelID": 2,
          "SequenceID": 143
        },
        {
          "UR_FUNCTIONID": 151,
          "FunctionName": "Send Receive Transaction List",
          "Parent": "Reports",
          "NavigationURL": "Agent_Reports\\Agent_Reports.aspx",
          "LevelID": 2,
          "SequenceID": 151
        },
        {
          "UR_FUNCTIONID": 164,
          "FunctionName": "All Sent Transaction (MIS)",
          "Parent": "Reports",
          "NavigationURL": "Reports\\AllAgentWiseMisReport.aspx",
          "LevelID": 2,
          "SequenceID": 164
        },
        {
          "UR_FUNCTIONID": 166,
          "FunctionName": "Transaction Account Details",
          "Parent": "Reports",
          "NavigationURL": "Reports\\TransactionDetailsReport.aspx",
          "LevelID": 2,
          "SequenceID": 166
        },
        {
          "UR_FUNCTIONID": 185,
          "FunctionName": "PayOut Summary",
          "Parent": "Reports",
          "NavigationURL": "Reports\\frmAgentPayOutSummary.aspx",
          "LevelID": 2,
          "SequenceID": 185
        },
        {
          "UR_FUNCTIONID": 188,
          "FunctionName": "CountryWise Agent Balances",
          "Parent": "Reports",
          "NavigationURL": "Reports\\CountryWiseAgentBalance.aspx",
          "LevelID": 2,
          "SequenceID": 186
        },
        {
          "UR_FUNCTIONID": 186,
          "FunctionName": "Search Transactions",
          "Parent": "Reports",
          "NavigationURL": "frmSearchTransactionsOperationRpt.aspx",
          "LevelID": 2,
          "SequenceID": 186
        },
        {
          "UR_FUNCTIONID": 209,
          "FunctionName": "Statement of Account - USD",
          "Parent": "Reports",
          "NavigationURL": "Agent_Reports\\frmUSD_SOA.aspx",
          "LevelID": 2,
          "SequenceID": 209
        },
        {
          "UR_FUNCTIONID": 248,
          "FunctionName": "Recovery Email (SOA)",
          "Parent": "Reports",
          "NavigationURL": "frmRecoveryEmail.aspx",
          "LevelID": 2,
          "SequenceID": 214
        },
        {
          "UR_FUNCTIONID": 228,
          "FunctionName": "Agent TTR Balance",
          "Parent": "Reports",
          "NavigationURL": "Reports\\frmAgentTTRAccount.aspx",
          "LevelID": 2,
          "SequenceID": 228
        },
        {
          "UR_FUNCTIONID": 229,
          "FunctionName": "Fund Flow Statement",
          "Parent": "Reports",
          "NavigationURL": "Reports\\frmFundFlow.aspx",
          "LevelID": 2,
          "SequenceID": 229
        },
        {
          "UR_FUNCTIONID": 235,
          "FunctionName": "Receive Agent Balances",
          "Parent": "Reports",
          "NavigationURL": "Reports\\ReceiveAgentBalances.aspx",
          "LevelID": 2,
          "SequenceID": 232
        },
        {
          "UR_FUNCTIONID": 236,
          "FunctionName": "Transactions Status Report(Paid / Unpaid)",
          "Parent": "Reports",
          "NavigationURL": "FrmTransactionStatuspaidunpaidRpt.aspx",
          "LevelID": 2,
          "SequenceID": 233
        },
        {
          "UR_FUNCTIONID": 238,
          "FunctionName": "FSA Report",
          "Parent": "Reports",
          "NavigationURL": "frmFSARpt.aspx",
          "LevelID": 2,
          "SequenceID": 235
        },
        {
          "UR_FUNCTIONID": 253,
          "FunctionName": "Search Transactions - Mode Wise",
          "Parent": "Reports",
          "NavigationURL": "frmSearchTrxnModeWiseOperationRpt.aspx",
          "LevelID": 2,
          "SequenceID": 239
        },
        {
          "UR_FUNCTIONID": 256,
          "FunctionName": "VT Commission Report",
          "Parent": "Reports",
          "NavigationURL": "frmVTCommissionDetails.aspx",
          "LevelID": 2,
          "SequenceID": 241
        },
        {
          "UR_FUNCTIONID": 260,
          "FunctionName": "Sent Transaction Summary",
          "Parent": "Reports",
          "NavigationURL": "Reports\\frmSentTrxnSummary.aspx",
          "LevelID": 2,
          "SequenceID": 255
        },
        {
          "UR_FUNCTIONID": 263,
          "FunctionName": "Turn-Over Report",
          "Parent": "Reports",
          "NavigationURL": "FrmGetCountryWiseSummaryReport.aspx",
          "LevelID": 2,
          "SequenceID": 258
        },
        {
          "UR_FUNCTIONID": 265,
          "FunctionName": "Austrac Report",
          "Parent": "Reports",
          "NavigationURL": "FrmAustReport.aspx",
          "LevelID": 2,
          "SequenceID": 260
        },
        {
          "UR_FUNCTIONID": 266,
          "FunctionName": "All Sent Transactions (Sales)",
          "Parent": "Reports",
          "NavigationURL": "Reports\\frmAllSentTrxnForSalesP.aspx",
          "LevelID": 2,
          "SequenceID": 261
        },
        {
          "UR_FUNCTIONID": 268,
          "FunctionName": "All Receive (MIS) Reports",
          "Parent": "Reports",
          "NavigationURL": "frmAllReceiveTxnsReport.aspx",
          "LevelID": 2,
          "SequenceID": 263
        },
        {
          "UR_FUNCTIONID": 272,
          "FunctionName": "All Sent Txns Corridor Wise",
          "Parent": "Reports",
          "NavigationURL": "frmAllSendReceiveTxnsReport.aspx",
          "LevelID": 2,
          "SequenceID": 267
        },
        {
          "UR_FUNCTIONID": 278,
          "FunctionName": "Agent Balances Fund Details",
          "Parent": "Reports",
          "NavigationURL": "FrmAgentBalanceFundDetails.aspx",
          "LevelID": 2,
          "SequenceID": 272
        },
        {
          "UR_FUNCTIONID": 282,
          "FunctionName": "Advance Search Transactions",
          "Parent": "Reports",
          "NavigationURL": "frmSearchTransactionsOperationRptForUKOnly.aspx",
          "LevelID": 2,
          "SequenceID": 276
        },
        {
          "UR_FUNCTIONID": 283,
          "FunctionName": "DateWise Trxn Count (MIS)",
          "Parent": "Reports",
          "NavigationURL": "FrmDateWiseTxnsCount.aspx",
          "LevelID": 2,
          "SequenceID": 277
        },
        {
          "UR_FUNCTIONID": 288,
          "FunctionName": "Settlement Report",
          "Parent": "Reports",
          "NavigationURL": "FrmSettlementReportOfUK.aspx",
          "LevelID": 2,
          "SequenceID": 281
        },
        {
          "UR_FUNCTIONID": 290,
          "FunctionName": "Global Summary Report",
          "Parent": "Reports",
          "NavigationURL": "MIS_Reports/FrmGlobalSummaryReport.aspx",
          "LevelID": 2,
          "SequenceID": 283
        },
        {
          "UR_FUNCTIONID": 308,
          "FunctionName": "Agent BranchWise Corridor Summary",
          "Parent": "Reports",
          "NavigationURL": "Agent_Reports\\frmAgentBranchSummary.aspx",
          "LevelID": 2,
          "SequenceID": 306
        },
        {
          "UR_FUNCTIONID": 310,
          "FunctionName": "Treasury Revaluation Report",
          "Parent": "Reports",
          "NavigationURL": "frmTreasuryRevReport.aspx",
          "LevelID": 2,
          "SequenceID": 308
        },
        {
          "UR_FUNCTIONID": 318,
          "FunctionName": "PayOut Bank Log Details",
          "Parent": "Reports",
          "NavigationURL": "Agent_Integration_Report.aspx",
          "LevelID": 2,
          "SequenceID": 314
        },
        {
          "UR_FUNCTIONID": 1356,
          "FunctionName": "View Total Wallet floating Amount Report",
          "Parent": "Reports",
          "NavigationURL": "ViewTotalWalletAmountBalance_Report.aspx",
          "LevelID": 2,
          "SequenceID": 353
        },
        {
          "UR_FUNCTIONID": 1357,
          "FunctionName": "View Wallet To Wallet Transaction Report",
          "Parent": "Reports",
          "NavigationURL": "ViewWalletToWalletTransaction.aspx",
          "LevelID": 2,
          "SequenceID": 354
        }
      ]
    },
    {
      "UR_FUNCTIONID": 154,
      "FunctionName": "Compliance",
      "Parent": "0",
      "NavigationURL": null,
      "LevelID": 1,
      "SequenceID": 154,
      "submenus": [
        {
          "UR_FUNCTIONID": 153,
          "FunctionName": "View Compliance List",
          "Parent": "Compliance",
          "NavigationURL": "frmViewblocktransaction.aspx",
          "LevelID": 2,
          "SequenceID": 153
        },
        {
          "UR_FUNCTIONID": 157,
          "FunctionName": "View Complaince Hold List",
          "Parent": "Compliance",
          "NavigationURL": "frmviewcomplainceholdtransaction.aspx",
          "LevelID": 2,
          "SequenceID": 155
        },
        {
          "UR_FUNCTIONID": 155,
          "FunctionName": "Compliance Reports",
          "Parent": "Compliance",
          "NavigationURL": null,
          "LevelID": 2,
          "SequenceID": 157
        },
        {
          "UR_FUNCTIONID": 275,
          "FunctionName": "Compliance Report",
          "Parent": "Compliance",
          "NavigationURL": "FrmComplianceRpt.aspx",
          "LevelID": 2,
          "SequenceID": 270
        },
        {
          "UR_FUNCTIONID": 321,
          "FunctionName": "Compliance Authorization",
          "Parent": "Compliance",
          "NavigationURL": "frmComplianceAuthorize.aspx",
          "LevelID": 2,
          "SequenceID": 317
        },
        {
          "UR_FUNCTIONID": 156,
          "FunctionName": "Suspicious Transaction Report",
          "Parent": "Compliance Reports",
          "NavigationURL": "frmSuspiciousTransactionReport.aspx",
          "LevelID": 3,
          "SequenceID": 156
        },
        {
          "UR_FUNCTIONID": 158,
          "FunctionName": "Indian inbound Transaction Report",
          "Parent": "Compliance Reports",
          "NavigationURL": "frmIndianinboundTransactionReport.aspx",
          "LevelID": 3,
          "SequenceID": 158
        },
        {
          "UR_FUNCTIONID": 159,
          "FunctionName": "High Value Transaction Report",
          "Parent": "Compliance Reports",
          "NavigationURL": "frmHighValueTransactionReport.aspx",
          "LevelID": 3,
          "SequenceID": 159
        },
        {
          "UR_FUNCTIONID": 161,
          "FunctionName": "Compliance hold Transaction Report",
          "Parent": "Compliance Reports",
          "NavigationURL": "frmComplianceholdTransactionReport.aspx",
          "LevelID": 3,
          "SequenceID": 160
        },
        {
          "UR_FUNCTIONID": 160,
          "FunctionName": "Compliance Released Transaction Report",
          "Parent": "Compliance Reports",
          "NavigationURL": "frmComplianceReleasedTransactionReport.aspx",
          "LevelID": 3,
          "SequenceID": 160
        }
      ]
    },
    {
      "UR_FUNCTIONID": 1345,
      "FunctionName": "Customers",
      "Parent": "0",
      "NavigationURL": null,
      "LevelID": 1,
      "SequenceID": 342,
      "submenus": [
        {
          "UR_FUNCTIONID": 1346,
          "FunctionName": "View Customer Details",
          "Parent": "Customers",
          "NavigationURL": "frmViewCustomerDetails.aspx",
          "LevelID": 2,
          "SequenceID": 343
        },
        {
          "UR_FUNCTIONID": 1347,
          "FunctionName": "Approve Bank Deposit Transactions",
          "Parent": "Customers",
          "NavigationURL": "BankDeposit_Txns.aspx",
          "LevelID": 2,
          "SequenceID": 344
        },
        {
          "UR_FUNCTIONID": 1348,
          "FunctionName": "Upload KYC Document",
          "Parent": "Customers",
          "NavigationURL": "FrmUploadKycDocument.aspx",
          "LevelID": 2,
          "SequenceID": 345
        },
        {
          "UR_FUNCTIONID": 1349,
          "FunctionName": "Approve KYC Document",
          "Parent": "Customers",
          "NavigationURL": "KYCDocument.aspx",
          "LevelID": 2,
          "SequenceID": 346
        },
        {
          "UR_FUNCTIONID": 1350,
          "FunctionName": "Block/Unlock Customers",
          "Parent": "Customers",
          "NavigationURL": "frmLockUnlockCustomer.aspx",
          "LevelID": 2,
          "SequenceID": 347
        },
        {
          "UR_FUNCTIONID": 1352,
          "FunctionName": "Load Wallet",
          "Parent": "Customers",
          "NavigationURL": "frmLoadWallet.aspx",
          "LevelID": 2,
          "SequenceID": 349
        },
        {
          "UR_FUNCTIONID": 1353,
          "FunctionName": "Register Customer",
          "Parent": "Customers",
          "NavigationURL": "frmCustomerRegistration.aspx",
          "LevelID": 2,
          "SequenceID": 350
        },
        {
          "UR_FUNCTIONID": 1354,
          "FunctionName": "Register Beneficiary",
          "Parent": "Customers",
          "NavigationURL": "frmViewCustomerDetailsS.aspx",
          "LevelID": 2,
          "SequenceID": 351
        },
        {
          "UR_FUNCTIONID": 1358,
          "FunctionName": "Merchants Management",
          "Parent": "Merchants",
          "NavigationURL": "GetAllMarchants.aspx",
          "LevelID": 2,
          "SequenceID": 355
        },
        {
          "UR_FUNCTIONID": 1359,
          "FunctionName": "Load Merchants Wallet",
          "Parent": "Merchants",
          "NavigationURL": "FrmLoadMerchantWallet.aspx",
          "LevelID": 2,
          "SequenceID": 356
        }
      ]
    }
  ]